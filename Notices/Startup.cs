﻿using Hangfire;
using Microsoft.Owin;
using Owin;
using Hangfire.SqlServer;
using System;
using Hangfire.Dashboard;
using System.Collections.Generic;
using Notices.Services;
using Notices.Core.Data;
using System.Linq;
using Notices.Core;
using System.Web;
using Hangfire.Storage;
using System.Net;
using Notices.Services.Email;
using Notices.Services.Postmark;
using System.IO;
using System.Web.Hosting;
using Clc.Polaris.Api.Models;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OpenIdConnect;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Owin.Security.Notifications;
using System.Threading.Tasks;
using Clc.Auth;
using Microsoft.IdentityModel.Logging;

[assembly: OwinStartup(typeof(Notices.Startup))]

namespace Notices
{
    public class Startup
    {
        // The Client ID is used by the application to uniquely identify itself to Microsoft identity platform.
        string clientId = System.Configuration.ConfigurationManager.AppSettings["ClientId"];

        // RedirectUri is the URL where the user will be redirected to after they sign in.
        string redirectUri = System.Configuration.ConfigurationManager.AppSettings["RedirectUri"];

        // Tenant is the tenant ID (e.g. contoso.onmicrosoft.com, or 'common' for multi-tenant)
        static string tenant = System.Configuration.ConfigurationManager.AppSettings["Tenant"];

        // Authority is the URL for authority, composed of the Microsoft identity platform and the tenant name (e.g. https://login.microsoftonline.com/contoso.onmicrosoft.com/v2.0)
        string authority = string.Format(System.Globalization.CultureInfo.InvariantCulture, System.Configuration.ConfigurationManager.AppSettings["Authority"], tenant);

        public void Configuration(IAppBuilder app)
        {
            IdentityModelEventSource.ShowPII = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            GlobalConfiguration.Configuration.UseSqlServerStorage($"Data Source={AppSettings.DbServer};Initial Catalog=CLC_Notices;Integrated Security=True");

            if (AppSettings.RebuildHangfireJobs || AppSettings.Dev)
            {
                using (var connection = JobStorage.Current.GetConnection())
                {
                    foreach (var recurringJob in connection.GetRecurringJobs())
                    {
                        RecurringJob.RemoveIfExists(recurringJob.Id);
                    }
                }
            }

            if (AppSettings.EnableDialout)
            {
                RecurringJob.AddOrUpdate("Delete old recordings", () => Janitor.DeleteOldRecordings(), Cron.Daily(2), TimeZoneInfo.Local);
                RecurringJob.AddOrUpdate("Delete old calls", () => Janitor.DeleteOldCalls(), Cron.Daily(2, 30), TimeZoneInfo.Local);

                if (AppSettings.Dev)
                {
                    RecurringJob.AddOrUpdate("DEV Run dialout", () => Dialout.Run(true), Cron.Never);
                }
                else
                {
                    RecurringJob.AddOrUpdate("Run dialout", () => Dialout.Run(false), $"30 */{AppSettings.DialoutRunInterval} * * *", TimeZoneInfo.Local);
                    RecurringJob.AddOrUpdate("Phone -> Mail Rollover", () => Dialout.PerformDbMailRollover(), Cron.Daily(22), TimeZoneInfo.Local);
                }

                RecurringJob.AddOrUpdate("Check dialout update service", () => Dialout.CheckService(), "*/5 * * * *");
            }

            if (AppSettings.EnableSmsProcessing)
            {
                RecurringJob.AddOrUpdate("Send after hours SMS", () => SMS.SendAfterHours(), Cron.Daily(AppSettings.AfterHoursSmsSendHour, AppSettings.AfterHoursSmsSendMinute), TimeZoneInfo.Local);
                RecurringJob.AddOrUpdate("Delete old sms messages", () => Janitor.DeleteOldSmsMessages(), Cron.Daily(3), TimeZoneInfo.Local);
                RecurringJob.AddOrUpdate("Check sms service", () => SMS.CheckService(), "*/5 * * * *");
            }            

            app.SetDefaultSignInAsAuthenticationType(CookieAuthenticationDefaults.AuthenticationType);

            app.UseCookieAuthentication(new CookieAuthenticationOptions());
            _ = app.UseOpenIdConnectAuthentication(
                new OpenIdConnectAuthenticationOptions
                {
                    // Sets the ClientId, authority, RedirectUri as obtained from web.config
                    ClientId = clientId,
                    Authority = authority,
                    RedirectUri = redirectUri,
                    // PostLogoutRedirectUri is the page that users will be redirected to after sign-out. In this case, it is using the home page
                    PostLogoutRedirectUri = redirectUri,
                    Scope = OpenIdConnectScope.OpenIdProfile,

                    // ResponseType is set to request the code id_token - which contains basic information about the signed-in user
                    ResponseType = OpenIdConnectResponseType.CodeIdToken,
                    // ValidateIssuer set to false to allow personal and work accounts from any organization to sign in to your application
                    // To only allow users from a single organizations, set ValidateIssuer to true and 'tenant' setting in web.config to the tenant name
                    // To allow users from only a list of specific organizations, set ValidateIssuer to true and use ValidIssuers parameter
                    TokenValidationParameters = new TokenValidationParameters()
                    {
                        ValidateIssuer = false // This is a simplification
                    },
                    // OpenIdConnectAuthenticationNotifications configures OWIN to send notification of failed authentications to OnAuthenticationFailed method
                    Notifications = new OpenIdConnectAuthenticationNotifications
                    {
                        AuthenticationFailed = OnAuthenticationFailed,
                        SecurityTokenValidated = Clc.Auth.ClcClaimsAuth.OnSecurityTokenValidated// OnSecurityTokenValidated
                    }
                }
            ); 
            
            var options = new DashboardOptions
            {
                Authorization = new[] { new HangfireAuthorizationFilter() }
            };

            app.UseHangfireDashboard("/hangfire", options);
            app.UseHangfireServer();

            Clc.Auth.ClcAuthorize.UnauthorizedResult = new System.Web.Mvc.ViewResult { ViewName = "~/Views/Shared/Unauthorized.cshtml" };

            Variables.Organizations = new DbHelper().PolarisOrganizations();
            DbSettings.Init();
        }

        private Task OnAuthenticationFailed(AuthenticationFailedNotification<OpenIdConnectMessage, OpenIdConnectAuthenticationOptions> context)
        {
            context.HandleResponse();
            context.Response.Redirect("/?errormessage=" + context.Exception.Message);
            return Task.FromResult(0);
        }
    }   

    public class HangfireAuthorizationFilter : IDashboardAuthorizationFilter
    {
        public bool Authorize(DashboardContext context)
        {
            var owinContext = new OwinContext(context.GetOwinEnvironment());
            return owinContext.Authentication.User.IsInRole(AppSettings.AdminPermissionGroup);
        }
    }
}