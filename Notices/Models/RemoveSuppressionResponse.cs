﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Notices.Models
{
    public class DeletedSuppression
    {
        public string EmailAddress { get; set; }
        public string Status { get; set; }
    }

    public class RemoveSuppressionResponse
    {
        public List<DeletedSuppression> Suppressions { get; set; }
    }
}
