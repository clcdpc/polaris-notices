﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Notices
{
    public class RejectionResultViewModel
    {
        public bool Success { get; set; }
        public string Message { get; set; }
    }
}