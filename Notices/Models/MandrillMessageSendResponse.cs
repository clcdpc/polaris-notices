﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Notices.Models
{
    public class MandrillMessageSendResponse
    {
        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("_id")]
        public string Id { get; set; }

        [JsonProperty("reject_reason")]
        public string RejectReason { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        public bool IsSuccess { get { return Status.Equals("sent", StringComparison.OrdinalIgnoreCase); } }
    }
}