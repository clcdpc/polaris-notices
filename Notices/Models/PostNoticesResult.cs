﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Notices.Models
{
    public class PostNoticesResult
    {
        public bool Success { get; set; }
        public string Message { get; set; }

        public PostNoticesResult() : this(false, "")
        {

        }

        public PostNoticesResult(bool success, string message)
        {
            Success = success;
            Message = message;
        }
    }
}