﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Notices.Models
{
    public class MandrillSearchResult
    {
        [JsonConverter(typeof(EpochTimestampConverter))]
        public DateTime ts { get; set; }
        public string state { get; set; }
        public string subject { get; set; }
        public string email { get; set; }
        public List<string> tags { get; set; }
        public int opens { get; set; }
        public int clicks { get; set; }
        public List<SmtpEvent> smtp_events { get; set; }
        public string _id { get; set; }
        public string sender { get; set; }
        public object template { get; set; }
    }
}