﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Notices.Models
{
    public class Suppression
    {
        public string EmailAddress { get; set; }
        public string SuppressionReason { get; set; }
        public string Origin { get; set; }
        public DateTime CreatedAt { get; set; }
    }

    public class GetSuppressionsResponse
    {
        public List<Suppression> Suppressions { get; set; }
    }
}
