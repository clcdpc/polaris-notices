﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Notices.Models
{
    public partial class PostmarkBouncesGetResponse
    {
        public int TotalCount { get; set; }
        public List<Bounce> Bounces { get; set; }
    }

    public partial class Bounce
    {
        public string RecordType { get; set; }
        public long Id { get; set; }
        public string Type { get; set; }
        public long TypeCode { get; set; }
        public string Name { get; set; }
        public Guid MessageId { get; set; }
        public long ServerId { get; set; }
        public string Description { get; set; }
        public string Details { get; set; }
        public string Email { get; set; }
        public string From { get; set; }
        public DateTime BouncedAt { get; set; }
        public bool DumpAvailable { get; set; }
        public bool Inactive { get; set; }
        public bool CanActivate { get; set; }
        public string Subject { get; set; }
    }

}