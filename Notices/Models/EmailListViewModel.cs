﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using clc_twilio_csharp;

namespace Notices.Models
{
	public class EmailListViewModel
	{
		public string Address { get; set; }
		public List<EmailHistoryItem> Emails { get; set; }

		public EmailListViewModel(string address, List<EmailHistoryItem> emails)
		{
            Address = address;
            Emails = emails;
		}
	}
}