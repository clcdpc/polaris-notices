﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using clc_twilio_csharp;
using clc_twilio_csharp.Models;

namespace Notices.Models
{
	public class CallListModel
	{
		public List<Call> Calls { get; set; }
		public string PhoneNumber { get; set; }
	}
}