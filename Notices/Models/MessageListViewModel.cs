﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using clc_twilio_csharp;
using Notices.Core.Models;

namespace Notices.Models
{
	public class MessageListViewModel
	{
		public string PhoneNumber { get; set; }
		public List<SmsMessageViewModel> Messages { get; set; }

		public MessageListViewModel(string phoneNumber, List<SmsMessageViewModel> messages)
		{
			PhoneNumber = phoneNumber;
			Messages = messages;
		}
	}
}