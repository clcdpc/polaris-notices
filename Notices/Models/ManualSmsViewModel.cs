﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Notices.Models
{
    public class ManualSmsViewModel
    {
        public string PhoneNumber { get; set; }
        public string Message { get; set; }
        public string Info { get; set; }
    }
}