﻿using Newtonsoft.Json;
using NLog;
using Notices.Core;
using Notices.Core.Models;
using Notices.Core.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace Notices.Models
{
    public class EmailTemplateCache
    {
        List<EmailTemplateCacheEntry> Templates { get; set; } = new List<EmailTemplateCacheEntry>();
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private static HttpClient client = new HttpClient();

        public static EmailTemplateCache BuildTemplateCache(IEnumerable<PolarisNotification> notices)
        {
            var templateCache = new EmailTemplateCache();

            var groupedNotices = notices.GroupBy(n => new { n.EmailGroupBranchId, n.PatronLanguage, n.NotificationType }).Select(g => g.First());

            foreach (var g in groupedNotices)
            {
                var templates = DbHelper.GetEmailTemplate(g.EmailGroupBranchId, g.NotificationTypeID, g.PatronLanguage);

                try
                {
                    // check to make sure getting the template actually works
                    var cacheObject = new EmailTemplateCacheEntry
                    {
                        OrganizationId = g.EmailGroupBranchId,
                        LanguageId = g.PatronLanguage,
                        NotificationType = g.NotificationType,
                        HtmlTemplate = client.GetStringAsync(templates.HtmlTempalateUrl).Result,
                        PlaintextTemplate = client.GetStringAsync(templates.PlaintextTemplateUrl).Result
                    };

                    templateCache.Templates.Add(cacheObject);
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                    templateCache.Templates.Add(new SkipThisEntry
                    {
                        OrganizationId = g.EmailGroupBranchId,
                        LanguageId = g.PatronLanguage,
                        NotificationType = g.NotificationType
                    });
                }
            }

            logger.Trace(JsonConvert.SerializeObject(templateCache.Templates));

            return templateCache;
        }

        public EmailTemplateCacheEntry GetTemplate(List<PolarisNotification> notices, int organizationId)
        {
            var notice = notices.First();

            var template = Templates.Single(t => t.OrganizationId == organizationId
                && t.NotificationType == notice.NotificationType
                && t.LanguageId == notice.PatronLanguage);

            return template;
        }
    }

    public class EmailTemplateCacheEntry
    {
        public int OrganizationId { get; set; }
        public int LanguageId { get; set; }
        public NotificationType NotificationType { get; set; }
        public string HtmlTemplate { get; set; }
        public string PlaintextTemplate { get; set; }
    }

    public class SkipThisEntry : EmailTemplateCacheEntry { }
}