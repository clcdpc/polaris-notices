﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Notices.Models
{
    public class EmailHistoryItem
    {
        public string Id { get; set; }
        public string To { get; set; }
        public string From { get; set; }
        public string Subject { get; set; }
        public DateTime ReceivedAt { get; set; }
        public string Status { get; set; }
    }
}