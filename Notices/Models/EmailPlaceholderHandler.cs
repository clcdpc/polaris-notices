﻿using NLog;
using Notices.Core.Data;
using Notices.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Notices.Models
{
    public class EmailPlaceholderHandler
    {
        private List<PolarisNotification> Notices;
        private List<IEmailTemplatePlaceholder> Placeholders;

        private static Logger logger = LogManager.GetCurrentClassLogger();

        public EmailPlaceholderHandler(List<PolarisNotification> notices, List<IEmailTemplatePlaceholder> placeholders)
        {
            Notices = notices;
            Placeholders = placeholders;
        }

        public string ReplaceAll(string templateText, EmailFormat format)
        {
            foreach (var ph in Placeholders)
            {
                var updated = templateText.Replace(ph.Mnemonic, ph.GetValue(Notices, format));
                templateText = updated;
            }

            return templateText;
        }
    }
}