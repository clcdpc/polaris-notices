﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Notices.Models
{
    public class PostmarkMessageSearchResult
    {
        public int TotalCount { get; set; }
        public List<PostmarkHistoryMessage> Messages { get; set; }
    }

    public class PostmarkHistoryMessage
    {
        public string MessageID { get; set; }
        public List<To> To { get; set; }
        public DateTime ReceivedAt { get; set; }
        public string From { get; set; }
        public string Subject { get; set; }
        public string Status { get; set; }

        public EmailHistoryItem ToEmailHistoryItem()
        {
            return new EmailHistoryItem
            {
                Id = MessageID,
                To = To.FirstOrDefault().Email,
                From = From,
                Subject = Subject,
                ReceivedAt = ReceivedAt,
                Status = Status
            };
        }
    }

    public class To
    {
        public string Email { get; set; }
        public string Name { get; set; }
    }
}