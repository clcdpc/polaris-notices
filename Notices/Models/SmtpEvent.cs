﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Notices.Models
{
    public class SmtpEvent
    {
        [JsonConverter(typeof(EpochTimestampConverter))]
        public DateTime ts { get; set; }
        public string type { get; set; }
        public string diag { get; set; }
        public string source_ip { get; set; }
        public string destination_ip { get; set; }
        public int size { get; set; }
    }
}