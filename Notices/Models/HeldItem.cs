﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Clc.Polaris.Api.Models;

namespace Notices.Models
{
    public class HeldItem
    {
        public int RequestId { get; set; }
        public string Title { get; set; }
        public string PickupBranchName { get; set; }
        public DateTime? PickupByDate { get; set; }
        public bool IsILLRequest { get; set; } = false;

        public HeldItem()
        {

        }

        public HeldItem(PatronHoldRequestsGetRow hold)
        {
            RequestId = hold.HoldRequestID;
            Title = hold.Title;
            PickupBranchName = hold.PickupBranchName;
            PickupByDate = hold.PickupByDate;
        }

        public HeldItem(PatronILLRequestsGetRow ill)
        {
            RequestId = ill.ILLRequestID;
            Title = ill.Title;
            PickupBranchName = ill.PickupBranch;
            PickupByDate = ill.PickupByDate;
            IsILLRequest = true;
        }
    }
}