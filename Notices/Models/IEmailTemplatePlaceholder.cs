﻿using Notices.Core;
using Notices.Core.Models;
using Notices.Core.Data;
using Notices.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Notices.Models
{

    public interface IEmailTemplatePlaceholder
    {
        string Mnemonic { get; }
        string GetValue(List<PolarisNotification> input, EmailFormat format);
    }

    public class PatronFirstNamePlaceholder : IEmailTemplatePlaceholder
    {
        public string Mnemonic { get; } = "{patron_first_name}";

        public string GetValue(List<PolarisNotification> input, EmailFormat format)
        {
            return input.First().NameFirst;
        }
    }

    public class PatronEmailPlaceholder : IEmailTemplatePlaceholder
    {
        public string Mnemonic { get; } = "{patron_email_address}";

        public string GetValue(List<PolarisNotification> input, EmailFormat format)
        {
            return input.First().EmailAddress;
        }
    }

    public class PatronPhonePlaceholder : IEmailTemplatePlaceholder
    {
        public string Mnemonic { get; } = "{patron_phone}";

        public string GetValue(List<PolarisNotification> input, EmailFormat format)
        {
            return input.First().PatronPhone;
        }
    }

    public class PatronLastNamePlaceholder : IEmailTemplatePlaceholder
    {
        public string Mnemonic { get; } = "{patron_last_name}";

        public string GetValue(List<PolarisNotification> input, EmailFormat format)
        {
            return input.First().NameLast;
        }
    }

    public class PatronStreetOnePlaceholder : IEmailTemplatePlaceholder
    {
        public string Mnemonic { get; } = "{patron_street_one}";

        public string GetValue(List<PolarisNotification> input, EmailFormat format)
        {
            return input.First().PatronStreetOne;
        }
    }

    public class PatronStreetTwoPlaceholder : IEmailTemplatePlaceholder
    {
        public string Mnemonic { get; } = "{patron_street_two}";

        public string GetValue(List<PolarisNotification> input, EmailFormat format)
        {
            return input.First().PatronStreetTwo;
        }
    }

    public class PatronCityPlaceholder : IEmailTemplatePlaceholder
    {
        public string Mnemonic { get; } = "{patron_city}";

        public string GetValue(List<PolarisNotification> input, EmailFormat format)
        {
            return input.First().PatronCity;
        }
    }

    public class PatronStatePlaceholder : IEmailTemplatePlaceholder
    {
        public string Mnemonic { get; } = "{patron_state}";

        public string GetValue(List<PolarisNotification> input, EmailFormat format)
        {
            return input.First().PatronState;
        }
    }

    public class PatronZipPlaceholder : IEmailTemplatePlaceholder
    {
        public string Mnemonic { get; } = "{patron_zip}";

        public string GetValue(List<PolarisNotification> input, EmailFormat format)
        {
            return input.First().PatronZip;
        }
    }

    public class ReportingBranchNamePlaceholder : IEmailTemplatePlaceholder
    {
        public string Mnemonic { get; } = "{reporting_branch_name}";

        public string GetValue(List<PolarisNotification> input, EmailFormat format)
        {
            return input.First().ReportingBranchName;
        }
    }

    public class ReportingBranchStreetOnePlaceholder : IEmailTemplatePlaceholder
    {
        public string Mnemonic { get; } = "{reporting_branch_street_one}";

        public string GetValue(List<PolarisNotification> input, EmailFormat format)
        {
            return input.First().ReportingBranchStreetOne;
        }
    }

    public class ReportingBranchStreetTwoPlaceholder : IEmailTemplatePlaceholder
    {
        public string Mnemonic { get; } = "{reporting_branch_street_two}";

        public string GetValue(List<PolarisNotification> input, EmailFormat format)
        {
            return input.First().ReportingBranchStreetTwo;
        }
    }

    public class ReportingBranchCityPlaceholder : IEmailTemplatePlaceholder
    {
        public string Mnemonic { get; } = "{reporting_branch_city}";

        public string GetValue(List<PolarisNotification> input, EmailFormat format)
        {
            return input.First().ReportingBranchCity;
        }
    }

    public class ReportingBranchStatePlaceholder : IEmailTemplatePlaceholder
    {
        public string Mnemonic { get; } = "{reporting_branch_state}";

        public string GetValue(List<PolarisNotification> input, EmailFormat format)
        {
            return input.First().ReportingBranchState;
        }
    }

    public class ReportingBranchZipPlaceholder : IEmailTemplatePlaceholder
    {
        public string Mnemonic { get; } = "{reporting_branch_zip}";

        public string GetValue(List<PolarisNotification> input, EmailFormat format)
        {
            return input.First().ReportingBranchZip;
        }
    }

    //public class HoldMessageIntroPlaceholder : IEmailTemplatePlaceholder
    //{
    //    public string Mnemonic { get; } = "{hold_message_intro}";

    //    public string GetValue(List<PolarisNotification> input, EmailFormat format)
    //    {
    //        var output = input.GetString($"hold_message_intro_{format.ToString()}");
    //        return output;
    //    }
    //}

    public class NoticeHeaderPlaceholder : IEmailTemplatePlaceholder
    {
        public string Mnemonic { get; } = "{notice_header}";

        public string GetValue(List<PolarisNotification> input, EmailFormat format)
        {
            var notice = input.First();
            var settingOrgs = new[] { 1, notice.EmailTemplateBranchId, notice.EmailTemplateLibraryId };
            var db = new DbHelper(AppSettings.DbServer);
            var header = db.GetEmailNoticeHeader(settingOrgs, notice.EmailGroupNotificationType, notice.PatronLanguage, format);
            return header.Value;
        }
    }

    public class NoticeFooterPlaceholder : IEmailTemplatePlaceholder
    {
        public string Mnemonic { get; } = "{notice_footer}";

        public string GetValue(List<PolarisNotification> input, EmailFormat format)
        {
            var notice = input.First();
            var settingOrgs = new[] { 1, notice.EmailTemplateBranchId, notice.EmailTemplateLibraryId };
            var db = new DbHelper(AppSettings.DbServer);
            var footer = db.GetEmailNoticeFooter(settingOrgs, notice.EmailGroupNotificationType, notice.PatronLanguage, format);
            return footer.Value;
        }
    }

    public class UnsubscribeLinkPlaceholder : IEmailTemplatePlaceholder
    {
        public string Mnemonic { get; } = "{unsubscribe_link}";

        public string GetValue(List<PolarisNotification> input, EmailFormat format)
        {
            if (format == EmailFormat.Html)
            {
                return "To unsubscribe from library account hold pick up notices, reminder notices and other account related correspondence, use this link: <a href=\"https://notices.clcohio.org/email/unsubscribe\">https://notices.clcohio.org/email/unsubscribe</a>";
            }

            return "To unsubscribe from library account hold pick up notices, reminder notices and other account related correspondence, use this link: https://notices.clcohio.org/email/unsubscribe";
        }
    }

    public class PatronHoldBranchGroupsPlaceholder : IEmailTemplatePlaceholder
    {
        public string Mnemonic { get; } = "{patron_hold_branch_groups}";

        public string GetValue(List<PolarisNotification> input, EmailFormat format)
        {
            var output = "";

            var groupFormat = input.GetString(Mnemonics.EmailHoldBranchGroupFormat(format));
            var rowFormat = input.GetString(Mnemonics.EmailHeldItemListFormat(format));
            var separator = input.GetString(Mnemonics.EmailHoldBranchGroupSeparator(format));

            var branches = input.Where(n => n.IsAnyHold).GroupBy(n => n.ReportingBranchID).ToList();

            for (int i = 0; i < branches.Count(); i++)
            {
                var itemRows = "";
                var branch = branches[i];

                foreach (var notice in branch.ToList().OrderBy(b => b.HoldTillDate))
                {
                    var itemRowsCheck = rowFormat.Replace("{item_title}", notice.Title)
                            .Replace("{item_author}", notice.Author)
                            .Replace("{item_barcode}", notice.ItemBarcode)
                            .Replace("{pickup_branch_name}", branch.First().ReportingBranchName)
                            .Replace("{item_material_type}", notice.MaterialType)
                            .Replace("{item_held_till_date}", notice.HoldTillDate.Value.ToShortDateString());
                    itemRows += itemRowsCheck;
                }

                var header = input.GetString(Mnemonics.EmailHeldItemGroupHeader(format));
                var headerRow = input.GetString(Mnemonics.EmailHeldItemsTableHeaderRow);
                var table = "";

                if (format == EmailFormat.Html)
                {
                    table = $"<table id=\"held-items-{branch.Key}\">\r\n{headerRow}\r\n{itemRows}\r\n</table>";
                }
                else
                {
                    table = $"{itemRows}";
                }

                header = header.Replace("{pickup_branch_url}", DbSettings.PickupBranchUrl(branch.First().ReportingBranchID, input.First().PatronLanguage))
                    .Replace("{pickup_branch_name}", branch.First().ReportingBranchName);

                var outputCheck = groupFormat
                    .Replace("{held_item_group_header}", header)
                    .Replace("{held_items}", table);

                output += outputCheck;
                if ((i + 1) < branches.Count()) { output += separator; }
            }

            return output;
        }
    }

    public class NoticeBodyPlaceholder : IEmailTemplatePlaceholder
    {
        public string Mnemonic { get; } = "{notice_body}";

        public string GetValue(List<PolarisNotification> input, EmailFormat format)
        {
            var notificationTypeId = input.First().NotificationTypeID;

            if (input.First().NotificationType == NotificationType.ClcCustomCovid19)
            {
                return new CovidHoldListPlaceHolder().GetValue(input, format);
            }

            if (input.First().EmailGroupNotificationType == NotificationType.Combined)
            {
                return new OverdueItemListPlaceholder().GetValue(input, format);
            }

            if (input.First().IsOverdue)
            {
                return new OverdueItemListPlaceholder().GetValue(input, format);
            }

            if (notificationTypeId == 2 || notificationTypeId == 18)
            {
                return new PatronHoldBranchGroupsPlaceholder().GetValue(input, format);
            }

            if (input.First().IsAlmostOverdueReminder)
            {
                var almostOverdueItems = new AlmostOverdueReminderAlmostOverdueItemsPlaceholder().GetValue(input, format);
                var autorenewedItems = new AlmostOverdueReminderAutorenewItemsGroupPlaceholder().GetValue(input, format);
                var otherItems = new AlmostOverdueReminderOtherItemsGroupPlaceholder().GetValue(input, format);

                var separator = input.GetString(Mnemonics.EmailAlmostOverdueTableSeparator(format));
                //var separator = format == EmailFormat.Html ? "<br/><br/>" : "\r\n\r\n";

                return $"{almostOverdueItems}{separator}{autorenewedItems}{separator}{otherItems}";
            }

            if (input.First().IsHoldCancellation)
            {
                return new CancelledHoldRequestListPlaceholder().GetValue(input, format);
            }

            if (notificationTypeId == 10)
            {
                return input.GetString(Mnemonics.EmailExpirationBody(format));
            }

            return "";
        }
    }

    public class CovidHoldListPlaceHolder : IEmailTemplatePlaceholder
    {
        public string Mnemonic { get; } = "{covid_hold_list}";

        public string GetValue(List<PolarisNotification> input, EmailFormat format)
        {
            var output = "";

            var detailsEnabled = DbSettings.GetValue<bool>(Mnemonics.EnableCovidHoldDetails, input.First().PatronBranch);

            if (detailsEnabled)
            {
                var headerRow = input.GetString(Mnemonics.EmailCovidHoldReminderTableHeaderRow);
                var rowMnemonic = $"email_covid_hold_request_row_format_{format.ToString()}";
                var rowFormat = input.GetString(rowMnemonic);

                foreach (var notice in input.Where(n => n.NotificationType == NotificationType.ClcCustomCovid19).OrderBy(n => n.ReportingBranchName))
                {
                    output += rowFormat.Replace("{item_title}", notice.Title)
                            .Replace("{item_author}", notice.Author)
                            .Replace("{pickup_branch_name}", notice.ReportingBranchName);
                }

                if (format == EmailFormat.Html)
                {
                    return $"<table id=\"covid-holds\">\r\n{headerRow}\r\n{output}\r\n</table>";
                }

                return output;
            }
            else
            {
                var mnemonic = $"email_covid_hold_request_general_{format.ToString()}";
                var value = input.GetString(mnemonic)
                            .Replace("{hold_count}", input.Count.ToString())
                            .Replace("{word_holds}", "hold".Pluralize(input.Count));
                return value;
            }
        }
    }

    public class AlmostOverdueReminderAutorenewItemsGroupPlaceholder : IEmailTemplatePlaceholder
    {
        public string Mnemonic { get; } = "{reminder_autorenew_items}";

        public string GetValue(List<PolarisNotification> input, EmailFormat format)
        {
            var autorenewItems = input.Where(n => n.NotificationType == NotificationType.AlmostOverdueReminder && n.AutoRenewal);
            if (!autorenewItems.Any()) return "";

            var rowMnemonic = Mnemonics.EmailReminderAutorenewItemRowFormat(format);
            var rowFormat = input.GetString(rowMnemonic);

            var itemRows = "";
            foreach (var notice in autorenewItems.OrderBy(i => i.DueDate))
            {
                itemRows += rowFormat.Replace("{item_duedate}", notice.DueDate.Value.ToShortDateString())
                        .Replace("{item_title}", notice.Title)
                        .Replace("{item_material_type}", notice.MaterialType)
                        .Replace("{item_assigned_branch}", notice.ItemAssignedBranch);
            }

            var headerMnemonic = Mnemonics.EmailReminderAutornewItemsHeader(format);
            var header = input.GetString(headerMnemonic);
            var headerRow = input.GetString("email_reminder_autorenew_items_table_header_row");
            var output = "";

            if (format == EmailFormat.Html)
            {
                output = $"{header}<table id=\"autorenew-items\">\r\n{headerRow}\r\n{itemRows}</table>";
            }
            else
            {
                output = $"{header}\r\n{itemRows}";
            }

            return output;
        }
    }

    public class AlmostOverdueReminderAlmostOverdueItemsPlaceholder : IEmailTemplatePlaceholder
    {
        public string Mnemonic { get; } = "{reminder_almost_overdue_items}";

        public string GetValue(List<PolarisNotification> input, EmailFormat format)
        {
            var almostOverdueItems = input.Where(n => n.NotificationType == NotificationType.AlmostOverdueReminder && !n.AutoRenewal);
            if (!almostOverdueItems.Any()) return "";

            var rowMnemonic = $"email_reminder_almost_overdue_item_row_format_{format.ToString()}";
            var rowFormat = input.GetString(rowMnemonic);

            var itemRows = "";
            foreach (var notice in almostOverdueItems.OrderBy(i => i.DueDate))
            {
                itemRows += rowFormat.Replace("{item_duedate}", notice.DueDate.Value.ToShortDateString())
                        .Replace("{item_title}", notice.Title)
                        .Replace("{item_material_type}", notice.MaterialType)
                        .Replace("{item_assigned_branch}", notice.ItemAssignedBranch);
            }

            var headerMnemonic = $"email_reminder_almost_overdue_items_header_{format.ToString()}";
            var header = input.GetString(headerMnemonic);
            var headerRow = input.GetString("email_reminder_almost_overdue_items_table_header_row");
            var output = "";

            if (format == EmailFormat.Html)
            {
                output = $"{header}<table id=\"almost-overdue-items\">\r\n{headerRow}\r\n{itemRows}</table>";
            }
            else
            {
                output = $"{header}\r\n{itemRows}";
            }

            return output;
        }
    }

    public class AlmostOverdueReminderOtherItemsGroupPlaceholder : IEmailTemplatePlaceholder
    {
        public string Mnemonic { get; } = "{reminder_other_items}";

        public string GetValue(List<PolarisNotification> input, EmailFormat format)
        {
            if (!input.Any(n => n.IsAlmostOverdueReminder)) return "";

            var otherItems = new List<GetEmailReminderOtherItems_Result>();

            if (Methods.GetEmailSender().GetType() == typeof(PostmarkEmailSender) || Methods.GetEmailSender().GetType() == typeof(AddToListEmailSender))
            {
                var db = new DbHelper(AppSettings.DbServer);
                otherItems = db.GetEmailReminderOtherItems(input.First().PatronID).ToList();
            }
            else
            {
                otherItems = Methods.CreateReminderOtherItemList();
            }

            if (!otherItems.Any()) return "";

            var rowMnemonic = $"email_reminder_other_item_row_format_{format.ToString()}";
            var rowFormat = input.GetString(rowMnemonic);

            var itemRows = "";
            foreach (var notice in otherItems.OrderBy(i => i.DueDate))
            {
                itemRows += rowFormat.Replace("{item_duedate}", notice.DueDate.Value.ToShortDateString())
                        .Replace("{item_title}", notice.Title)
                        .Replace("{item_material_type}", notice.MaterialType)
                        .Replace("{item_assigned_branch}", notice.AssignedBranch)
                        .Replace("{item_due}", notice.DueDate < DateTime.Now ? "Yes" : "No");
            }


            var headerMnemonic = $"email_reminder_other_items_header_{format.ToString()}";
            var header = input.GetString(headerMnemonic);
            var headerRow = input.GetString("email_reminder_other_items_table_header_row");
            var output = "";

            if (format == EmailFormat.Html)
            {
                output = $"{header}<table id=\"other-items\">\r\n{headerRow}\r\n{itemRows}</table>";
            }
            else
            {
                output = $"{header}\r\n{itemRows}";
            }

            return output;
        }
    }

    public class CancelledHoldRequestListPlaceholder : IEmailTemplatePlaceholder
    {
        public string Mnemonic { get; } = "{cancelled_holds}";

        public string GetValue(List<PolarisNotification> input, EmailFormat format)
        {
            var output = "";

            var rowMnemonic = $"email_cancelled_hold_request_row_format_{format.ToString()}";
            var rowFormat = input.GetString(rowMnemonic);

            foreach (var notice in input.Where(n => n.IsHoldCancellation))
            {
                output += rowFormat.Replace("{item_title}", notice.Title)
                        .Replace("{hold_request_date}", notice.CancelledHoldRequestDate.Value.ToShortDateString())
                        .Replace("{hold_cancellation_reason}", notice.HoldCancellationReason);
            }

            if (format == EmailFormat.Html)
            {
                return $"<table id=\"cancelled-holds\">{output}</table>";
            }

            return output;
        }
    }

    public class InnreachOverdueItemListPlaceholder : OverdueItemListPlaceholder
    {
        public new string Mnemonic { get; } = "{overdue_items}";

        IEnumerable<PolarisNotification> LoadItems(List<PolarisNotification> input, bool separateInnreachItems)
        {
            return input.Where(n => n.IsOverdue && (!separateInnreachItems || !n.ILLFlag.GetValueOrDefault())).OrderBy(n => n.DueDate);
        }
    }

    public class OverdueItemListPlaceholder : IEmailTemplatePlaceholder
    {
        public string Mnemonic { get; } = "{overdue_items}";

        public string GetValue(List<PolarisNotification> input, EmailFormat format)
        {
            var separateInnreachItems = DbSettings.EmailSeparateInnreachItems(input.First().EmailTemplateBranchId, input.First().PatronLanguage);
            var isCombinedOverdue = DbSettings.EmailCombineOverdueNotices(input.First().EmailTemplateBranchId, input.First().PatronLanguage);

            var overdueItems = LoadItems(input, separateInnreachItems);
            if (!overdueItems.Any()) { return ""; }

            if (isCombinedOverdue)
            {
                overdueItems = overdueItems.OrderByDescending(i => i.NotificationTypeID).ThenBy(i => i.DueDate);
            }

            var overdueCountDescDef = new Dictionary<NotificationType, string>
            {
                [NotificationType.FirstOverdue] = "#1",
                [NotificationType.SecondOverdue] = "#2",
                [NotificationType.ThirdOverdue] = "#3"
            };

            var rowMnemonic = isCombinedOverdue ? $"email_combined_overdue_item_row_format_{format.ToString()}" : $"email_overdue_item_row_format_{format.ToString()}";
            var rowFormat = input.GetString(rowMnemonic);

            var itemRows = "";
            foreach (var notice in overdueItems)
            {
                itemRows += rowFormat.Replace("{item_title}", notice.Title)
                        .Replace("{item_author}", notice.Author)
                        .Replace("{item_call_number}", notice.ItemCallNumber)
                        .Replace("{item_barcode}", notice.ItemBarcode)
                        .Replace("{item_material_type}", notice.MaterialType)
                        .Replace("{item_due_date}", notice.DueDate.Value.ToShortDateString())
                        .Replace("{item_cko_date}", notice.CkoDate.Value.ToShortDateString())
                        .Replace("{item_cko_branch}", notice.CkoLocation)
                        .Replace("{overdue_count}", overdueCountDescDef[notice.NotificationType]);
            }

            var headerRowMnemonic = isCombinedOverdue ? "email_combined_overdue_items_table_header_row" : "email_overdue_items_table_header_row";

            var headerRow = input.GetString(headerRowMnemonic);
            var output = "";

            if (format == EmailFormat.Html)
            {
                output = $"<table id=\"overdue-items\">\r\n{headerRow}\r\n{itemRows}</table>";
            }
            else
            {
                output = $"{itemRows}";
            }

            return output;
        }

        IEnumerable<PolarisNotification> LoadItems(List<PolarisNotification> input, bool separateInnreachItems)
        {
            return input.Where(n => n.IsOverdue && (!separateInnreachItems || !n.ILLFlag.GetValueOrDefault())).OrderBy(n => n.DueDate);
        }
    }

    public class ReportingOrgWebsiteUrlPlaceholder : IEmailTemplatePlaceholder
    {
        public string Mnemonic { get; } = "{reporting_branch_website_url}";

        public string GetValue(List<PolarisNotification> input, EmailFormat format)
        {
            return input.GetString("website_url");
        }
    }

    public class ReportingBranchPhonePlaceholder : IEmailTemplatePlaceholder
    {
        public string Mnemonic { get; } = "{reporting_branch_phone}";

        public string GetValue(List<PolarisNotification> input, EmailFormat format)
        {
            if (input.First().EmailTemplateBranchId == input.First().ReportingBranchID)
            {
                return input.First().ReportingBranchPhone;
            }

            if (input.First().EmailTemplateBranchId == input.First().PatronBranch)
            {
                return input.First().PatronBranchPhone;
            }

            return "";
        }
    }
}