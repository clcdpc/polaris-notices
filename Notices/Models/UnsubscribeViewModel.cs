﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Notices.Models
{
    public class UnsubscribeViewModel
    {
        public int? OrgID { get; set; }
        [Required]
        [MinLength(5, ErrorMessage="Please enter a valid library card number")]
        [Display(Name = "Library card number")]
        public string Barcode { get; set; }
        [Required]
        [DataType(DataType.Password)]
        [MinLength(4, ErrorMessage="Please enter a valid PIN")]
        [Display(Name="PIN")]
        public string Pin { get; set; }
    }
}