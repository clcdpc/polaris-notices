﻿using Newtonsoft.Json;
using Notices.Core;
using System;
using System.Collections.Generic;

namespace Notices.Models
{
    public class MandrillMailEvent
    {
        [JsonProperty(PropertyName = "ts")]
        public string TimeStamp { get; set; }

        [JsonProperty(PropertyName = "event")]
        public string Event { get; set; }

        [JsonProperty(PropertyName = "msg")]
        public Message Msg { get; set; }

		public static MandrillMailEvent CreateTestEvent()
		{
			return new MandrillMailEvent
			{
				Msg = new Notices.Models.Message
				{
					To = new string[][] { new string[] { AppSettings.SmsTestNumber, "" } },
					Text = "test sms message"
				}
			};
		}
    }

    public class Message
    {
        [JsonProperty(PropertyName = "raw_msg")]
        public string RawMessage { get; set; }

        [JsonProperty(PropertyName = "headers")]
        public Header Header { get; set; }

        [JsonProperty(PropertyName = "text")]
        public string Text { get; set; } = "";

        [JsonProperty(PropertyName = "html")]
        public string Html { get; set; }

        [JsonProperty(PropertyName = "from_email")]
        public string FromEmail { get; set; }

        [JsonProperty(PropertyName = "from_name")]
        public string FromName { get; set; }

        [JsonProperty(PropertyName = "to")]
        public string[][] To { get; set; } = new string[][] { new string[] { "", "" } };

        public string CleanTo { get { return To[0][0].Split('@')[0]; } }
        public string CleanBody { get { return Text.Replace('\r', ' ').Replace('\n', ' '); } }

        [JsonProperty(PropertyName = "email")]
        public string Email { get; set; }

        [JsonProperty(PropertyName = "subject")]
        public string Subject { get; set; } = "";

        [JsonProperty(PropertyName = "tags")]
        public string[] Tags { get; set; }

        [JsonProperty(PropertyName = "sender")]
        public string Sender { get; set; }

        [JsonProperty(PropertyName = "dkim")]
        public DKIM DKIM { get; set; }

        [JsonProperty(PropertyName = "spf")]
        public SPF SPF { get; set; }

        [JsonProperty(PropertyName = "spam_report")]
        public SpamReport SpamReport { get; set; }

        [JsonProperty(PropertyName="bounce_description")]
        public string BounceDescription { get; set; }
    }

    [JsonDictionary()]
	[Serializable]
    public class Header : Dictionary<string, object>
    {
        // Need to find a nicer way of doing this... Dictionary<string, object> is kinda dumb
    }

    public class SpamReport
    {
        [JsonProperty(PropertyName = "score")]
        public decimal Score { get; set; }

        [JsonProperty(PropertyName = "matched_rules")]
        public SpamRule[] MatchedRules { get; set; }
    }

    public class SpamRule
    {
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "score")]
        public decimal Score { get; set; }

        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }
    }

    public class DKIM
    {
        [JsonProperty(PropertyName = "signed")]
        public bool Signed { get; set; }

        [JsonProperty(PropertyName = "valid")]
        public bool Valid { get; set; }
    }

    public class SPF
    {
        [JsonProperty(PropertyName = "result")]
        public string Result { get; set; }

        [JsonProperty(PropertyName = "detail")]
        public string Detail { get; set; }
    }


}