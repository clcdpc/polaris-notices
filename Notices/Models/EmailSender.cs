﻿using Newtonsoft.Json;
using NLog;
using Notices.Core;
using Notices.Services;
using Notices.Services.Postmark;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Notices.Models
{
    public interface IEmailSender
    {
        bool QueueUpdate { get; }
        bool UseBackgroundJob { get; }
        bool Send(EmailMessage message);
    }

    public class NullEmailSender : IEmailSender
    {
        public bool QueueUpdate => false;
        public bool UseBackgroundJob => false;

        public bool Send(EmailMessage message)
        {
            return true;
        }
    }

    public class AddToListEmailSender : IEmailSender
    {
        public bool QueueUpdate => false;
        public bool UseBackgroundJob => false;

        public static List<EmailMessage> Messages = new List<EmailMessage>();        

        public bool Send(EmailMessage message)
        {
            Messages.Add(message);
            return true;
        }
    }

    public class OutputToFileEmailSender : IEmailSender
    {
        public bool QueueUpdate => false;
        public bool UseBackgroundJob => false;

        private static Logger logger = LogManager.GetLogger($"email-file-outout.{Core.Methods.GenerateRandomString(5)}");

        public static void GenerateNewLogger()
        {
            logger = LogManager.GetLogger($"email-file-outout.{Core.Methods.GenerateRandomString(5)}");
        }

        public bool Send(EmailMessage message)
        {
            logger.Debug(message.ToJson());
            LogManager.Flush();
            return true;
        }
    }

    public class PostmarkEmailSender : IEmailSender
    {
        public bool QueueUpdate => true;
        public bool UseBackgroundJob => true;

        private static PostmarkClient postmark = new PostmarkClient(AppSettings.PostmarkEmailKey);

        public bool Send(EmailMessage message)
        {
            return postmark.Send(message);
        }
    }

    public class PostmarkStagingEmailSender : IEmailSender
    {
        public bool QueueUpdate => false;
        public bool UseBackgroundJob => true;

        private static PostmarkClient postmark = new PostmarkClient("POSTMARK_API_TEST");

        public bool Send(EmailMessage message)
        {
            return postmark.Send(message);
        }
    }

    public class MandrillEmailSender : IEmailSender
    {
        public bool QueueUpdate => true;
        public bool UseBackgroundJob => true;

        private static PostmarkClient postmark = new PostmarkClient(AppSettings.PostmarkEmailKey);
        

        public bool Send(EmailMessage message)
        {
            return Mandrill.SendMessage(String.Join(",", message.To), message.From, message.Subject, message.HtmlBody, message.TextBody)?.IsSuccess ?? false;
        }
    }
}