﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Notices.Models
{
    public abstract class RejectionRemovalResult
    {
        public bool Success { get; set; }
        public string Message { get; set; }
    }

    public class SuccessfulRejectionRemovalResult : RejectionRemovalResult
    {
        public SuccessfulRejectionRemovalResult(string message)
        {
            Success = true;
            Message = message;
        }
    }

    public class FailedRejectionRemovalResult : RejectionRemovalResult
    {
        public FailedRejectionRemovalResult(string message)
        {
            Success = false;
            Message = message;
        }
    }
}