﻿using NLog;
using Notices.Core;
using Notices.Core.Data;
using Notices.Extensions;
using Notices.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using clc_twilio_csharp;
using clc_twilio_csharp.Models;
using Clc.Polaris.Api.Models;
using Notices.Core.Data;

namespace Notices.Models
{
    public abstract class CallPlacer
    {
        public List<PolarisNotification> Notices { get; set; }
        protected int PatronId => Notices.First().PatronID;

        protected static Logger logger;

        public CallPlacer(List<PolarisNotification> notices)
        {
            Notices = notices;
        }

        public virtual string PlaceCall()
        {
            return "";
        }
    }

    public class DummyCallPlacer : CallPlacer
    {
        public DummyCallPlacer(List<PolarisNotification> notices) : base(notices)
        {
            logger = LogManager.GetCurrentClassLogger();
        }

        public override string PlaceCall()
        {
            var message = "";

            if (DbSettings.PrebuildDialoutMessages())
            {
                message = Dialout.PatronMessageCache[PatronId];
            }
            else
            {
                message = Dialout.BuildMessage($"dummy{PatronId}", "human", PatronId);
            }

            logger.Info(message);
            return "foo";
        }
    }

    public class TwilioCallPlacer : CallPlacer
    {
        public TwilioCallPlacer(List<PolarisNotification> notices) : base(notices)
        {
            logger = LogManager.GetCurrentClassLogger();
        }

        public override string PlaceCall()
        {
            var items = "";
            Notices.ForEach(n => items += $"&items={n.ItemRecordID}");

            var callUrl = $"{AppSettings.TwilioCallURL}?patronId={PatronId}{items}";
            var statusUrl = $"{AppSettings.DialoutStatusUrl}?patronId={PatronId}{items}";
            var call = Methods.CreateTwilioClient().PlaceCall
                (
                    new CallOptions
                    {
                        From = DbSettings.DialoutFromPhone(Notices.First().PatronBranch),
                        To = Notices.First().DeliveryPhone,
                        //IfMachine = "Continue",
                        FallbackUrl = $"{AppSettings.DialoutFallbackUrl}?patronId={PatronId}",
                        StatusCallback = statusUrl,
                        Url = callUrl,
                        Record = AppSettings.RecordCalls
                    }
                );

            if (!string.IsNullOrWhiteSpace(call.Exception?.Message) || string.IsNullOrWhiteSpace(call.Data?.Sid))
            {
                logger.Error("Failed to call patron {0}: {1}\r\n{2}", Notices.First().PatronID, call.Exception?.Message, Notices.ToJson());
                foreach (var notice in Notices)
                {
                    Dialout.QueueUpdate(notice.BuildUpdate(NotificationStatus.CallNoDialTone));
                }
            }

            return call?.Data?.Sid;
        }
    }

    public class TestingCallPlacer : TwilioCallPlacer
    {
        public TestingCallPlacer(List<PolarisNotification> notices) : base(SetPhoneNumber(notices))
        {
        }

        static List<PolarisNotification> SetPhoneNumber(List<PolarisNotification> notices)
        {
            notices.ForEach(n => n.DeliveryPhone = AppSettings.DebugPhoneNumber);

            return notices;
        }
    }
}