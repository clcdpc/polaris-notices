﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Notices.Models
{
    public partial class PostmarkBounceActivateResponse
    {
        public Bounce Bounce { get; set; }
        public string Message { get; set; }
    }
}