﻿using Notices.Core.Data;
using Notices.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Notices.Models
{
    public class ExampleEmailNoticeViewModel
    {
        [DisplayName("Patron Branch")]
        public int PatronBranchId { get; set; }

        [DisplayName("Reporting Organizations")]
        public List<int> ReportingOrgIds { get; set; }

        public List<PolarisOrganization> AllOrganizations { get; set; }

        [DisplayName("Notice Type")]
        public NotificationType NoticeType { get; set; }

        public SelectList NoticeTypeTest { get; set; }
        public SelectList PolarisBranches { get; set; }

        public List<NotificationType> AvailableNotificationTypes { get; set; }
    }
}