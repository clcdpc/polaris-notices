﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Notices.Models
{
	public class SubresourceUris
	{
		public string notifications { get; set; }
		public string recordings { get; set; }
	}
}
