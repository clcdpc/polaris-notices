﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Notices.Models
{
    public class EmailMessage
    {
        public IEnumerable<string> To { get; set; }
        public string From { get; set; }
        public string Subject { get; set; }
        public string HtmlBody { get; set; }
        public string TextBody { get; set; }

        public override string ToString()
        {
            return $"{From} | {string.Join(",", To)} | {Subject} | Html: {HtmlBody.Length} | Text: {TextBody.Length}";
        }
    }

    public class PostmarkMessage
    {
        public string To { get; set; }
        public string From { get; set; }
        public string Subject { get; set; }
        public string HtmlBody { get; set; }
        public string TextBody { get; set; }

        public PostmarkMessage()
        {

        }

        public PostmarkMessage(EmailMessage message)
        {
            To = string.Join(", ", message.To);
            From = message.From;
            Subject = message.Subject;
            HtmlBody = message.HtmlBody;
            TextBody = message.TextBody;
        }

        public override string ToString()
        {
            return $"{From} | {To} | {Subject} | Html: {HtmlBody.Length} | Text: {TextBody.Length}";
        }
    }
}