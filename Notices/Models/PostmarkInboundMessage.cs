﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Notices.Models
{
    public class FromFull
    {
        public string Email { get; set; }
        public string Name { get; set; }
        public string MailboxHash { get; set; }
    }

    public class ToFull
    {
        public string Email { get; set; }
        public string Name { get; set; }
        public string MailboxHash { get; set; }
    }

    //public class Header
    //{
    //    public string Name { get; set; }
    //    public string Value { get; set; }
    //}

    public class PostmarkInboundMessage
    {
        public string FromName { get; set; }
        public string MessageStream { get; set; }
        public string From { get; set; }
        public FromFull FromFull { get; set; }
        public string To { get; set; }
        public List<ToFull> ToFull { get; set; }
        public string Cc { get; set; }
        public List<object> CcFull { get; set; }
        public string Bcc { get; set; }
        public List<object> BccFull { get; set; }
        public string OriginalRecipient { get; set; }
        public string Subject { get; set; }
        public string MessageID { get; set; }
        public string ReplyTo { get; set; }
        public string MailboxHash { get; set; }
        public string Date { get; set; }
        public string TextBody { get; set; }
        public string HtmlBody { get; set; }
        public string StrippedTextReply { get; set; }
        public string Tag { get; set; }
        public List<Header> Headers { get; set; }
        public List<object> Attachments { get; set; }
    }
}