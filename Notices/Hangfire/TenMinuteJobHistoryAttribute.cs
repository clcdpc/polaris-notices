﻿using Hangfire.Common;
using Hangfire.States;
using Hangfire.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Notices.Hangfire
{
    public class TenMinuteJobHistoryAttribute : JobFilterAttribute, IApplyStateFilter
        {
            public void OnStateUnapplied(ApplyStateContext context, IWriteOnlyTransaction transaction)
            {
                context.JobExpirationTimeout = TimeSpan.FromMinutes(10);
            }

            public void OnStateApplied(ApplyStateContext context, IWriteOnlyTransaction transaction)
            {
                context.JobExpirationTimeout = TimeSpan.FromMinutes(10);
            }
        }
    
}