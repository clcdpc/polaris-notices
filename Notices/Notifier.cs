﻿using Newtonsoft.Json;
using Notices.Core;
using Notices.Services.Postmark;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Notices
{
	public class Notifier
	{
        string Name;

        public Notifier(string name)
        {
            Name = name;
        }

        public static Notifier Create(string name)
        {
            return new Notifier(name);
        }

		public void Notify(string message, NotificationMethod notificationMethod)
		{
            if (!AppSettings.EnableNotifier) return;

			switch (notificationMethod)
			{
				case NotificationMethod.All:
					SendSMS(message);
					SendEmail(message);
					break;
				case NotificationMethod.SMS:
					SendSMS(message);
					break;
				case NotificationMethod.Email:
					SendEmail(message);
					break;
				default:
					goto case NotificationMethod.Email;
			}
		}

		void SendSMS(string message)
        {
            return;
   //         var twilio = Methods.CreateTwilioClient();
   //         var numbers = twilio.ListIncomingPhoneNumbers().IncomingPhoneNumbers;
			
			//AppSettings.NotifyPhoneNumbers.ForEach(
			//	p => twilio.SendMessage(numbers.First().PhoneNumber, p, message)
			//	);
		}

        void SendEmail(string text)
        {
            //var json = new
            //{
            //    key = AppSettings.MandrillApiKey,
            //    message = new
            //    {
            //        text = text,
            //        subject = "Application Notification",
            //        from_email = "donotreply@clcohio.org",
            //        from_name = Name,
            //        to = AppSettings.NotifyEmailAddresses.Select(e => new { email = e, type = "to" }).ToArray()
            //    }
            //};

            //var client = new HttpClient();
            //var content = new StringContent(JsonConvert.SerializeObject(json), Encoding.UTF8, "application/json");
            //var result = client.PostAsync("https://mandrillapp.com/api/1.0/messages/send.json", content).Result;

            //var resultContent = result.Content.ReadAsStringAsync().Result;

			var postmark = new PostmarkClient(AppSettings.PostmarkDbKey);
			postmark.Send(AppSettings.NotifyEmailAddresses.ToArray(), "donotreply@clcohio.org", "Notice Application Notification", text, text);
        }
	}

	public enum NotificationMethod
	{
		All,
		SMS,
		Email
	}
}
