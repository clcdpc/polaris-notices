﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Twilio.TwiML;
using Clc.Polaris.Api;
using System.Text;
using Newtonsoft.Json;
using NLog;
using Notices;
using Notices.Core.Data;
using Notices.Models;
using Notices.Extensions;
using System.Threading.Tasks;
using System.Diagnostics;
using Notices.Services;
using Notices.Core;
using System.Configuration;
using Clc.Polaris.Api.Models;

namespace Notices.Controllers
{
    public class DialinController : Controller
    {
        object voiceAttribute = new { voice = "alice" };
        string hangupResponse;
        PapiClient polaris = Methods.CreatePapiClient();
        DbHelper db;
        private static Logger logger = LogManager.GetCurrentClassLogger();

        // Create the dictionary to hold all of the menu options.
        Dictionary<string, string[]> menu = new Dictionary<string, string[]>
            {
                {"default", new string[] {"yourlibrary", "renewall", "listitems", "", "listholds"} },
                {"listItems", new string[] {"yourlibrary", "listallitems", "itembybarcode", "emailitems", "", "listoverdueitems", "", "mainmenu"} },
                {"renewOptions", new string[] { "yourlibrary", "renewItem", "nextItem", "", "", "", "", "mainmenu"} },
                {"holdOptions", new string[] { "yourlibrary", "nextHold", "", "", "", "", "", "mainmenu" } }
            };

        public DialinController()
        {
            db = new DbHelper(AppSettings.DbServer);
            hangupResponse = new TwilioResponse().Say("We didn't understand your response, please call back later. Goodbye.", voiceAttribute).Hangup().ToString();
        }

        async Task updateNotices(int patronId, string from, string callsid)
        {
            await Task.Run(() =>
            {
                Dialout.PostNotices(patronId, callsid, "completed", details: "automatically updated by dialin transfer");
                Dialout.TransferredCalls.Add(callsid);
            });
        }

        public string Index(int? digits, bool isTransfer = false, string transferredBarcode = "", int transferredPatronId = 0, string node = "", string callsid = "", string from = "", bool test = false)
        {
            ConfigurationManager.RefreshSection("appSettings");

            var twiml = new TwilioResponse();

            if (!AppSettings.EnableDialin) return twiml.Hangup().ToString();

            if (test)
            {
                ProcessBarcode(AppSettings.TestBarcode, callsid);
                ProcessPin(AppSettings.TestPin, callsid);
                LookupAccount(callsid, from);
            }

            if (isTransfer)
            {
                updateNotices(transferredPatronId, from, callsid).Forget();
                ProcessBarcode(transferredBarcode, callsid);
            }

            if (HandleTimeout(digits.ToString(), callsid)) return hangupResponse;

            // Checks to see if the list of patron barcodes has an entry for the current callsid and prompts the user if needed
            if (!CallVars.PatronBarcodes.Keys.Contains(callsid))
            {
                var dialinPatrons = db.DialinPatrons(Pred.Field<Dialin_Patrons>(p => p.PhoneNumber, "=", from));
                var ignoreListMatches = db.DialinIgnoreList(Pred.Field<Dialin_Ignore_List>(p => p.PhoneNumber, "=", from));
                if (dialinPatrons.Any() && !ignoreListMatches.Any())
                {
                    return PromptRememberedPatron(callsid, from);
                }

                return PromptBarcode(callsid);
            }
            // Checks to see if the list of patron PINs contains an entry for the current callsid and prompts the user if needed

            if (!CallVars.PatronPINs.Keys.Contains(callsid))
            {
                return PromptPIN(callsid);
            }

            var output = HandleMenu(callsid, digits, node);
            return output;
        }

        public string PromptPIN(string callsid)
        {
            var twiml = new TwilioResponse();
            twiml.BeginGather(new { action = "/dialin/processpin", numDigits = AppSettings.PinDigits, timeout = 10 })
                    .Say("Please enter your pin", voiceAttribute);
            twiml.EndGather();
            twiml.Say("Sorry, I didn't get that.", voiceAttribute);
            twiml.Redirect("/dialin/index");
            return twiml.ToString();
        }

        public string PromptBarcode(string callsid)
        {
            var twiml = new TwilioResponse();
            // Prompts the user to enter their barcode and sends the input to be processed.
            // This adds the barcode to the list of barcodes and redirects the user back to
            // the index page to collect their PIN.

            // If the user does not enter a barcode they will be sent back to the index page 
            // and prompted again for their barcode.
            twiml.BeginGather(new { action = "/dialin/processbarcode" })
                .Say("Hello, thank you for calling the library. Please enter your library account barcode, followed by the pound key", voiceAttribute);
            twiml.EndGather();
            twiml.Say("Sorry, I didn't get that.", voiceAttribute);
            twiml.Redirect("/dialin/index");
            return twiml.ToString();
        }

        public string PromptRememberedPatron(string callsid, string from)
        {
            var twiml = new TwilioResponse();
            var tmpDialinPatron = db.DialinPatrons(Pred.Field<Dialin_Patrons>(p => p.PhoneNumber, "=", from)).Single();
            var tmpPatron = polaris.PatronBasicDataGet(tmpDialinPatron.Barcode);


            if (tmpPatron.Data.PAPIErrorCode != 0)
            {
                RemoveRememberedPatron(callsid, from);
                twiml.Redirect("/dialin/index");
            }
            else
            {
                twiml.BeginGather(new { action = "/dialin/processrememberedpatron", numDigits = 1 });
                twiml.Say(string.Format("Hello. Are you calling about {0}'s library account? Press 1 to confirm or press 2 to enter your library account barcode.", tmpPatron.Data.PatronBasicData.NameFirst.ToLower()), voiceAttribute);
                twiml.EndGather();
            }
            return twiml.ToString();
        }

        void RemoveRememberedPatron(string callsid, string from)
        {
            try
            {
                db.RemoveDialinPatron(from);
            }
            catch
            {
            }
            CallVars.PatronBarcodes.Remove(callsid);
            CallVars.PatronPINs.Remove(callsid);
        }

        public string HandleMenu(string callsid, int? digits, string node = "")
        {
            var twiml = new TwilioResponse();
            // If the user pressed a key at one of the menus then use that input to determine where 
            // they need to go
            int index = digits ?? 0;

            // Create the variable to determine where the user is going to end up
            string destination = "";

            // Checks to see if the node specified in the URL is a valid menu node
            if (menu.Keys.Contains(node))
            {
                // Send the user to the proper menu option, based on the number they pressed.
                //if (menu[node].Length >= index && digits.HasValue)
                //{
                //    if (menu[node].Length > digits.Value)
                //    {
                //        destination = menu[node][digits.Value];
                //    }
                //    else
                //    {
                //        destination = "mainmenu";
                //    }
                //}

                if (menu[node].Length > index)
                {
                    if (digits.HasValue)
                    {
                        destination = menu[node][index];
                    }
                    else
                    {
                        destination = "mainmenu";
                    }
                }
            }

            // Put the items out and holds into local variables to make them easier to use.
            //var itemsOut = CallVars.ItemsOut[callsid];
            //var holds = CallVars.Holds[callsid];
            var _patron = CallVars.PatronData[callsid];

            // Send the patron where they need to go.
            switch (destination)
            {
                // Connect the patron to their library.
                case "yourlibrary":
                    var libPhone = polaris.SA_GetValueByOrg("ORGPHONE1", CallVars.OtherPatronInfo[callsid].AssignedBranchID).Data.Value;

                    if (libPhone?.Length < 10)
                    {
                        twiml.Say($"We’re sorry, we were unable to connect you to {CallVars.OtherPatronInfo[callsid].AssignedBranchName}. Please visit the library’s website for more information.", voiceAttribute);
                        break;
                    }
                    else
                    {
                        twiml.Say("Connecting you to your library.", voiceAttribute);
                        twiml.Dial(ParsePhoneNumber(polaris.SA_GetValueByOrg("ORGPHONE1", CallVars.OtherPatronInfo[callsid].AssignedBranchID).Data.Value));
                    }
                    break;
                case "listallitems":
                    twiml.ListAllItems(callsid);
                    break;
                case "listitems":
                    twiml.BuildItemListMenu(callsid);
                    break;
                case "listoverdueitems":
                    twiml.ListOverdueItems(callsid);
                    break;
                case "emailitems":
                    twiml.EmailTitles(callsid);
                    break;
                case "itembybarcode":
                    twiml.ItemByBarcode(callsid);
                    break;
                case "renewall":
                    twiml.RenewAll(callsid);
                    break;
                case "listholds":
                    twiml.ListHolds(callsid);
                    break;
                case "nextHold":
                    twiml.NextHold(callsid);
                    break;
                case "cancelHold":
                    twiml.CancelHold(callsid);
                    break;
                case "confirmCancel":
                    twiml.ConfirmCancel(callsid);
                    break;
                case "renewItem":
                    twiml.RenewItem(callsid);
                    break;
                case "nextItem":
                    twiml.NextItem(callsid);
                    break;
                case "mainmenu":
                    twiml.BuildMainMenu(callsid);
                    break;
                default:
                    CallVars.ItemRenewResults.Remove(callsid);
                    if (CallVars.SkipIntro.Keys.Contains(callsid))
                    {
                        if (HandleTimeout(digits.ToString(), callsid))
                        {
                            return hangupResponse;
                        }
                    }
                    //if (HandleTimeout(digits.ToString(), callsid)) return hangupResponse;
                    switch (node)
                    {
                        case "default":
                            twiml.BuildMainMenu(callsid);
                            break;
                        case "listItems":
                            twiml.BuildItemListMenu(callsid);
                            break;
                        case "holdOptions":
                            twiml.ListHolds(callsid);
                            break;
                        default:
                            twiml.BuildMainMenu(callsid);
                            break;
                    }
                    break;
            }
            return twiml.ToString();
        }

        public string ProcessBarcode(string digits, string callsid)
        {
            if (HandleTimeout(digits, callsid)) return hangupResponse;
            if (AppSettings.EnableItivaTransfer)
            {
                if (!CallVars.TryItiva.Keys.Contains(callsid) && digits.Length == 10)
                {
                    var dpPatronCheckData = polaris.PatronValidate(digits);

                    if (dpPatronCheckData.Data.ValidPatron)
                    {
                        var patronOrg = polaris.OrganizationsGet(OrganizationType.All).Data.OrganizationsGetRows.Single(o => o.OrganizationID == dpPatronCheckData.Data.AssignedBranchID);
                        if (patronOrg?.ParentOrganizationID == 39)
                        {
                            //Transfer them to the iTiva system
                            return new TwilioResponse().Dial(AppSettings.ItivaNumber).ToString();
                        }
                    }

                    CallVars.TryItiva[callsid] = false;
                }
            }
            CallVars.PatronBarcodes[callsid] = digits;
            return new TwilioResponse().Redirect("/dialin/index").ToString();
        }

        public string ProcessPin(string digits, string callsid)
        {
            if (HandleTimeout(digits, callsid)) return hangupResponse;
            CallVars.PatronPINs[callsid] = digits;
            return new TwilioResponse()
                .Say("One moment while I look up your account.", voiceAttribute)
                .Redirect("/dialin/lookupaccount").ToString();
        }

        public string LookupAccount(string callsid, string from)
        {
            var twiml = new TwilioResponse();
            // Grabs the patron barcode for the current call
            var barcode = CallVars.PatronBarcodes[callsid];
            // Grabs the patron PIN for the current call
            var pin = CallVars.PatronPINs[callsid];

            //logger.Trace("CallSid: {0} | Barcode: {1} | Pin: {2}", callsid, barcode, pin);         

            // Gets the patron information for the patron on the current call
            var patron = polaris.PatronBasicDataGet(barcode, pin).Data;

            // Get the patron's registered branch because Polaris is dumb and doesn't
            // include it in PatronBasicDataGet
            var otherPatronInfo = polaris.PatronValidate(barcode, pin).Data;

            // Look and see if we were able to successfully get the information for the specified
            // patron information.
            if (patron?.PatronBasicData == null || !(otherPatronInfo?.ValidPatron ?? false))
            {
                // If we were unable to get a patron from Polaris with the specified barcode and PIN
                // then remove the barcode and PIN for the current call
                CallVars.PatronBarcodes.Remove(callsid);
                CallVars.PatronPINs.Remove(callsid);

                // Send the patron back to the index to be prompted for their barcode and PIN again
                twiml.Say("Incorrect barcode or pin", voiceAttribute);
                twiml.Redirect("/dialin/index");
                return twiml.ToString();
            }            

            if (otherPatronInfo == null)
            {
                try
                {
                    logger.Trace(JsonConvert.SerializeObject(patron));
                }
                catch { }

                twiml.Say("Incorrect barcode or pin", voiceAttribute);
                twiml.Redirect("/dialin/index");
                return twiml.ToString();
            }

            var branchName = otherPatronInfo.AssignedBranchName;

            if (!branchName.ToLower().Contains("librar") && !branchName.ToLower().Contains("branch"))
            {
                otherPatronInfo.AssignedBranchName += " library";
            }

            SetRememberedPatron(barcode, from);
            LogUsage(patron.PatronBasicData.PatronID, otherPatronInfo.AssignedBranchID);

            CallVars.PatronData[callsid] = patron.PatronBasicData;
            CallVars.OtherPatronInfo[callsid] = otherPatronInfo;
            Dialin.RefreshItemsOut(callsid);
            CallVars.ItemType[callsid] = ItemType.All;

            return new TwilioResponse().Redirect("/dialin/index").ToString();
        }

        void SetRememberedPatron(string barcode, string phonenumber)
        {
            var patrons = db.DialinPatrons(Pred.Field<Dialin_Patrons>(p => p.PhoneNumber, "=", phonenumber));
            if (!string.IsNullOrWhiteSpace(phonenumber) || !patrons.Any())
            {
                var foo = db.RemoveDialinPatron(phonenumber);
                db.AddDialinPatron(barcode, phonenumber);
            }
        }

        public string Test(string key)
        {
            if (key != AppSettings.AuthString) { return ""; }

            CallVars.PatronBarcodes["foo"] = "21870002169440";
            CallVars.PatronPINs["foo"] = "1234";
            LookupAccount("foo", "+14405446504");

            return "test";
        }

        void LogUsage(int patronId, int branchId)
        {
            db.LogDialinUsage(patronId, branchId);
        }

        public string RenewItemByBarcode(string digits, string callsid)
        {
            var twiml = new TwilioResponse();

            var itemBarcode = digits;

            var item = db.GetCheckedOutItems(CallVars.PatronData[callsid].PatronID).SingleOrDefault(i => i.Barcode == itemBarcode);


            if (item == null)
            {
                twiml.Say("The barcode you entered is not valid. Please try again.", voiceAttribute);
                twiml.Redirect("/dialin/index?node=listItems&digits=3");
                return twiml.ToString();
            }

            var result = polaris.ItemRenew(CallVars.PatronBarcodes[callsid], item.ItemRecordID, CallVars.PatronPINs[callsid]).Data;

            if (result.PAPIErrorCode == 0)
            {
                twiml.Say(string.Format("Successfully renewed {0}. The new due date is {1}. Returning to items menu.", TwilioResponseExtension.FormatTitle(item.BrowseTitle), result.ItemRenewResult.DueDateRows.Single().DueDate.ToString("dddd MMMM dd")), voiceAttribute);
                twiml.Redirect("/dialin/index?node=listItems");
                return twiml.ToString();
            }

            var error = result.ItemRenewResult.BlockRows.FirstOrDefault()?.ErrorDesc;
            if (string.IsNullOrWhiteSpace(error))
            {
                error = "Unknown reason. Please try again or contact your library for more information.";
            }
            twiml.Say(string.Format("Unable to renew {0} for the following reason: {1}. Returning to items menu.", TwilioResponseExtension.FormatTitle(item.BrowseTitle), error), voiceAttribute);
            twiml.Say(error, voiceAttribute);
            twiml.Redirect("/dialin/index?node=listItems");


            return twiml.ToString();
        }

        public string ProcessRememberedPatron(string digits, string callsid, string from)
        {
            if (HandleTimeout(digits, callsid)) return new TwilioResponse().Hangup().ToString();

            if (digits == "1")
            {
                CallVars.PatronBarcodes[callsid] = db.GetDialinPatron(from).Barcode;
            }
            else
            {
                db.RemoveDialinPatron(from);
            }

            return new TwilioResponse().Redirect("/dialin/index").ToString();
        }

        public bool HandleTimeout(string digits, string callsid)
        {
            // If the user doesn't have an entry in the timeout dictionary add one.
            if (!CallVars.Timeout.Keys.Contains(callsid))
            {
                CallVars.Timeout.Add(callsid, -1);
            }

            // If the user didn't enter a value then increment their value
            if (string.IsNullOrWhiteSpace(digits))
            {
                CallVars.Timeout[callsid]++;
                // If the user is at or over the limit then hang up the call.
                if (CallVars.Timeout[callsid] >= AppSettings.TimeoutLimit)
                {
                    return true;
                }
                return false;
            }
            // If the user did enter something then reset their value
            else
            {
                CallVars.Timeout.Remove(callsid);
                return false;
            }
        }

        string ParsePhoneNumber(string input)
        {
            var pn = "";
            foreach (var c in input)
            {
                if (char.IsDigit(c)) pn += c;
                if ((pn.StartsWith("1") && pn.Length == 11) || (!pn.StartsWith("1") && pn.Length == 10)) return pn;
            }
            return "";
        }
    }
}
