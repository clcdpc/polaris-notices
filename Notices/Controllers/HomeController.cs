﻿using Notices.Core;
using Notices.Core.Models;
using Notices.Core.Data;
using Notices.Models;
using Notices.Services;
using Notices.Services.Email;
using Notices.Services.Postmark;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using Notices.Extensions;
using System.Data.SqlClient;
using Dapper;
using NLog;
namespace Notices.Controllers
{
    public class HomeController : Controller
    {
        public string test(string auth)
        {
            if (auth != AppSettings.AuthString) return "";


            return "test";
        }

        // GET: Home
        public string Index()
        {
            
            return "home";
        }
    }
}