﻿using Clc.Auth;
using Newtonsoft.Json;
using Notices.Core;
using Notices.Services;
using Notices.Services.Postmark;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace Notices.Controllers
{
    [ClcAuthorize]
    public class RejectionsController : Controller
    {
        [HttpGet]
        public ActionResult Remove()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Remove(string address)
        {
            if (AppSettings.EmailProvider == "postmark")
            {
                var postmark = new PostmarkClient(AppSettings.PostmarkDbKey);

                var model = new RejectionResultViewModel();

                if ((postmark.GetSuppressions(address).Suppressions?.Any()).GetValueOrDefault())
                {
                    var response = postmark.RemoveSuppression(address);
                    model.Success = string.Equals(response.Suppressions.FirstOrDefault().Status, "deleted", StringComparison.OrdinalIgnoreCase);
                    model.Message = string.Equals(response.Suppressions.FirstOrDefault().Status, "deleted", StringComparison.OrdinalIgnoreCase) ? "Address removed from rejection list" : "Removal failed. If you continue to get this error please open a helpdesk ticket";
                }
                else
                {
                    model.Success = false;
                    model.Message = "Address not found on rejection list";
                }

                return View("RemovalResult", model);
            }

            if (!Mandrill.CheckRejection(address))
            {
                return View("RemovalResult", new RejectionResultViewModel { Success = false, Message = "Address does not exist in rejection list." });
            }

            var result = Mandrill.RemoveRejection(address);
            if (Mandrill.RemoveRejection(address))
            {
                return View("RemovalResult", new RejectionResultViewModel { Success = true, Message = address + " successfully deleted" });
            }
            else
            {
                return View("RemovalResult", new RejectionResultViewModel { Success = false, Message = $"Error removing {address} from rejection list" });
            }
        }
    }
}
