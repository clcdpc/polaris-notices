﻿using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Core.EntityClient;
using System.Linq;
using System.Net;
using System.Resources;
using System.Web;
using System.Web.Mvc;
using clc_twilio_csharp;
using Notices.Extensions;
using Notices.Models;
using Notices.Core.Data;
using Notices.Services;
using Notices.Core;
using Clc.Polaris.Api.Models;
using Twilio.TwiML;
using System.Net.Http;
using Notices.Core.Models;
using System.Text;
using RabbitMQ.Client;
using Notices.Security;
using System.IO;
using Newtonsoft.Json.Linq;
using clc_telnyx_csharp.Models;
using System.Data.SqlClient;
using Dapper;
using clc_telnyx_csharp;
using Clc.Auth;

namespace Notices.Controllers
{
    [SessionState(System.Web.SessionState.SessionStateBehavior.Disabled)]
    public class SmsController : Controller
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        static TwilioClient twilio = Methods.CreateTwilioClient();
        static TelnyxClient telnyx = new TelnyxClient(AppSettings.TelnyxV2ApiKey);

        public SmsController()
        {
        }

        public ActionResult TelnyxSmsWebhook(string auth)
        {
            if (!string.Equals(auth, AppSettings.AuthString)) { return Content("invalid key"); }

            var conn = new SqlConnection(DbHelper.ConnectionString);
            try
            {
                var json = Request.GetBody();
                var webhook = JsonConvert.DeserializeObject<TelnyxSmsWebhook>(json);


                foreach (var to in webhook.data.payload.to)
                {
                    var status = to.status.ToLower();
                    var message = "";

                    switch (status)
                    {
                        case "queued":
                            break;
                        case "sending":
                            break;
                        case "sent":
                            break;
                        case "expired":
                            break;
                        case "sending_failed":
                            message = string.Join("; ", webhook.data.payload.errors?.Select(e => $"{e.code} - {e.detail}"));
                            break;
                        case "delivery_unconfirmed":
                            break;
                        case "delivered":
                            break;
                        case "delivery_failed":
                            message = string.Join("; ", webhook.data.payload.errors?.Select(e => $"{e.code} - {e.detail}"));
                            break;
                    }

                    var sql = "insert into TelnyxSmsWebhooks (Id,Timestamp,[To],Status,Message) values (@id, @timestamp, @phone_number, @status, @message)";
                    conn.Execute(sql, new { webhook.data.payload.id, timestamp = DateTime.Now, to.phone_number, Status = to.status, message });

                    sql = @"
                        update CLC_Notices.dbo.TelnyxSmsMessages
                        set Status = @status
                        where Id = @id
                    ";
                    conn.Execute(sql, new { webhook.data.payload.id, to.status });
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            finally
            {
                conn.Dispose();
            }

            return Content("");
        }

        [HttpPost]
        public string Oplin()
        {
            try
            {
                var webhook = Request.GetJsonBodyAs<OplinSmsWebhookBody>();
                LogManager.GetLogger("oplin.sms").Log(LogLevel.FromString(webhook.LogLevel), webhook.ToString());

                if (!webhook.IsSuccess)
                {
                    if (new[] { "MPME0601", "MPME1204" }.Contains(webhook.Status, StringComparer.OrdinalIgnoreCase))
                    {

                    }
                    else if (webhook.Status.IndexOf("willretry", StringComparison.OrdinalIgnoreCase) == -1)
                    {
                        SmsHelper.RemovePatronSmsDetails(webhook.PatronPhone, $"Phone number {webhook.PatronPhone} is undeliverable for SMS: {webhook.Status}");
                    }
                }
            }
            catch (Exception ex) { logger.Error(ex); }

            return "";
        }

        [ValidateInput(false)]
        public ActionResult Process(FormCollection fc, string auth)
        {
            if (auth != AppSettings.AuthString)
            {
                return Content("invalid request");
            }

            if (!AppSettings.EnableSmsProcessing) return Content("sms processing disabled in config");

            if (AppSettings.EmailProvider.Equals("postmark", StringComparison.OrdinalIgnoreCase))
            {
                var bodyStream = new StreamReader(Request.InputStream);
                bodyStream.BaseStream.Seek(0, SeekOrigin.Begin);
                var bodyText = bodyStream.ReadToEnd();
                var body = JsonConvert.DeserializeObject<PostmarkInboundMessage>(bodyText);

                string to = body.ToFull[0].Email.Split('@')[0];

                var db = new DbHelper(AppSettings.DbServer);
                string fromDomain = body.FromFull.Email.Split('@')[1];
                var org = db.GetLibraryFromEmailDomain(fromDomain);

                string message = body.TextBody.Replace("\r", "").Replace("\n", "");

                if (string.IsNullOrWhiteSpace(to) || string.IsNullOrWhiteSpace(message)) return Content("invalid request");


                SMS.SendMessage(to, message, org);
                return Content("message sent");
            }

            if (!fc.AllKeys.Contains("mandrill_events"))
            {
                return Content("invalid request");
            }

            //logger.Trace(fc["mandrill_events"]);
            SMS.SendMessages(fc.GetEvents());

            return Content("message processed");
        }

        [ClcAuthorize]
        public ActionResult Manual()
        {
            return View();
        }

        [ClcAuthorize]
        [HttpPost]
        public ActionResult Manual(string phoneNumber, string message)
        {
            var twilio = Methods.CreateTwilioClient();
            var response = twilio.SendMessageWithService(phoneNumber, message, AppSettings.MessageServiceSid);
            var msg = "";
            if (response.Response.IsSuccessStatusCode)
            {
                if (response.Data.ErrorCode.HasValue)
                {
                    msg = $"Failed to send message: {response.Data.ErrorCode} - {response.Data.ErrorMessage}";
                }
                else
                {
                    msg = "Message sent successfully";
                }
            }

            return View(new ManualSmsViewModel { Info = msg });
        }

        [ValidateInput(false)]
        public ActionResult ProcessMandrill(FormCollection fc, string auth)
        {
            if (!AppSettings.EnableSmsProcessing) return Content("sms processing disabled in config");

            if (!fc.AllKeys.Contains("mandrill_events") || auth != AppSettings.AuthString)
            {
                return Content("invalid request");
            }

            //logger.Trace(fc["mandrill_events"]);
            SMS.SendMessages(fc.GetEvents());

            return Content("message processed");
        }

        public string Test(string auth)
        {
            if (!AppSettings.EnableSmsProcessing) return "sms processing disabled in config";

            if (auth != AppSettings.AuthString)
            {
                return "Invalid auth string";
            }

            SMS.SendTestMessage();

            return "test";
        }


        [ClcAuthorize]
        public ActionResult ListNumbers()
        {
            var twilioResponse = twilio.GetMessageServiceNumbers(AppSettings.MessageServiceSid);
            var twilioNumbers = twilioResponse.Data.phone_numbers.Select(n => n.phone_number).ToList();

            var telnyxNumbers = telnyx.GetMessagingServicePhoneNumbers(AppSettings.MessagingProfileId)?.Data?.PhoneNumbers;

            var numbers = twilioNumbers.Concat(telnyxNumbers).OrderBy(n => n).ToList();

            return View(numbers);
        }

        [ClcAuthorize(Roles = "Admin")]
        public string TestAfterHours()
        {
            var factory = new ConnectionFactory
            {
                HostName = AppSettings.RabbitMqHostname,
                VirtualHost = AppSettings.RabbitMqVirtualHost,
                UserName = AppSettings.RabbitMqUsername,
                Password = AppSettings.RabbitMqPassword
            };

            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(
                    queue: Queues.AFTER_HOURS_MESSAGES,
                    durable: true,
                    exclusive: false,
                    autoDelete: false,
                    arguments: null);

                var json = JsonConvert.SerializeObject(new SmsMessage { To = AppSettings.SmsTestNumber, Body = "test after hours message" });
                var body = Encoding.UTF8.GetBytes(json);

                IBasicProperties props = channel.CreateBasicProperties();
                props.ContentType = "text/plain";
                props.DeliveryMode = 2;

                channel.BasicPublish("", Queues.AFTER_HOURS_MESSAGES, props, body);
            }

            SMS.SendAfterHours();

            return "testafterhours";
        }


        public string ProcessInbound(string auth, string from, string body, string service = "twilio")
        {
            if (auth != AppSettings.AuthString)
            {
                return "";
            }

            ISmsSender smsSender;

            switch (service)
            {
                case "twilio":
                    smsSender = new TwilioSmsSender();
                    break;
                case "telnyx":
                    smsSender = new TelnyxSmsSender();
                    break;
                case "oplin":
                    smsSender = new OplinSmsSender();
                    break;
                default:
                    logger.Error($"Invalid sms sender {service}");
                    throw new ArgumentException($"Invalid sms sender {service}");
            }

            var inboundMessage = smsSender.ProcessInbound(Request);

            var replyText = AppSettings.SmsReplyText;

            if (AppSettings.SmsStartTriggerWords.Any(w => string.Equals(body, w, StringComparison.OrdinalIgnoreCase)))
            {
                replyText = AppSettings.SmsStartReplyText;
            }
            else if (!AppSettings.SmsStopTriggerWords.Any(w => string.Equals(body, w, StringComparison.OrdinalIgnoreCase)))
            {
                replyText = AppSettings.SmsReplyText;
            }
            else
            {
                logger.Info("{0} opted out", from);

                var pn = from.Replace("+1", "");

                var note = !string.IsNullOrWhiteSpace(AppSettings.SmsStopNoteIdentifier)
                            ? $"{AppSettings.SmsStopNoteIdentifier} - Phone number {pn} opted-out."
                            : $"Phone number {pn} opted-out.";

                var count = 0;

                try
                {
                    count = SmsHelper.RemovePatronSmsDetails(pn, note, true);

                    if (count == 0)
                    {
                        replyText = AppSettings.SmsStopZeroAccountsReplyText;
                        //return new TwilioResponse().Message(AppSettings.SmsStopZeroAccountsReplyText).ToString();
                    }
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                }

                replyText = string.Format(AppSettings.SmsStopReplyText, count);
            }

            return smsSender.ProcessReply(new SmsMessage { To = inboundMessage.From, Body = replyText });

        }

        public string TelnyxTransferToDialin(string auth)
        {
            return auth == AppSettings.AuthString ? "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Response><Dial><Number>+18777726657</Number></Dial></Response>" : "";
        }

        public string Status(int patronId = 0, string itemIds = "")
        {
            var status = Request.Form["SmsStatus"];

            if (string.Equals(status, "undelivered", StringComparison.OrdinalIgnoreCase))
            {
                var errorCode = Request.Form["ErrorCode"];

                // don't remove sms details where the failure was a carrier violation
                if (string.Equals(errorCode, "30007"))
                {
                    Methods.HandlePrtgSensor(AppSettings.TwilioCarrierViolationErrorSensorUrl);
                }
                else
                {
                    var to = Request.Form["to"].Replace("+1", "");
                    SmsHelper.RemovePatronSmsDetails(to, $"Phone number {to} is undeliverable");
                }
            }

            return "";
        }
    }
}