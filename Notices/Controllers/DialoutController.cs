﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Notices.Models;
using Notices;
using clc_twilio_csharp;
using Notices.Extensions;
using Clc.Polaris.Api;
using Notices.Core.Data;
using Twilio.TwiML;

using System.Resources;
using Notices.Services;
using NLog;
using System.Web.Script.Serialization;
using Clc.Auth;

namespace Notices.Controllers
{
    public class DialoutController : Controller
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        Notifier notifier = new Notifier("Dialout");

        public DialoutController()
        {

        }

        public ActionResult DoError()
        {
            return new HttpStatusCodeResult(500);
        }

        public string TwilioResponse(string callsid, int patronId, int[] items = null, string answeredby = "human", bool testing = false)
        {
            logger.Info("Sid: {0} | patronId: {1}", callsid, patronId);
            try
            {
                return Dialout.BuildMessage(callsid, answeredby, patronId, items, testing);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return new TwilioResponse().Say(Internal.FallbackPhoneMessage, new { voice = "alice" }).ToString();
            }
        }

        public string FallbackResponse(int patronId)
        {
            notifier.Notify($"Using fallback response for patron {patronId}.", NotificationMethod.All);

            try
            {
                var notices = Dialout.Notifications.Where(n => n.PatronID == patronId);
                logger.Error($"Using fallback for patron {patronId}.\r\n{new JavaScriptSerializer().Serialize(notices)}");
            }
            catch { }

            return Dialout.FallbackResponse();
        }

        public ActionResult TwilioStatus(string callSid, string callStatus, string to, int patronId, int[] items = null, bool testing = false)
        {
            var result = Dialout.PostNotices(patronId, callSid, callStatus, items);
            LogManager.GetLogger("Call." + callSid).Debug("TwilioStatus return value: {0}", result.Message);
            return Content(result.Message);
        }

        public ActionResult ListNumbers()
        {
            var twilio = Methods.CreateTwilioClient();
            var allNumbers = twilio.GetOutgoingCallerIds();
            var dialoutNumbers = allNumbers.Data.outgoing_caller_ids.Where(o => !o.friendly_name.StartsWith("zzz")).Select(o => o.phone_number).OrderBy(n => n).ToList();

            return View(dialoutNumbers);
        }

        [ClcAuthorize]
        public string TestTwiml()
        {
            return Dialout.GenerateTestTwiml();
        }
    }
}
