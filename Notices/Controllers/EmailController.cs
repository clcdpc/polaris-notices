﻿using Newtonsoft.Json;
using Notices.Core.Data;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Resources;
using System.Web;
using System.Web.Mvc;
using Notices.Extensions;
using Notices.Resources;
using System.Globalization;
using System.Collections;
using Notices.Models;
using Notices.Services;
using Clc.Polaris.Api;
using Notices.Security;
using Notices.Core;
using Clc.Polaris.Api.Models;
using System.Web.Script.Serialization;
using Notices.Core.Models;
using NLog;
using Notices.Services.Email;
using Hangfire;
using System.IO;
using Newtonsoft.Json.Linq;
using Clc.Auth;

namespace Notices.Controllers
{
    public class EmailController : Controller
    {
        DbHelper db;
        PapiClient papi;
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public EmailController()
        {
            db = new DbHelper(AppSettings.DbServer);
            papi = Methods.CreatePapiClient();
        }

        public ActionResult Index()
        {
            return Content("");
        }

        [ClcAuthorize(Roles ="Admin")]
        public string ViewExpirationReminderPatrons()
        {
            var notices = db.GetEmailNotices(new[] { NotificationType.ExpirationReminder }).Where(n => DbSettings.EmailEnableExpirationNotices(n.PatronBranch));

            return string.Join("\r\n", notices.Select(n => n.PatronID).Distinct());

        }

        public string SendNotices(NotificationType[] noticeTypes, string auth)
        {
            if (auth == AppSettings.AuthString)
            {
                if (!Methods.GetEmailSender().UseBackgroundJob || AppSettings.Dev)
                {
                    Email.SendNotices(noticeTypes);
                }
                else
                {
                    BackgroundJob.Enqueue(() => Email.SendNotices(noticeTypes, null));
                }

                return $"Sent {string.Join(", ", noticeTypes.Select(t => t.ToString()))}";
            }
            return "";
        }

        [ClcAuthorize(Roles ="Admin")]
        public ActionResult TestBounce(string email, string subject = "test subject", string sender = "sender@mail.com", string eventType = "HardBounce", string eventDescription = "")
        {
            if (!AppSettings.EnableEmailProcessing) return Content("email processing disabled in config");

            //var events = new List<MandrillMailEvent> { new MandrillMailEvent { Event = "hard_bounce", Msg = new Message { Subject = subject, Sender = sender, Email = email } } };
            //var fc = new FormCollection() { { "mandrill_events", new JavaScriptSerializer().Serialize(events) } };
            //ProcessFailure(fc, AppSettings.AuthString);

            var emailEvent = new EmailEvent
            {
                To = email,
                From = sender,
                Subject = subject,
                EventType = eventType,
                Description = eventDescription
            };

            var events = new List<EmailEvent> { emailEvent };
            ProcessFailedEmailEvents(events);

            return Content("test bounce processed");
        }

        [ClcAuthorize(Roles ="Admin")]
        public ActionResult TestSpam(string email, string subject = "test subject", string sender = "sender@mail.com")
        {
            if (!AppSettings.EnableEmailProcessing) return Content("email processing disabled in config");

            var emailEvent = new EmailEvent
            {
                To = email,
                From = sender,
                Subject = subject,
                EventType = "SpamComplaint",
                Description = ""
            };

            var events = new List<EmailEvent> { emailEvent };
            ProcessFailedEmailEvents(events);

            return Content("test spam processed");
        }

        [ClcAuthorize(Roles ="Admin")]
        public ActionResult TestUnsub(string barcode)
        {
            if (!AppSettings.EnableEmailProcessing) return Content("email processing disabled in config");

            ProcessUnsubscribe(barcode);
            return Content("test unsub processed");
        }

        [HttpGet]
        public ActionResult Unsubscribe()
        {
            return View(new UnsubscribeViewModel());
        }

        [HttpPost]
        public ActionResult Unsubscribe(UnsubscribeViewModel model)
        {
            if (!ModelState.IsValid) return View();
            var polaris = Methods.CreatePapiClient();

            if ((!polaris.PatronValidate(model.Barcode, model.Pin)?.Data?.ValidPatron) ?? false)
            {
                ModelState.AddModelError("", "Invalid barcode or pin");
                return View();
            }

            ProcessUnsubscribe(model.Barcode);

            return View("Unsubscribed");
        }

        [HttpGet]
        public ActionResult ViewTestNoticeEmail()
        {
            return View();
        }

        [ClcAuthorize]
        public ActionResult ViewExample()
        {
            var noticeTypes = new List<NotificationType> {
                            NotificationType.AlmostOverdueReminder,
                            NotificationType.FirstOverdue,
                            NotificationType.SecondOverdue,
                            NotificationType.ThirdOverdue,
                            NotificationType.Hold,
                            NotificationType.SecondHold,
                            NotificationType.ExpirationReminder,
                            NotificationType.Cancel
                        };

            var preds = new[]
            {
                Pred.Field<PolarisOrganization>(o=>o.OrganizationCodeID, "=", 3),
                Pred.Field<PolarisOrganization>(o=>o.Name, "not like", "z%")
            };

            var model = new ExampleEmailNoticeViewModel
            {
                AllOrganizations = db.PolarisOrganizations().ToList(),
                AvailableNotificationTypes =
                    new List<NotificationType> {
                            NotificationType.AlmostOverdueReminder,
                            NotificationType.FirstOverdue,
                            NotificationType.SecondOverdue,
                            NotificationType.ThirdOverdue,
                            NotificationType.Hold,
                            NotificationType.SecondHold,
                            NotificationType.ExpirationReminder,
                            NotificationType.Cancel
                    },
                NoticeTypeTest = new SelectList(noticeTypes.Select(nt => new SelectListItem { Text = nt.ToString(), Value = ((int)nt).ToString() }), "Value", "Text"),
                PolarisBranches = new SelectList(db.PolarisOrganizations(preds).OrderBy(o => o.Name).Select(o => new SelectListItem { Text = o.Name, Value = o.OrganizationID.ToString() }).ToList(), "Value", "Text")
            };


            return View(model);
        }



        [ClcAuthorize]
        public ActionResult Example(NotificationType? noticeType = null, int[] reportingOrgIds = null, int? patronBranchId = null, EmailFormat format = EmailFormat.Html)
        {
            reportingOrgIds = reportingOrgIds ?? new int[0];

            if (!noticeType.HasValue || !reportingOrgIds.Any() || !patronBranchId.HasValue) { return Content(""); }

            var notices = PolarisNotification.GenerateTest(new[] { noticeType.Value }, DeliveryOption.EmailAddress, patronBranchId.Value, reportingOrgIds);
            var email = Email.GenerateNoticeEmail(notices);
            return Content(format == EmailFormat.PlainText ? email.TextBody : email.HtmlBody, $"text/{format.ToString().ToLower()}");
        }

        [ClcAuthorize]
        public string RunNoticeTests(int patronBranch, int[] reportingBranchIds, NotificationType[] noticeTypes = null)
        {
            Email.RunTests(patronBranch, reportingBranchIds, noticeTypes, new OutputToFileEmailSender());
            return "done";
        }

        int GetLibrary(string emailDomain)
        {
            var orgId = db.GetLibraryFromEmailDomain(emailDomain);
            return orgId;
        }

        List<EmailEvent> GetMandrillEvents(FormCollection fc)
        {
            return fc.GetEvents()
                .Select(e => new EmailEvent
                {
                    To = e.Msg.Email,
                    From = e.Msg.Sender,
                    Subject = e.Msg.Subject,
                    EventType = e.Event,
                    Description = e.Msg.BounceDescription
                }).ToList();
        }

        List<EmailEvent> GetPostmarkEvents()
        {
            var bodyStream = new StreamReader(Request.InputStream);
            bodyStream.BaseStream.Seek(0, SeekOrigin.Begin);
            var bodyText = bodyStream.ReadToEnd();
            dynamic body = JObject.Parse(bodyText);

            var emailEvent = new EmailEvent
            {
                To = body.Email,
                From = body.From,
                Subject = body.Subject,
                EventType = body.Type,
                Description = body.Description
            };

            return new List<EmailEvent> { emailEvent };
        }

        [ValidateInput(false)]
        public ActionResult ProcessFailure(FormCollection fc, string auth)
        {
            if (!AppSettings.EnableEmailProcessing) return Content("email processing disabled in config");

            if (auth != AppSettings.AuthString) { return Content("invalid request"); }

            var events = new List<EmailEvent>();

            if (AppSettings.EmailProvider == "mandrill" || Request.UserAgent.ToLower().Contains("mandrill"))
            {
                if (!fc.AllKeys.Contains("mandrill_events"))
                {
                    logger.Trace("invalid request");
                    return Content("invalid request");
                }

                events = GetMandrillEvents(fc);
            }

            try
            {
                if (AppSettings.EmailProvider == "postmark")
                {
                    events = GetPostmarkEvents();
                    if (events.Any(e => string.IsNullOrWhiteSpace(e.From))) return Content("invalid request");
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }


            if (ProcessFailedEmailEvents(events))
            {
                return new HttpStatusCodeResult((int)HttpStatusCode.InternalServerError);
            }

            return new HttpStatusCodeResult((int)HttpStatusCode.OK);
        }

        bool ProcessFailedEmailEvents(List<EmailEvent> events)
        {
            var exception = false;

            foreach (var ev in events)
            {
                logger.Trace("Processing message | To: {0} | From: {1} | Subject: {2} | Event: {3} | Description: {4}", ev.To, ev.From, ev.Subject, ev.EventType, ev.Description);

                try
                {
                    if (AppSettings.EmailProcessingExcludedWords.Any(w => ev.Subject.IndexOf(w, StringComparison.OrdinalIgnoreCase) > -1))
                    {
                        logger.Trace("Matched excluded word. To: {0} | From: {1} | Subject: {2}", ev.To, ev.From, ev.Subject);
                        continue;
                    }

                    if (AppSettings.EmailProcessingExcludedSenders.Any(w => string.Equals(w, ev.From, StringComparison.OrdinalIgnoreCase)))
                    {
                        logger.Trace("Matched excluded sender. To: {0} | From: {1} | Subject: {2}", ev.To, ev.From, ev.Subject);
                        continue;
                    }

                    if (AppSettings.EmailProcessingExcludedReceivers.Any(w => string.Equals(w, ev.To, StringComparison.OrdinalIgnoreCase)))
                    {
                        logger.Trace("Matched excluded receiver. To: {0} | From: {1} | Subject: {2}", ev.To, ev.From, ev.Subject);
                        continue;
                    }

                    var _eventTypeId = db.GetEmailEventTypeId(ev.EventType);

                    if (_eventTypeId.HasValue == false)
                    {
                        logger.Error("UNKNOWN BOUNCE TYPE: {0}", ev.EventType);
                        Methods.HandlePrtgSensor(AppSettings.UnknownBounceSensorUrl);
                        continue;
                    }

                    Enum.TryParse(_eventTypeId.Value.ToString(), out EmailEventType eventType);

                    switch (eventType)
                    {
                        case EmailEventType.HardBounce:
                            ProcessBounce(ev.To);
                            break;
                        case EmailEventType.SoftBounce:
                            if (ev.Description.ToLower().Contains("general"))
                            {
                                goto case EmailEventType.HardBounce;
                            }
                            continue;
                        case EmailEventType.SpamComplaint:
                            ProcessSpam(ev.To, ev.From);
                            break;
                        case EmailEventType.Blocked:
                            Methods.HandlePrtgSensor(AppSettings.PostmarkIspBlockSensorUrl);
                            break;
                        case EmailEventType.Ignore:
                            break;
                        default:
                            logger.Error("UNKNOWN BOUNCE TYPE: {0}", ev.EventType);
                            Methods.HandlePrtgSensor(AppSettings.UnknownBounceSensorUrl);
                            break;
                    }
                }
                catch (Exception ex)
                {
                    exception = true;
                    logger.Error("Error JSON: \r\n{0}", JsonConvert.SerializeObject(ev));
                    logger.Error(ex);
                }

                logger.Trace("Finished {0}", ev.To);
            }

            return exception;
        }


        GetPatronsForEmail_Result ProcessPatron(GetPatronsForEmail_Result patron, string email, RecordSetTypes recordSetType, string note)
        {
            if (string.IsNullOrWhiteSpace(email)) return patron;

            var rsid = db.GetRecordSet(patron.LibraryID.Value, recordSetType).RecordSetID;
            papi.RecordSetContentAdd(rsid, patron.PatronID);

            RemoveEmail(patron, email);
            db.AddPatronNote(patron.PatronID, note);
            return patron;
        }

        void ProcessBounce(string email)
        {
            try
            {
                var patrons = db.GetPatronsForEmail(email).ToList();
                foreach (var p in patrons)
                {
                    ProcessPatron(p, email, RecordSetTypes.Bounce, string.Format("{0} - Address {1} bounced.", DateTime.Now.ToShortDateString(), email));
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
        }

        void ProcessSpam(string email, string from)
        {
            var patrons = new List<GetPatronsForEmail_Result>();

            if (AppSettings.LibrarySpecificSpam)
            {
                var library = GetLibrary(from.Split('@')[1]);
                patrons = db.GetPatronsForEmail(email).Where(p => p.LibraryID == library || library == 0).ToList();
            }
            else
            {
                patrons = db.GetPatronsForEmail(email).ToList();
            }

            foreach (var p in patrons)
            {
                ProcessPatron(p, email, RecordSetTypes.Spam, string.Format("{0} - Address {1} reported spam from {2}.", DateTime.Now.ToShortDateString(), email, from));
            }

            if (AppSettings.EnableSpamRejectionRemoval && AppSettings.EmailProvider == "mandrill")
            {
                Mandrill.RemoveRejection(email);
            }
        }

        void ProcessUnsubscribe(string barcode)
        {
            var patronData = papi.PatronBasicDataGet(barcode).Data.PatronBasicData;
            var patron = db.GetPatronsForEmail(patronData.EmailAddress).Single(p => string.Equals(p.Barcode, barcode, StringComparison.OrdinalIgnoreCase));

            ProcessPatron(patron, patronData.EmailAddress, RecordSetTypes.Spam, string.Format("{0} - Address {1} unsubscribed.", DateTime.Now.ToShortDateString(), patronData.EmailAddress));
            ProcessPatron(patron, patronData.AltEmailAddress, RecordSetTypes.Spam, string.Format("{0} - Address {1} unsubscribed.", DateTime.Now.ToShortDateString(), patronData.AltEmailAddress));
        }

        GetPatronsForEmail_Result RemoveEmail(GetPatronsForEmail_Result patron, string email)
        {
            var patronData = papi.PatronBasicDataGet(patron.Barcode).Data.PatronBasicData;
            var updateParams = new PatronUpdateParams();// { EReceiptOptionID = 0 };

            if (string.Equals("Yes", papi.SA_GetValueByOrg("PAC_PATRON_ERECEIPTS_ENABLE", patron.BranchID)?.Data?.Value, StringComparison.OrdinalIgnoreCase))
            {
                updateParams.EReceiptOptionID = 0;
            }

            if (string.Equals(patronData.EmailAddress, email, StringComparison.OrdinalIgnoreCase))
            {
                updateParams.EmailAddress = "";
            }

            if (string.Equals(patronData.AltEmailAddress, email, StringComparison.OrdinalIgnoreCase))
            {
                updateParams.AltEmailAddress = "";
            }

            if ((updateParams.EmailAddress == "" || string.IsNullOrWhiteSpace(patronData.EmailAddress)) && (updateParams.AltEmailAddress == "" || string.IsNullOrWhiteSpace(patronData.AltEmailAddress)))
            {
                var smsDeliveryPhone = patronData.TxtPhoneNumber == 1 ? patronData.PhoneNumber : patronData.TxtPhoneNumber == 2 ? patronData.PhoneNumber2 : patronData.TxtPhoneNumber == 3 ? patronData.PhoneNumber3 : "";
                updateParams.DeliveryOptionID = !string.IsNullOrWhiteSpace(smsDeliveryPhone) ? 8 : !string.IsNullOrWhiteSpace(patronData.PhoneNumber) ? 3 : 1;
            }

            var result = papi.PatronUpdate(patron.Barcode, updateParams);

            if (result.Data?.PAPIErrorCode != 0)
            {
                logger.Error($"{result.Data.PAPIErrorCode} - {result.Data.ErrorMessage}");
                logger.Debug(updateParams.ToJson());
            }

            return patron;
        }
    }

    public class EmailEvent
    {
        public string To { get; set; }
        public string From { get; set; }
        public string Subject { get; set; }
        public string EventType { get; set; }
        public string Description { get; set; }
    }

    public enum EmailEventType
    {
        Ignore = -1,
        HardBounce,
        SoftBounce,
        SpamComplaint,
        Blocked
    }
}
