﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using clc_twilio_csharp;

using Notices.Models;
using Notices.Services;
using clc_twilio_csharp.Models;
using Notices.Core.Data;
using Notices.Services.Postmark;
using Notices.Core;
using Notices.Core.Models;
using Clc.Auth;

namespace Notices.Controllers
{
    [ClcAuthorize(Roles = "Clc.Notices.ViewHistory")]
    public class HistoryController : Controller
    {
        TwilioClient twilio = Methods.CreateTwilioClient();
        PostmarkClient postmark = new PostmarkClient(AppSettings.PostmarkEmailKey);

        const int RECORDINGS_PER_PAGE = 1000;

        public ActionResult Calls(string pn)
        {
            if (string.IsNullOrWhiteSpace(pn))
            {
                //var test2 = twilio.ListCalls(new CallListRequest { To = pn });//.Calls.Where(c => c.Direction == "outbound-api").ToList();
                return View(new CallListModel { Calls = twilio.ListCalls().Data.Calls.Where(c => c.Direction == "outbound-api").ToList() });
            }

            return View(new CallListModel { Calls = twilio.ListCalls(to: pn).Data.Calls.Where(c => c.Direction == "outbound-api").ToList(), PhoneNumber = pn });
        }

        public ActionResult Listen(string callSid)
        {
            if (string.IsNullOrWhiteSpace(callSid))
                return Redirect("Index");

            var recording = twilio.ListRecordings(callSid: callSid).Data.Recordings.FirstOrDefault();

            if (recording != null)
            {
                return Redirect(string.Format("{0}{1}", "https://api.twilio.com", recording.Uri.ToString().Replace(".json", "")));
            }
            else
            {
                return View("InvalidRecording");
            }
        }

        public ActionResult Email(string email = "", bool extraDays = false)
        {
            var messages = new List<EmailHistoryItem>();

            if (!string.IsNullOrWhiteSpace(email))
            {
                new DbHelper(AppSettings.DbServer).AddEmailHistorySearchLog(User.Identity.GetPreferredUsername(), email);

                var postmark = new PostmarkClient(AppSettings.PostmarkEmailKey);
                var history = postmark.SearchMessages(email);
                var bounces = postmark.GetBounces(email)?.Bounces;
                postmark = new PostmarkClient(AppSettings.PostmarkDbKey);
                history.AddRange(postmark.SearchMessages(email));
                bounces.AddRange(postmark.GetBounces(email)?.Bounces);

                var bouncedMessageIds = bounces.Select(b => b.MessageId.ToString());

                messages = history.Select(h => h.ToEmailHistoryItem()).ToList();
                for (int i = 0; i < messages.Count; i++)
                {
                    if (bouncedMessageIds.Contains(messages[i].Id))
                    {
                        messages[i].Status = "bounced";
                    }
                }
            }

            var model = new EmailListViewModel(email, messages);

            return View(model);
        }

        public ActionResult EmailContent(string id)
        {
            var content = postmark.GetMessageContent(id);

            if (string.IsNullOrWhiteSpace(content?.TextBody) && string.IsNullOrWhiteSpace(content?.HtmlBody))
            {
                content = new PostmarkClient(AppSettings.PostmarkDbKey).GetMessageContent(id);
            }

            return Content(content.GetContent(), content.GetContentType());

            //return View((object)Mandrill.GetContent(id));
        }

        public List<Recording> GetOldRecordings()
        {
            var twilio = Methods.CreateTwilioClient();
            var recordings = new List<Recording>();

            var result = twilio.ListRecordings(startDateComparison: "<=", startDate: Dialout.RecordingCutOffDate);
            recordings.AddRange(result.Data.Recordings);
            while (result.Data.NextPageUri != null)
            {
                result = twilio.GetNextPage<RecordingList>(result.Data.NextPageUri);
                recordings.AddRange(result.Data.Recordings);
            }

            return recordings;
        }

        public ActionResult Sms(string pn)
        {
            List<SmsMessageViewModel> messages = LoadTelnyxMessages(pn);
            messages.AddRange(LoadTwilioMessages(pn));

            /*
            switch (AppSettings.SmsSender.ToLower())
            {
                case "telnyx":
                    messages = LoadTelnyxMessages(pn);
                    break;
                default:
                    messages = LoadTwilioMessages(pn);
                    break;
            }
            */

            return View(new MessageListViewModel(pn, messages));
        }

        List<SmsMessageViewModel> LoadTwilioMessages(string pn)
        {
            var messages = new List<SmsMessageViewModel>();
            if (string.IsNullOrWhiteSpace(pn))
            {
                messages = twilio.ListMessages(pageSize: 100).Data.Messages.Select(m => new SmsMessageViewModel(m)).ToList();
            }
            else
            {
                messages = twilio.GetSentMessages(pn, pageSize: 100).Data.Messages.Select(m => new SmsMessageViewModel(m)).ToList();
                messages.AddRange(twilio.ListMessages(to: pn, pageSize: 100).Data.Messages.Select(m => new SmsMessageViewModel(m)).ToList());
            }

            return messages;
        }

        List<SmsMessageViewModel> LoadTelnyxMessages(string pn)
        {
            var db = new DbHelper(AppSettings.DbServer);
            var messages = db.GetTelnyxSmsMessages(pn).ToList();

            return messages;
        }
    }
}
