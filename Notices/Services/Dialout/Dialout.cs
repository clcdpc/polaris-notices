﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Notices.Extensions;
using Clc.Polaris.Api;
using Notices.Models;
using clc_twilio_csharp;
using Twilio.TwiML;
using System.Web.Mvc;
using NLog.Internal;
using NLog;
using Notices.Core;
using RabbitMQ.Client;
using Notices.Core.Models;
using System.Text;
using Clc.Polaris.Api.Models;
using System.ServiceProcess;
using System.IO;
using System.Web.Script.Serialization;
using Hangfire;
using System.Net.Http;
using Notices.Hangfire;
using Notices.Core.Data;
using System.Data.SqlClient;
using Dapper;

namespace Notices.Services
{
    public static class Dialout
    {
        private static List<Dialout_Strings> _strings;
        public static List<Dialout_Strings> Strings
        {
            get
            {
                if (_strings == null || !_strings.Any())
                {
                    var db = new DbHelper(AppSettings.DbServer);
                    _strings = db.DialoutStrings().ToList();
                }

                return _strings;
            }
        }

        static ConnectionFactory factory = new ConnectionFactory
        {
            HostName = AppSettings.RabbitMqHostname,
            VirtualHost = AppSettings.RabbitMqVirtualHost,
            UserName = AppSettings.RabbitMqUsername,
            Password = AppSettings.RabbitMqPassword
        };

        private static Logger logger = LogManager.GetCurrentClassLogger();
        static Notifier notifier = new Notifier("Dialout");
        static Dictionary<string, NotificationStatus> CallStatuses = new Dictionary<string, NotificationStatus>
        {
            { "completed", NotificationStatus.CallCompletedVoice },
            { "busy", NotificationStatus.CallBusy },
            { "failed", NotificationStatus.CallNoDialTone },
            { "no-answer", NotificationStatus.CallNoAnswer }
        };

        static DbHelper db = new DbHelper(AppSettings.DbServer);
        public static List<PolarisNotification> Notifications;// = new List<PolarisNotification>();
        public static List<string> TransferredCalls = new List<string>();
        static bool UsingOverrideFile = false;
        static bool PlaceCallsStarted = false;
        static bool PlaceCallsFinished = false;
        static DateTime? PlaceCallsFinishTime = null;

        public static Dictionary<int, string> PatronMessageCache = new Dictionary<int, string>();

        //public static int CallStartHour => DateTime.Now.IsWeekend() ? AppSettings.WeekendStartHour : AppSettings.WeekdayStartHour;
        //public static int CallStopHour => DateTime.Now.IsWeekend() ? AppSettings.WeekendStopHour : AppSettings.WeekdayStopHour;

        [AutomaticRetry(Attempts = 0)]
        public static string Run(bool ignoreTime = false)
        {
            if (!AppSettings.EnableDialout)
            {
                logger.Info("dialout disabled in config");
                return "dialout disabled in config";
            }
            // Check to see if we are already making calls to avoid making duplicates and generally screwing things up.
            if (InProcess)
            {
                logger.Info(Internal.AlreadyRunning);
                return Internal.AlreadyRunning;
            }

            // Check that we should make calls at the current hour.
            if (!AppSettings.Debug && !AppSettings.Dev)
            {
                if (!IsValidCallTime() && !ignoreTime)
                {
                    logger.Debug(Internal.OutsideRunHours);
                    return Internal.OutsideRunHours;
                }
            }

            if (!CheckPolarisApi())
            {
                logger.Error("Polaris API down");
                notifier.Notify(Internal.ApiDown, NotificationMethod.All);
                return Internal.ApiDown;
            }

            if (QueueHelper.GetQueueMessageCount(Queues.DIALOUT_NOTICE_UPDATES) != 0)
            {
                logger.Error("Unprocessed updates");
                notifier.Notify("The dialout update service is down", NotificationMethod.All);
                return "dialout update service down";
            }

            if (AppSettings.EnableFailedUpdateCheck)
            {
                if (QueueHelper.GetQueueMessageCount(Queues.FAILED_DIALOUT_UPDATES) > 0)
                {
                    logger.Error("Failed updates, aborting until cleared");
                    notifier.Notify("Failed updates in queue, dialout will not run until checked and cleared", NotificationMethod.All);
                    return "Failed updates in queue, dialout will not run until checked and cleared";
                }
            }

            if (!db.HoldNoticesHaveBeenPopuldated())
            {
                logger.Info("Hold notices were not generated this hour, exiting");
                return "Hold notices were not generated this hour, exiting";
            }            

            // If all of the required conditions are met then finally begin to make phone calls.
            logger.Info("Starting dialout");
            System.Configuration.ConfigurationManager.RefreshSection("appSettings");
            ResetVariables();
            ReloadStrings();
            LoadNotifications();
            RemovePreviouslyCalledPatrons();
            var callsPlaced = CallPatrons();
            return string.Format("Called {0} patrons", callsPlaced);
        }

        static void ResetVariables()
        {
            UsingOverrideFile = false;
            Notifications = new List<PolarisNotification>();
            TransferredCalls.Clear();
            PatronMessageCache = new Dictionary<int, string>();
            PlaceCallsStarted = false;
            PlaceCallsFinished = false;
            PlaceCallsFinishTime = null;
        }

        public static void ReloadStrings()
        {
            if (_strings != null) _strings.Clear();
            // Call .Any() to repopulate backing variable
            var x = Strings.Any();
        }

        static void LoadNotifications()
        {
            if (File.Exists(@"c:\programdata\clc_notices\dialout_override_file.log"))
            {
                UsingOverrideFile = true;
                logger.Info("Using dialout override file");
                Notifications = new List<PolarisNotification>();
                var lines = File.ReadAllLines(@"c:\programdata\clc_notices\dialout_override_file.log").Select(l => new string(l.Skip(77).ToArray()).TrimEnd('|'));

                var js = new JavaScriptSerializer();
                foreach (var line in lines)
                {
                    Notifications.AddRange(js.Deserialize<List<PolarisNotification>>(line));
                }

                if (File.Exists(@"c:\programdata\clc_notices\excluded_patrons.txt"))
                {
                    var exclude = File.ReadAllLines(@"c:\programdata\clc_notices\excluded_patrons.txt").Select(l => int.Parse(l)).ToList();
                    Notifications = Notifications.Where(n => !exclude.Contains(n.PatronID)).ToList();
                }

                File.Move(@"c:\programdata\clc_notices\dialout_override_file.log", $@"c:\programdata\clc_notices\dialout_override_file_processed_{DateTime.Now.ToString("yyyy-dd-M--HH-mm-ss")}.log");
            }
            else
            {
                Notifications = db.GetPhoneNotices().ToList();
            }

            if (Notifications.Count() == 0)
            {
                logger.Info("No notifications to deliver");
                return;
            }

            if (AppSettings.TestPatronId != 0)
            {
                Notifications = Notifications.Where(n => n.PatronID == AppSettings.TestPatronId).ToList();
            }
            else
            {
                // If we're testing only grab notices for one patron.
                if (AppSettings.Debug) Notifications = Notifications.GroupBy(n => n.PatronID).First().ToList();
            }

            Notifications.RemoveAll(n => !n.ShouldPlaceCall);

            logger.Info(string.Format("Loaded {0} notifications for {1} patrons", Notifications.Count, Notifications.Select(n => n.PatronID).Distinct().Count()));
        }

        public static string GenerateTestTwiml()
        {
            if (InProcess) return "in process";

            ReloadStrings();
            LoadNotifications();
            var msg = "";

            foreach (var pn in Notifications.GroupBy(n => n.PatronID))
            {
                msg += pn.First().DeliveryPhone;
                msg += "<br/>-------------<br/>";
                msg += pn.ToList().PhoneMessageTwiML("human");
                msg += "<br/>";
                msg += "<br/>";
            }
            return msg;
        }

        static void RemovePreviouslyCalledPatrons()
        {
            List<TodaysHoldCall> calledPatrons;

            calledPatrons = db.TodaysHoldCalls().ToList();


            if (AppSettings.LibrariesToCall.Count > 1 || AppSettings.LibrariesToCall.First() != 0)
            {
                calledPatrons = calledPatrons.Where(n => AppSettings.LibrariesToCall.Contains(n.ReportingLibraryID)).ToList();
            }

            if (calledPatrons.Count() == 0)
            {
                logger.Info("No patrons called for holds yet today");
                return;
            }

            logger.Info("Found {0} patrons called for holds", calledPatrons.Select(p => p.PatronID).Distinct().Count());

            foreach (var calledPatronNotices in Notifications
                .Where(n => calledPatrons.Any(cp => cp.PatronID == n.PatronID && cp.ReportingOrgId == n.ReportingBranchID))
                .GroupBy(n => n.PatronID))
            {
                foreach (var notice in calledPatronNotices)
                {
                    QueueUpdate(notice.BuildUpdate(NotificationStatus.CallCompletedVoice, Internal.PatronAlreadyCalledHold));
                }
                Notifications.RemoveAll(n => n.PatronID == calledPatronNotices.First().PatronID);
            }
        }

        public static int PerformDbMailRollover()
        {
            var db = new DbHelper();
            var rolloverCount = db.TwilioMailRollover(string.Join(";", AppSettings.LibrariesToCall));
            return rolloverCount;
        }


        static int CallPatrons()
        {
            PlaceCallsStarted = true;
            int callsPlaced = 0;

            var patronGroupedNotices = Notifications.GroupBy(n => n.PatronID).Select(g => g.ToList()).ToList();

            if (DbSettings.PrebuildDialoutMessages())
            {
                foreach (var patronNotices in patronGroupedNotices)
                {
                    var message = "";

                    if (AppSettings.Debug)
                    {
                        message = patronNotices.PhoneMessageTwiML("human");
                        PatronMessageCache.Add(patronNotices.First().PatronID, message);
                    }
                    else
                    {
                        try
                        {
                            message = patronNotices.PhoneMessageTwiML("human");
                            PatronMessageCache.Add(patronNotices.First().PatronID, message);
                        }
                        catch (Exception ex)
                        {
                            logger.Error(ex);
                            throw ex;
                        }
                        finally
                        {
                            PlaceCallsStarted = false;
                        }
                    }
                }

                if (AppSettings.Debug)
                {
                    Notifications.Clear();
                }
            }

            foreach (var patronNotices in patronGroupedNotices)
            {
                CallPlacer cp;

                if (AppSettings.Dev)
                {
                    cp = new DummyCallPlacer(patronNotices);
                }
                else if (AppSettings.Debug)
                {
                    cp = new TestingCallPlacer(patronNotices);
                }
                else
                {
                    cp = new TwilioCallPlacer(patronNotices);
                }

                if (string.IsNullOrWhiteSpace(patronNotices.First().DeliveryPhone) || patronNotices.First().DeliveryPhone.StartsWith("000"))
                {
                    logger.Error("Failed to call patron {0} - their phone number is invalid", patronNotices.First().PatronID);
                    foreach (var notice in patronNotices)
                    {
                        QueueUpdate(notice.BuildUpdate(NotificationStatus.CallNoDialTone, "Unable to deliver notice, phone number invalid"));
                    }

                    continue;
                }

                var call = cp.PlaceCall();

                if (!string.IsNullOrWhiteSpace(call))
                {
                    var _logger = LogManager.GetLogger("Call." + call);
                    callsPlaced++;
                    _logger.Debug("Called patron {0} at {1}", patronNotices.First().PatronID, patronNotices.First().DeliveryPhone);
                }
            }

            PlaceCallsFinished = true;
            PlaceCallsFinishTime = DateTime.Now;
            return callsPlaced;
        }

        static bool CheckForFailedUpdates()
        {
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                return ((int)channel.QueueDeclarePassive(Queues.FAILED_DIALOUT_UPDATES).MessageCount > 0);
            }
        }

        public static string FallbackResponse()
        {
            return new TwilioResponse().Say(Internal.FallbackPhoneMessage, new { voice = "alice" }).ToString();
        }

        public static string BuildMessage(string callsid, string answeredby, int patronId, int[] items = null, bool testing = false)
        {
            var _logger = LogManager.GetLogger("Call." + callsid);

            if (DbSettings.PrebuildDialoutMessages())
            {
                return PatronMessageCache[patronId];
            }

            if (string.IsNullOrWhiteSpace(callsid))
            {
                return FallbackResponse();
            }

            try
            {
                _logger.Trace("Start patron {0}", patronId);

                var notices = GetPatronNotices(patronId, items).ToList();
                _logger.Trace("Found {0} notices", notices.Count);

                var twiml = notices.PhoneMessageTwiML(answeredby);

                _logger.Trace("TwiML: {0}", twiml);
                return twiml;
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Error building message");
                try
                {
                    var notices = Notifications.Where(n => n.PatronID == patronId).ToList();
                    logger.Error(ex, "Error building message for call {0} | patronid {1} | notices \r\n {2}", callsid, patronId, notices);
                }
                catch (Exception ex2)
                {
                    logger.Error(ex2, "Error creating log message in BuildMessage catch");
                }

                notifier.Notify("Error building phone twiml.", NotificationMethod.All);
                return FallbackResponse();
            }
        }

        public static void QueueUpdate(NoticeUpdate notice)
        {
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(
                    queue: Queues.DIALOUT_NOTICE_UPDATES,
                    durable: true,
                    exclusive: false,
                    autoDelete: false,
                    arguments: null);

                IBasicProperties props = channel.CreateBasicProperties();
                props.ContentType = "text/plain";
                props.DeliveryMode = 2;

                channel.BasicPublish("", Queues.DIALOUT_NOTICE_UPDATES, props, Encoding.UTF8.GetBytes(notice.ToJson()));
            }
        }

        public static IEnumerable<PolarisNotification> GetPatronNotices(int patronId, int[] items = null)
        {
            //if (patronId == null)
            //{
            //    throw new ArgumentException("patronid cannot be null");
            //}
            logger.Trace(patronId);
            IEnumerable<PolarisNotification> notices = new List<PolarisNotification>();

            try
            {
                notices = Notifications.Where(n => n.PatronID == patronId);
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error retrieving notices from cache");
            }

            if (!notices.Any())
            {
                logger.Info("Hitting db to load notices");

                if (items == null) { throw new ArgumentException("Unable to find notices in cache and no items were specified."); }

                try
                {
                    notices = db.GetPhoneNotices().Where(n => n.PatronID == patronId && items.Contains(n.ItemRecordID)).ToList();
                }
                catch (Exception ex)
                {
                    logger.Error(ex, $"Unable to retrieve notices for patron {patronId}");
                    throw ex;
                }
            }

            return notices;
        }

        [HttpPost]
        public static PostNoticesResult PostNotices(int patronId, string callSid, string callStatus, int[] items = null, string details = "")
        {
            logger.Trace("CallSid: {0} | Patron: {1} | Items: {2}", callSid, patronId, string.Join(", ", items));
            if (UsingOverrideFile) return new PostNoticesResult(true, "using override file");

            PostNoticesResult result = new PostNoticesResult();

            if (string.IsNullOrWhiteSpace(callSid)) return new PostNoticesResult(false, "no callsid");

            var _logger = LogManager.GetLogger("Call." + callSid);
            if (TransferredCalls.Contains(callSid))
            {
                _logger.Debug("Patron {0} transferred to dialin, don't update notices", patronId);
                return new PostNoticesResult(true, "ok");
            }

            var notices = GetPatronNotices(patronId, items);

            if (!notices.Any())
            {
                _logger.Debug("No notices found for patron {0}", patronId);
                return new PostNoticesResult(false, "Notices not found for patron");
            }

            if (!CallStatuses.TryGetValue(callStatus, out NotificationStatus notificationStatus)) notificationStatus = NotificationStatus.CallNoAnswer;

            if (AppSettings.Debug || AppSettings.Dev)
            {
                notificationStatus = NotificationStatus.CallNoAnswer;
            }

            _logger.Debug("Call status: {0} - {1}", notificationStatus, callStatus);
            _logger.Trace("Found {0} notices", notices.Count());

            details = !string.IsNullOrWhiteSpace(details)
                ? details
                : AppSettings.Debug || AppSettings.Dev ? Internal.TestCallDetailsMessage : callSid;

            var errorMessage = "error message: ";
            var errorOccurred = false;

            foreach (var notice in notices)
            {
                var update = new NoticeUpdate
                {
                    NotificationTypeId = notice.NotificationTypeID,
                    DeliveryOptionId = notice.DeliveryOptionID,
                    DeliveryDate = DateTime.Now,
                    PatronId = notice.PatronID,
                    DeliveryString = notice.DeliveryPhone,
                    ItemRecordId = notice.ItemRecordID,
                    NotificationStatus = notificationStatus,
                    ReportingOrgId = 1, // use 1 to account for a change made in 7.7 and make sure entries still end up in notification history
                    Details = details
                };

                QueueUpdate(update);
            }

            return new PostNoticesResult(!errorOccurred, errorOccurred ? errorMessage : "ok");
        }

        [TenMinuteJobHistory]
        public static string CheckService()
        {
            if (QueueHelper.GetQueueConsumerCount(Queues.DIALOUT_NOTICE_UPDATES) > 0) return "running";

            if (!AppSettings.Dev)
            {
                WindowsHelper.RestartService(AppSettings.NOTICE_UPDATE_SERVICE_NAME);
            }

            return "restarted";
        }

        static bool CheckPolarisApi()
        {
            try
            {
                var papi = Methods.CreatePapiClient();
                var sysOrg = papi.OrganizationsGet(OrganizationType.System);
                return sysOrg.Data.OrganizationsGetRows.Count == 1;
            }
            catch
            {
                return false;
            }
        }

        static bool CheckUrl(string url)
        {
            var result = new HttpClient().GetAsync(url).Result;

            if (result.StatusCode == System.Net.HttpStatusCode.OK) return true;
            return false;
        }

        static bool IsValidCallTime()
        {
            int startHour, stopHour;
            var day = DateTime.Now.DayOfWeek;

            if (day == DayOfWeek.Saturday)
            {
                startHour = AppSettings.SaturdayStartHour;
                stopHour = AppSettings.SaturdayStopHour;
            }
            else if (day == DayOfWeek.Sunday)
            {
                startHour = AppSettings.SundayStartHour;
                stopHour = AppSettings.SundayStopHour;
            }
            else
            {
                startHour = AppSettings.WeekdayStartHour;
                stopHour = AppSettings.WeekdayStopHour;
            }

            return DateTime.Now.Hour >= startHour && DateTime.Now.Hour <= stopHour;
        }

        public static int CallsInProcess
        {
            get
            {
                var twilio = Methods.CreateTwilioClient();
                var statuses = new List<string> { "queued", "ringing", "in-progress" };

                var calls = -1;

                foreach (var status in statuses)
                {
                    var result = twilio.ListCalls(status: status, from: AppSettings.FromPhone, startDateComparison: ">=", startDate: DateTime.Now.Date);

                    if (result?.Response?.IsSuccessStatusCode ?? false)
                    {
                        calls += result.Data.Calls.Count;
                    }
                    else
                    {
                        logger.Error("Failure getting calls in progress: {0}|{1}", result.Response.StatusCode, result.Response.Content);
                    }
                }

                return calls;
            }
        }

        public static bool InProcess
        {
            get
            {
                return PlaceCallsStarted && (!PlaceCallsFinished || (PlaceCallsFinished && PlaceCallsFinishTime?.AddMinutes(AppSettings.DialoutEndTimeInprocessBuffer) > DateTime.Now));
            }
        }

        public static DateTime RecordingCutOffDate
        {
            get
            {
                return DateTime.Now.AddDays(AppSettings.RecordingRetentionDays * -1);
            }
        }
    }
}