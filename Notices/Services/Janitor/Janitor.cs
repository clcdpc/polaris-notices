﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using clc_twilio_csharp;
using clc_twilio_csharp.Models;
using NLog;
using Notices.Core;

namespace Notices.Services
{
    public class Janitor
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public static int DeleteOldRecordings()
        {
            var twilio = Methods.CreateTwilioClient();
            var response = twilio.ListRecordings(startDateComparison: "<=", startDate: Dialout.RecordingCutOffDate);

            if (!response.Response.IsSuccessStatusCode)
            {
                logger.Error(response.Exception);
                return 0;
            }

            var recordings = response.Data;
            var count = 0;
            do
            {
                foreach (var recording in recordings.Recordings)
                {
                    logger.Info(recording.Sid);
                    twilio.DeleteRecording(recording.Sid);
                    count++;
                }

                if (recordings.NextPageUri != null)
                {
                    response = twilio.ListRecordings(startDateComparison: "<=", startDate: Dialout.RecordingCutOffDate);

                    if (!response.Response.IsSuccessStatusCode)
                    {
                        logger.Error(response.Exception);
                        return count;
                    }

                    recordings = response.Data;
                }
            } while (recordings.NextPageUri != null);

            return count;
        }

        public static void ClearOldLogFiles()
        {
            var ps1File = @"C:\Program Data\clc_notices\scripts\log_cleanup.ps1";
            var startInfo = new ProcessStartInfo()
            {
                FileName = "powershell.exe",
                Arguments = $"-NoProfile -ExecutionPolicy unrestricted -file \"{ps1File}\"",
                UseShellExecute = false
            };
            Process.Start(startInfo);
        }

        public static int DeleteOldCalls()
        {
            var twilio = Methods.CreateTwilioClient();

            var response = twilio.ListCalls(startDateComparison: "<=", startDate: DateTime.Now.AddDays(AppSettings.CallRetentionDays * -1));

            if (response.Exception != null)
            {
                logger.Error(response.Exception);
                return 0;
            }

            var calls = response.Data;

            var count = 0;
            do
            {
                foreach (var call in calls.Calls)
                {
                    logger.Info(call.Sid);
                    twilio.DeleteCall(call.Sid);
                    count++;
                }

                response = twilio.ListCalls(startDateComparison: "<=", startDate: DateTime.Now.AddDays(AppSettings.CallRetentionDays * -1));

                if (response.Exception != null)
                {
                    logger.Error(response.Exception);
                    return count;
                }

                calls = response.Data;
            } while (calls?.Calls?.Count > 0);

            return count;
        }

        public static int DeleteOldSmsMessages()
        {
            var twilio = Methods.CreateTwilioClient();
            var count = 0;

            var response = twilio.ListMessages(startDateComparison: "<=", startDate: DateTime.Now.AddDays(AppSettings.SmsRetentionDays * -1));

            if (!response.Response.IsSuccessStatusCode)
            {
                logger.Error(response.Exception);
                return 0;
            }

            var messages = response.Data;
            do
            {
                foreach (var msg in messages.Messages)
                {
                    logger.Info(msg.Sid);
                    twilio.DeleteMessage(msg.Sid);
                    count++;
                }

                if (messages.NextPageUri != null)
                {
                    response = twilio.ListMessages(startDateComparison: "<=", startDate: DateTime.Now.AddDays(AppSettings.SmsRetentionDays * -1));

                    if (!response.Response.IsSuccessStatusCode)
                    {
                        logger.Error(response.Exception);
                        return count;
                    }

                    messages = response.Data;
                }
            } while (!string.IsNullOrWhiteSpace(messages?.NextPageUri));

            return count;
        }
    }
}