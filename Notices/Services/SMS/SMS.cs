﻿using Notices.Core.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Notices.Extensions;
using Notices.Models;
using clc_twilio_csharp;
using System.Messaging;
using RabbitMQ.Client;
using System.Text;
using Notices.Core;
using Notices.Core.Models;
using NLog;
using System.ServiceProcess;
using Clc.Polaris.Api;
using Clc.Polaris.Api.Models;
using Notices.Hangfire;
using Hangfire;
using System.Data.SqlClient;
using Dapper;

namespace Notices.Services
{
    public static class SMS
    {
        static Random r = new Random();
        static TwilioClient twilio = Methods.CreateTwilioClient();
        static ConnectionFactory factory = new ConnectionFactory
        {
            HostName = AppSettings.RabbitMqHostname,
            VirtualHost = AppSettings.RabbitMqVirtualHost,
            UserName = AppSettings.RabbitMqUsername,
            Password = AppSettings.RabbitMqPassword
        };
        static Logger logger = LogManager.GetCurrentClassLogger();
        static PapiClient papi = Methods.CreatePapiClient();

        static List<SmsMessage> TodaysSentMessages = new List<SmsMessage>();

        public static void SendMessages(IEnumerable<MandrillMailEvent> events)
        {
            foreach (var ev in events)
            {
                SendMessage(ev.Msg.CleanTo, ev.Msg.CleanBody);
            }
        }

        public static void SendMessage(string to, string body, int orgId = 0)
        {
            if (!AppSettings.EnableSmsProcessing) return;

            to = new string(to.Where(c => char.IsDigit(c)).ToArray());

            to = to.Length == 10 ? $"1{to}" : to;

            if (AppSettings.PreventDuplicateSmsMessages)
            {
                var isDupe = false;
                var sender = Core.Methods.GetSmsSender(orgId);
                isDupe = sender.CheckDuplicate(to, body, orgId);
                
                if (isDupe)
                {
                    SendToQueue(to, body, Queues.DUPLICATE_SMS_MESSAGES);
                    logger.Info($"Duplicate message: {to} - {body}");
                    return;
                }
            }            

            logger.Info("Queueing: {0} - {1}", to, body);
            SendToQueue(to, body, orgId: orgId);
        }

        static void SendToQueue(string to, string body, string queue = Queues.SMS_MESSAGES, int orgId = 0)
        {
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(
                    queue: queue,
                    durable: true,
                    exclusive: false,
                    autoDelete: false,
                    arguments: null);

                var message = new SmsMessage(to, body, orgId) { QueuedAt = DateTime.Now };

                IBasicProperties props = channel.CreateBasicProperties();
                props.ContentType = "text/plain";
                props.DeliveryMode = 2;

                channel.BasicPublish("", queue, props, Encoding.UTF8.GetBytes(message.ToJson()));
            }
        }

        public static string ClearSentMessages()
        {
            TodaysSentMessages = new List<SmsMessage>();
            return "messages cleared";
        }

        [TenMinuteJobHistory]
        public static string CheckService()
        {
            if (QueueHelper.GetQueueConsumerCount(Queues.SMS_MESSAGES) > 0) return "running";

            if (!AppSettings.Dev)
            {
                WindowsHelper.RestartService(AppSettings.SMS_SERVICE_NAME);
            }

            return "restarted";
        }

        public static void SendTestMessage()
        {
            if (!AppSettings.EnableSmsProcessing) return;

            SendMessages(new List<MandrillMailEvent>() { MandrillMailEvent.CreateTestEvent() });
        }

        [AutomaticRetry(Attempts = 0)]
        public static void SendAfterHours()
        {
            var afterHoursQueue = new SmsListener(Queues.AFTER_HOURS_MESSAGES, true);
            afterHoursQueue.Start();
        }

        public static void QueueUpdate(NoticeUpdate notice)
        {
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(
                    queue: Queues.SMS_NOTICE_UPDATES,
                    durable: true,
                    exclusive: false,
                    autoDelete: false,
                    arguments: null);

                IBasicProperties props = channel.CreateBasicProperties();
                props.ContentType = "text/plain";
                props.DeliveryMode = 2;

                channel.BasicPublish("", Queues.SMS_NOTICE_UPDATES, props, Encoding.UTF8.GetBytes(notice.ToJson()));
            }
        }

        public static bool IsValidCallTime()
        {
            return true;
        }
    }
}