﻿using Clc.Polaris.Api;
using Clc.Polaris.Api.Models;
using Notices.Core;
using Notices.Core.Data;
using Notices.Models;
using Notices.Services.Postmark;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Hosting;

namespace Notices.Services
{
    public static class Dialin
    {
        private static IEnumerable<Dialin_Strings> _strings;
        public static IEnumerable<Dialin_Strings> Strings
        {
            get
            {
                if (_strings == null)
                {
                    var db = new DbHelper(AppSettings.DbServer);
                    _strings = db.DialinStrings().ToList();

                }

                return _strings;
            }
        }

        public static void Initialize()
        {

        }

        public static string GetString(string mnemonic, int orgId = 1)
        {
            var value = Strings.SingleOrDefault(s => s.Mnemonic == mnemonic && s.OrganizationID == orgId);
            if (value != null) return value.Value;

            var library = Variables.Organizations.Single(o => o.OrganizationID == orgId).ParentOrganizationID;
            value = Strings.SingleOrDefault(s => s.Mnemonic == mnemonic && s.OrganizationID == library);
            if (value != null) return value.Value;

            return Strings.Single(s => s.Mnemonic == mnemonic && s.OrganizationID == 1).Value;
        }

        public static void RefreshHeldItems(string callsid)
        {
            var papi = Methods.CreatePapiClient();

            var holds = new List<HeldItem>();

            foreach (var hold in papi.PatronHoldRequestsGet(CallVars.PatronBarcodes[callsid], PatronHoldStatus.held, CallVars.PatronPINs[callsid]).Data.PatronHoldRequestsGetRows)
            {
                holds.Add(new HeldItem(hold));
            }

            foreach (var ill in papi.PatronILLRequestsGet(CallVars.PatronBarcodes[callsid], ILLStatus.ReceivedHeld, CallVars.PatronPINs[callsid]).Data.PatronILLRequestsGetRows)
            {
                holds.Add(new HeldItem(ill));
            }

            CallVars.Holds[callsid] = holds.OrderBy(h => h.PickupByDate).ToList();
        }

        public static void RefreshItemsOut(string callsid, bool overdueOnly = false)
        {
            var db = new DbHelper(AppSettings.DbServer);
            CallVars.ItemsOut[callsid] = db.GetCheckedOutItems(CallVars.PatronData[callsid].PatronID, overdueOnly ? 1 : 0).Where(i => !i.ElectronicItem).ToList();
        }

        public static bool EmailTitleList(string patronEmail, List<GetCheckedOutItems_Result> items)
        {
            var template = File.ReadAllText(HostingEnvironment.MapPath("~/content/templates/dialin/email_title_list.html"));
            var sb = new StringBuilder();
            sb.Append(@"<table style=""width:75%; text-align:left;border-collapse:collapse;"">");
            sb.Append(@"<tr><th align=left style=""width:75%;border-bottom: 1px solid #000;"">Title</th><th align=left style=""width:25%; padding-left:15px;border-bottom: 1px solid #000;"">Due Date</th></tr>");
            foreach (var item in items.OrderByDescending(i => i.DueDate))
            {
                sb.Append(string.Format(@"<tr><td style=""width:90%; padding-top:4px;vertical-align: top;"">{0}</td><td style=""width:10%; padding-top:4px; padding-left:15px;vertical-align: top;"">{1}</td></tr>", item.BrowseTitle, item.DueDate.ToShortDateString()));
            }
            sb.Append("</table>");

            var body = template.Replace("{cko_item_list}", sb.ToString());

            var response = new PostmarkClient(AppSettings.PostmarkTelephonyKey).Send(patronEmail, "donotreply@clcohio.org", "List of Items Checked Out - Sent from 1.877.77.BOOKS", body, "");
            return response;
        }
    }
}