﻿using Clc.Polaris.Api.Models;
using Hangfire;
using Newtonsoft.Json;
using NLog;
using Notices.Core;
using Notices.Core.Models;
using Notices.Core.Data;
using Notices.Models;
using Notices.Services.Postmark;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Web;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using Dapper;

namespace Notices.Services.Email
{
    public static class Email
    {
        static ConnectionFactory factory = new ConnectionFactory
        {
            HostName = AppSettings.RabbitMqHostname,
            VirtualHost = AppSettings.RabbitMqVirtualHost,
            UserName = AppSettings.RabbitMqUsername,
            Password = AppSettings.RabbitMqPassword
        };

        private static Logger logger = LogManager.GetCurrentClassLogger();

        private static Dictionary<NotificationType, string> SubjectMnemonicMap = new Dictionary<NotificationType, string>
        {
            [NotificationType.Combined] = Mnemonics.EmailSubjectCombined,
            [NotificationType.FirstOverdue] = Mnemonics.EmailSubjectFirstOverdue,
            [NotificationType.SecondOverdue] = Mnemonics.EmailSubjectSecondOverdue,
            [NotificationType.ThirdOverdue] = Mnemonics.EmailSubjectThirdOverdue,
            [NotificationType.Cancel] = Mnemonics.EmailSubjectCancel,
            [NotificationType.ExpirationReminder] = Mnemonics.EmailSubjectExpiration,
            [NotificationType.Hold] = Mnemonics.EmailSubjectHold,
            [NotificationType.AlmostOverdueReminder] = Mnemonics.EmailSubjectAlmostOverdue,
            [NotificationType.SecondHold] = Mnemonics.EmailSubjectSecondHold,
            [NotificationType.ClcCustomCovid19] = Mnemonics.EmailSubjectCovid19
        };

        public static Dictionary<NotificationType, string> EnableTemplateSettingMnemonicMap = new Dictionary<NotificationType, string>
        {
            [NotificationType.ExpirationReminder] = Mnemonics.EmailEnableExpirationNotices,
            [NotificationType.Hold] = Mnemonics.EmailEnableHoldNotices,
            [NotificationType.SecondHold] = Mnemonics.EmailEnableSecondHoldNotices,
            [NotificationType.Cancel] = Mnemonics.EmailEnableCancelNotices,
            [NotificationType.FirstOverdue] = Mnemonics.EmailEnableOverdueNotices,
            [NotificationType.SecondOverdue] = Mnemonics.EmailEnableOverdueNotices,
            [NotificationType.ThirdOverdue] = Mnemonics.EmailEnableOverdueNotices,
            [NotificationType.AlmostOverdueReminder] = Mnemonics.EmailEnableAlmostOverdueNotices,
            [NotificationType.ClcCustomCovid19] = Mnemonics.EmailEnableCovidNotice
        };

        public static Dictionary<NotificationType, bool> InProcessDictionary = new Dictionary<NotificationType, bool>();

        [AutomaticRetry(Attempts = 0)]
        public static void SendNotices(NotificationType[] noticeTypes, IEmailSender emailSender = null)
        {
            if (InProcessDictionary.Any(d => noticeTypes.Contains(d.Key) && d.Value))
            {
                throw new Exception("Email process is already running");
            }
            else
            {
                foreach (var nt in noticeTypes)
                {
                    InProcessDictionary[nt] = true;
                }
            }

            var isSuccess = true;

            logger.Debug("Starting email notice process");
            logger.Debug("Notice types: {0}", string.Join(", ", noticeTypes.Select(n => n.ToString())));

            var db = new DbHelper(AppSettings.DbServer);
            var allNotices = db.GetEmailNotices(noticeTypes).ToList();

            var groupedNotices = allNotices
                .Where(n => DbSettings.GetValue<bool>(EnableTemplateSettingMnemonicMap[n.NotificationType], n.EmailTemplateBranchId))
                .GroupBy(n => n.EmailGroupNotificationType)
                .Select(g => new { NotificationType = g.Key, Notices = g.ToList() });

            foreach (var noticeGroup in groupedNotices)
            {
                logger.Info($"Start {noticeGroup.NotificationType}");
                var notices = noticeGroup.Notices;

                if (AppSettings.TestPatronId > 0)
                {
                    notices = notices.Where(n => n.PatronID == AppSettings.TestPatronId).ToList();
                    notices.ForEach(n => { n.EmailAddress = "mfields@clcohio.org"; n.AltEmailAddress = null; });
                }

                var currentSuccess = SendNotices(notices, emailSender);

                if (!currentSuccess) { isSuccess = false; }
                logger.Info("Done ({0}): {1}", currentSuccess ? "Success" : "Error", noticeGroup.NotificationType);
            }

            logger.Info("Done ({0}): ALL", isSuccess ? "Success" : "Error");

            db.AddEmailStatus(isSuccess);


            foreach (var nt in noticeTypes)
            {
                InProcessDictionary[nt] = false;
            }
        }

        [AutomaticRetry(Attempts = 0)]
        public static bool SendNotices(List<PolarisNotification> notices = null, IEmailSender emailSender = null)
        {
            bool isSuccess = true;

            if (notices == null)
            {
                logger.Error("No notice list provided, exiting.");
                return false;
            }

            var byPatronBranch = false;

            EmailTemplateCache templateCache;

            if (AppSettings.Dev)
            {
                templateCache = EmailTemplateCache.BuildTemplateCache(notices);
            }
            else
            {
                try
                {
                    templateCache = EmailTemplateCache.BuildTemplateCache(notices);
                }
                catch (Exception ex)
                {
                    //Notifier.Create("Email").Notify("Error building template cache", NotificationMethod.Email);
                    logger.Error(ex, JsonConvert.SerializeObject(notices));
                    return false;
                }
            }

            emailSender = emailSender ?? Methods.GetEmailSender();

            if (AppSettings.Dev && emailSender.GetType() == typeof(PostmarkEmailSender) && notices.Select(n=>n.PatronID).Distinct().Count() > 5)
            {
                throw new Exception("You probably don't want to do this");
            }

            var groupedNotices = notices.GroupBy(n => new EmailNoticeGroup(n)).Select(g => new { g.Key.PatronId, g.Key.OrganizationId, g.Key.LanguageId, g.Key.NotificationType, Notices = g.ToList() }).ToList();

            foreach (var patronNotices in groupedNotices)
            {
                logger.Trace("{0} | {1} | {2} | {3}", patronNotices.NotificationType, patronNotices.PatronId, patronNotices.OrganizationId, Core.Methods.GetOrg(patronNotices.OrganizationId)?.Name);
                var patronLogger = LogManager.GetLogger($"email.{patronNotices.PatronId}");

                //var templateType = PolarisNotification.EmailTemplateMap[patronNotices.NotificationType];

                var patronEmail = patronNotices.Notices.First().EmailAddress;
                var patronAltEmail = patronNotices.Notices.First().AltEmailAddress;

                var to = new List<string>() { patronEmail };

                patronLogger.Debug("Start {0}", patronNotices.NotificationType);
                patronLogger.Debug("Email: {0}", patronEmail);

                if (!string.IsNullOrWhiteSpace(patronAltEmail))
                {
                    to.Add(patronAltEmail);
                    patronLogger.Debug("Alt email: {0}", patronAltEmail);
                }

                var subjectMnemonic = SubjectMnemonicMap[patronNotices.NotificationType];
                var subject = DbSettings.GetValue<string>(SubjectMnemonicMap[patronNotices.NotificationType], patronNotices.OrganizationId, patronNotices.LanguageId);

                var message = new EmailMessage
                {
                    To = to,
                    From = DbSettings.GetValue<string>(Mnemonics.EmailNoticeFromAddress, patronNotices.OrganizationId),
                    Subject = DbSettings.GetValue<string>(SubjectMnemonicMap[patronNotices.NotificationType], patronNotices.OrganizationId, patronNotices.LanguageId)
                };

                if (AppSettings.Dev)
                {
                    message = GenerateNoticeEmail(message, templateCache, patronNotices.Notices, patronNotices.OrganizationId);
                }

                try
                {
                    patronLogger.Trace("Build body");
                    message = GenerateNoticeEmail(message, templateCache, patronNotices.Notices, patronNotices.OrganizationId);
                }
                catch (Exception ex)
                {
                    Notifier.Create("Email").Notify($"Error building email body for patron {patronNotices.PatronId}", NotificationMethod.Email);
                    logger.Error(ex, $"Error building email body. Patron: {patronNotices.PatronId} | NoticeType: {patronNotices.NotificationType} | Notices: \r\n{JsonConvert.SerializeObject(notices)}");
                    continue;
                }

                patronLogger.Debug("NoticeType: {0}", patronNotices.NotificationType);
                patronLogger.Trace("Subject: {0}", message.Subject);
                patronLogger.Trace("HtmlBody: {0}", message.HtmlBody);
                patronLogger.Trace("TextBody: {0}", message.TextBody);

                var response = false;

                if (!CheckForBadPlaceholders(message))
                {
                    response = emailSender.Send(message);
                }
                else
                {
                    logger.Error("Found unreplaced placeholders, skipping {0}...", patronEmail);
                    patronLogger.Error("Found unreplaced placeholders, skipping...");
                    continue;
                }

                logger.Info("{0} | {1} | {2} | {3} | {4}", patronNotices.PatronId, patronEmail, patronNotices.OrganizationId, patronNotices.NotificationType, response);//.IsSuccess, response.Status);
                //logger.Trace("{0} raw response:\r\n{1}", patronNotices.Key.PatronID, JsonConvert.SerializeObject(response));
                //patronLogger.Debug("Response: {0} - {1}", response.IsSuccess, response.Status);
                patronLogger.Debug(response ? "Success" : "Failed");

                if (!response)
                {
                    isSuccess = false;
                    logger.Error("Failure sending to: {0}", string.Join(", ", to));
                }

                if (emailSender.QueueUpdate && AppSettings.TestPatronId == 0)
                {
                    foreach (var notice in patronNotices.Notices.Where(n => n.NotificationTypeID < 999))
                    {
                        var update = notice.BuildUpdate(response ? NotificationStatus.EmailCompleted : NotificationStatus.EmailFailed);
                        QueueUpdate(update);
                    }

                    using (var conn = new SqlConnection($"Server={AppSettings.DbServer};Database=CLC_Notices;Trusted_Connection=True;"))
                    {
                        foreach (var notice in patronNotices.Notices.Where(n => n.NotificationTypeID == 999))
                        {
                            var sql = @"
                            insert into CLC_Notices.dbo.ClcCustomNoticeHistory (NoticeDate,PatronId,NoticeTypeId,DeliveryOptionId,DeliveryString,StatusId,ItemRecordId)
                            values (@noticeDate, @patronId, @noticeTypeId, @deliveryOptionId, @deliveryString, @statusId, @itemRecordId)
                            ";

                            var parms = new
                            {
                                noticeDate = DateTime.Now,
                                patronId = notice.PatronID,
                                noticeTypeId = notice.NotificationTypeID,
                                deliveryOptionId = 2,
                                deliveryString = string.Join(";", to),
                                statusId = response ? NotificationStatus.EmailCompleted : NotificationStatus.EmailFailed,
                                itemRecordId = notice.ItemRecordID
                            };

                            var foo = conn.Query(sql, parms);
                            var bar = foo;
                        }

                        var sql2 = "delete from CLC_Notices.dbo.ClcCustomNotificationQueue where PatronId = @patronId and NotifictationTypeId = 999";
                        var foo2 = conn.Query(sql2, new { patronId = patronNotices.PatronId });
                        var bar2 = foo2;
                    }
                }
            }

            return isSuccess;
        }

        static bool CheckForBadPlaceholders(EmailMessage message)
        {
            var matches = Regex.Matches($"{message.TextBody}{message.HtmlBody}", "{[a-zA-z0-9_-]{3,25}}").Cast<Match>().Select(m => m.Value);

            if (matches.Any()) { logger.Error("Non-replaced placeholders: {0}. Subject: {1} | From: {2}", string.Join(", ", matches), message.Subject, message.From); }

            return matches.Any();
        }

        public static EmailMessage GenerateNoticeEmail(List<PolarisNotification> notices, int? orgId = null)
        {
            var _orgId = orgId ?? notices.First().EmailGroupBranchId;

            var templateCache = EmailTemplateCache.BuildTemplateCache(notices);
            return GenerateNoticeEmail(new EmailMessage(), templateCache, notices, _orgId);
        }

        public static EmailMessage GenerateNoticeEmail(EmailMessage message, EmailTemplateCache templateCache, List<PolarisNotification> notices, int orgId)
        {
            var Placeholders = new List<IEmailTemplatePlaceholder>
                {
                    new NoticeHeaderPlaceholder(),
                    new NoticeFooterPlaceholder(),
                    new NoticeBodyPlaceholder(),
                    new PatronFirstNamePlaceholder(),
                    new PatronLastNamePlaceholder(),
                    new PatronEmailPlaceholder(),
                    new PatronPhonePlaceholder(),
                    new PatronStreetOnePlaceholder(),
                    new PatronStreetTwoPlaceholder(),
                    new PatronCityPlaceholder(),
                    new PatronStatePlaceholder(),
                    new PatronZipPlaceholder(),
                    new ReportingBranchNamePlaceholder(),
                    new ReportingBranchStreetOnePlaceholder(),
                    new ReportingBranchStreetTwoPlaceholder(),
                    new ReportingBranchCityPlaceholder(),
                    new ReportingBranchStatePlaceholder(),
                    new ReportingBranchZipPlaceholder(),
                    new ReportingBranchPhonePlaceholder(),
                    new CancelledHoldRequestListPlaceholder(),
                    new OverdueItemListPlaceholder(),
                    new PatronHoldBranchGroupsPlaceholder(),
                    new AlmostOverdueReminderAlmostOverdueItemsPlaceholder(),
                    new AlmostOverdueReminderAutorenewItemsGroupPlaceholder(),
                    new AlmostOverdueReminderOtherItemsGroupPlaceholder(),
                    new UnsubscribeLinkPlaceholder()
                };
            var placeholderHandler = new EmailPlaceholderHandler(notices, Placeholders);

            var templates = templateCache.GetTemplate(notices, orgId);

            if (templates.GetType() == typeof(SkipThisEntry)) { throw new Exception("Error loading template data"); }

            message.HtmlBody = placeholderHandler.ReplaceAll(templates.HtmlTemplate, EmailFormat.Html);
            message.TextBody = placeholderHandler.ReplaceAll(templates.PlaintextTemplate, EmailFormat.PlainText);

            return message;
        }

        public static string RunTests(int patronBranch, int reportingBranchId, NotificationType[] noticeTypes = null, IEmailSender emailSender = null)
        {
            return RunTests(patronBranch, new[] { reportingBranchId }, noticeTypes, emailSender);
        }

        public static string RunTests(int patronBranch, int[] reportingBranchIds, NotificationType[] noticeTypes = null, IEmailSender emailSender = null)
        {
            noticeTypes = noticeTypes ?? new[] { NotificationType.FirstOverdue, NotificationType.SecondOverdue, NotificationType.ThirdOverdue, NotificationType.AlmostOverdueReminder, NotificationType.ExpirationReminder, NotificationType.Cancel, NotificationType.Hold, NotificationType.SecondHold };
            //var noticeTypes = new[] { NotificationType.Hold, NotificationType.SecondHold };

            var notices = new List<PolarisNotification>();
            notices = PolarisNotification.GenerateTest(noticeTypes, DeliveryOption.EmailAddress, patronBranch, reportingBranchIds);

            //using (var db = NoticeEntities.Create())
            //{
            //    foreach (var noticetype in noticeTypes)
            //    {
            //        foreach (var reportingBranch in reportingBranchIds)
            //        {
            //            var bar = db.CreateTestNotices(1752752, (int)noticetype, reportingBranch, 2, null).ToList();
            //            notices.AddRange(bar);
            //        }
            //    }
            //}

            //var foo = SendNotices(notices, emailSender ?? new AddToListEmailSender());

            return "";
        }

        public static void QueueUpdate(NoticeUpdate notice)
        {
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(
                    queue: Queues.EMAIL_NOTICE_UPDATES,
                    durable: true,
                    exclusive: false,
                    autoDelete: false,
                    arguments: null);

                IBasicProperties props = channel.CreateBasicProperties();
                props.ContentType = "text/plain";
                props.DeliveryMode = 2;

                channel.BasicPublish("", Queues.EMAIL_NOTICE_UPDATES, props, Encoding.UTF8.GetBytes(notice.ToJson()));
            }
        }
    }

    public class EmailNoticeGroup
    {
        public int PatronId { get; set; }
        public int LanguageId { get; set; }
        public int OrganizationId { get; set; }
        public NotificationType NotificationType { get; set; }

        public EmailNoticeGroup(PolarisNotification notice)
        {
            PatronId = notice.PatronID;
            LanguageId = notice.PatronLanguage;
            OrganizationId = notice.EmailGroupBranchId;
            NotificationType = notice.EmailGroupNotificationType;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 17;
                hash *= 23 + PatronId.GetHashCode();
                hash *= 23 + LanguageId.GetHashCode();
                hash *= 23 + OrganizationId.GetHashCode();
                hash *= 23 + NotificationType.GetHashCode();
                return hash;
            }
        }

        public override bool Equals(object obj)
        {
            if (obj is EmailNoticeGroup other)
            {
                return other.PatronId == PatronId
                    && other.LanguageId == LanguageId
                    && other.OrganizationId == OrganizationId
                    && other.NotificationType == NotificationType;
            }
            return false;
        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}