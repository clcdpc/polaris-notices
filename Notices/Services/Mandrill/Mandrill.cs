﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using NLog;
using Notices.Core;
using Notices.Core.Models;
using Notices.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;

namespace Notices.Services
{
    public class Mandrill
    {
        //static RestClient client = new RestClient("https://mandrillapp.com/api/1.0/");
        static HttpClient client = new HttpClient();
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public static bool CheckRejection(string address)
        {

            var json = new
            {
                key = AppSettings.MandrillApiKey,
                email = address
            };

            var content = new StringContent(JsonConvert.SerializeObject(json), Encoding.UTF8, "application/json");
            var result = client.PostAsync("https://mandrillapp.com/api/1.0/rejects/list.json", content).Result;

            var data = new List<RejectionListEntry>();
            try
            {
                data = JsonConvert.DeserializeObject<List<RejectionListEntry>>(result.Content.ReadAsStringAsync().Result);
            }
            catch { }

            return data != null && data.Count > 0;
        }

        public static bool RemoveRejection(string address)
        {
            var json = new
            {
                key = AppSettings.MandrillApiKey,
                email = address
            };

            var content = new StringContent(JsonConvert.SerializeObject(json), Encoding.UTF8, "application/json");
            var result = client.PostAsync("https://mandrillapp.com/api/1.0/rejects/delete.json", content).Result;

            var resultContent = result.Content.ReadAsStringAsync().Result;


            //var failure = JsonConvert.DeserializeObject<RejectionRemovalFailure>(resultContent);
            try
            {
                var success = JsonConvert.DeserializeObject<MandrillRejectionRemovalSuccess>(resultContent);
                return success.deleted;
            }
            catch (Exception ex)
            {
                logger.Error(resultContent);
                logger.Error(ex);
                return false;
            }
        }

        public static MandrillMessageSendResponse SendMessage(string to, string from, string subject, string htmlBody = null, string plaintextBody = null)
        {
            return SendMessage(new[] { to }, from, subject, htmlBody, plaintextBody);
        }

        public static MandrillMessageSendResponse SendMessage(string[] to, string from, string subject, string htmlBody = null, string plaintextBody = null)
        {
            var json = new
            {
                key = AppSettings.MandrillApiKey,
                message = new
                {
                    html = htmlBody,
                    text = plaintextBody,
                    subject = subject,
                    from_email = from,
                    to = to.Select(t => new { email = t, type = "to" })
                }
            };

            var jsonString = JsonConvert.SerializeObject(json);

            HttpResponseMessage response;
            using (var client = new HttpClient())
            {
                var content = new StringContent(JsonConvert.SerializeObject(json), Encoding.UTF8, "application/json");
                response = client.PostAsync("https://mandrillapp.com/api/1.0/messages/send.json", content).Result;
            }
            var responseJson = response.Content.ReadAsStringAsync().Result;
            var deserialized = JsonConvert.DeserializeObject<List<MandrillMessageSendResponse>>(response.Content.ReadAsStringAsync().Result);
            return deserialized.First();
        }

        public static List<MandrillSearchResult> GetMessages(string to, int historyDays = 30)
        {
            var json = new
            {
                key = AppSettings.MandrillApiKey,
                query = $"full_email:{to}",
                date_from = DateTime.Now.AddDays(historyDays * -1).ToShortDateString(),
                limit = 50
            };

            HttpResponseMessage response;
            using (var client = new HttpClient())
            {
                var content = new StringContent(JsonConvert.SerializeObject(json), Encoding.UTF8, "application/json");
                response = client.PostAsync("https://mandrillapp.com/api/1.0/messages/search.json", content).Result;
            }
            var responseJson = response.Content.ReadAsStringAsync().Result;
            return JsonConvert.DeserializeObject<List<MandrillSearchResult>>(responseJson);
        }

        public static EmailContent GetContent(string id)
        {
            var json = new
            {
                key = AppSettings.MandrillApiKey,
                id
            };

            HttpResponseMessage response;
            using (var client = new HttpClient())
            {
                var content = new StringContent(JsonConvert.SerializeObject(json), Encoding.UTF8, "application/json");
                response = client.PostAsync("https://mandrillapp.com/api/1.0/messages/content.json", content).Result;
            }
            var responseJson = response.Content.ReadAsStringAsync().Result;
            dynamic d = JObject.Parse(responseJson);

            return !string.IsNullOrWhiteSpace(d.html) ? new EmailContent(EmailFormat.Html, d.html.ToString()) : new EmailContent(EmailFormat.PlainText, d.text.ToString());
        }

        public class EmailContent
        {
            public EmailFormat Format { get; set; }
            public string Value { get; set; }

            public EmailContent()
            {

            }

            public EmailContent(EmailFormat _format, string _value)
            {
                Format = _format;
                Value = _value;
            }
        }
    }



    public class MandrillRejectionRemovalSuccess
    {
        public string email { get; set; }
        public bool deleted { get; set; }
        public string subaccount { get; set; }
    }

}