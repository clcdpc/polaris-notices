﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NLog;
using Notices.Controllers;
using Notices.Core;
using Notices.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;

namespace Notices.Services.Postmark
{
    public class PostmarkClient
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        HttpClient client = new HttpClient { BaseAddress = new Uri("https://api.postmarkapp.com") };
        string ServerApiKey { get; }
        string AccountApiKey { get; }

        public const string NO_BOUNCES_MESSAGE = "NoBouncesFound";
        public const string ALL_BOUNCES_SPAM_MESSAGE = "AllBouncesSpam";

        public PostmarkClient(string serverApiKey = "", string accountApiKey = "")
        {
            if (!string.IsNullOrWhiteSpace(serverApiKey))
            {
                ServerApiKey = serverApiKey;
                client.DefaultRequestHeaders.Add("X-Postmark-Server-Token", ServerApiKey);
            }

            if (!string.IsNullOrWhiteSpace(accountApiKey))
            {
                AccountApiKey = accountApiKey;
                client.DefaultRequestHeaders.Add("X-Postmark-Account-Token", AccountApiKey);
            }
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public bool Send(string[] to, string from, string subject, string htmlBody, string plaintextBody)
        {
            return Send(string.Join(",", to), from, subject, htmlBody, plaintextBody);
        }

        public bool Send(string to, string from, string subject, string htmlBody, string plaintextBody)
        {
            return Send(new EmailMessage { To = new[] { to }, From = from, Subject = subject, HtmlBody = htmlBody, TextBody = plaintextBody });
        }

        public void UpdateServer(int serverId, object body)
        {
            var url = $"/servers/{serverId}";
            var json = body.ToJson();
            var content = new StringContent(body.ToJson(), Encoding.UTF8, "application/json");
            var response = client.PutAsync(url, content).Result;
            var responseBody = response.Content.ReadAsStringAsync().Result;
            var foo = response;
        }

        public bool Send(EmailMessage message)
        {
            var postmarkMessage = new PostmarkMessage(message);
            //var jsonString = JsonConvert.SerializeObject(postmarkMessage, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
            var jsonString = postmarkMessage.ToJson(new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
            var content = new StringContent(jsonString, Encoding.UTF8, "application/json");

            var response = client.PostAsync("/email", content).Result;
            return response.IsSuccessStatusCode;
        }

        public Bounce GetBounce(long bounceId)
        {
            var response = client.GetStringAsync($"/bounces/{bounceId}").Result;
            var obj = JsonConvert.DeserializeObject<Bounce>(response);
            return obj;
        }

        public PostmarkBouncesGetResponse GetBounces(string email)
        {
            var response = client.GetStringAsync($"/bounces?count=50&offset=0&emailFilter={email}").Result;
            var bounces = JsonConvert.DeserializeObject<PostmarkBouncesGetResponse>(response);
            return bounces;
        }

        public PostmarkBouncesGetResponse GetBounceCountByType(string bounceType, DateTime? fromDate = null, DateTime? toDate = null)
        {
            var url = $"/bounces?count=50&offset=0&type={bounceType}";
            if (fromDate.HasValue) { url = url + $"&fromDate={fromDate.Value.ToString("yyyy-MM-dd")}"; }

            var response = client.GetStringAsync(url).Result;
            var bounces = JsonConvert.DeserializeObject<PostmarkBouncesGetResponse>(response);
            return bounces;
        }

        public RejectionRemovalResult ActivateEmail(string email)
        {
            var bounces = GetBounces(email);
            if (bounces.Bounces.Any(b => b.Type == "SpamComplaint")) { return new FailedRejectionRemovalResult("Patron has SPAM complaints, which cannot be removed automatically. Please open a helpdesk ticket and include the patron's email address."); }
            if (bounces.TotalCount == 0) { return new FailedRejectionRemovalResult($"No bounces found for address {email}."); }

            var activatableBounces = bounces.Bounces.Where(b => b.CanActivate);

            if (!activatableBounces.Any())
            {
                return new FailedRejectionRemovalResult("No activatable bounces found. This means the patron is probably able to receive emails. Add the address back to their account and open a helpdesk ticket if they receive further bounces.");
            }

            try
            {
                var status = ActivateBounce(activatableBounces.OrderBy(b => b.BouncedAt).First().Id);
                if (string.Equals(status?.Message, "ok", StringComparison.OrdinalIgnoreCase))
                {
                    return new SuccessfulRejectionRemovalResult($"Address {email} successfully reactivated.");
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }

            return new FailedRejectionRemovalResult($"Error activating email {email}.");
        }

        public PostmarkBounceActivateResponse ActivateBounce(long bounceId)
        {
            var response = client.PutAsync($"/bounces/{bounceId}/activate", null).Result;
            var body = response.Content.ReadAsStringAsync().Result;
            var obj = JsonConvert.DeserializeObject<PostmarkBounceActivateResponse>(body);
            return obj;
        }

        public EmailHistoryItemContent GetMessageContent(string messageId)
        {
            var response = client.GetAsync($"/messages/outbound/{messageId}/details").Result;
            var jsonBody = response.Content.ReadAsStringAsync().Result;

            dynamic json = JObject.Parse(jsonBody);
            string htmlBody = json["HtmlBody"];
            string textBody = json["TextBody"];

            return new EmailHistoryItemContent { HtmlBody = htmlBody, TextBody = textBody };
        }

        public List<PostmarkHistoryMessage> SearchMessages(string to, int historyDays = 45)
        {
            var fromDate = DateTime.Now.AddDays(historyDays * -1).ToShortDateString();
            var toDate = DateTime.Now.ToShortDateString();

            var url = $"/messages/outbound?recipient={to}&count=50&offset=0&status=sent&fromdate={fromDate}&todate={toDate}";
            var response = client.GetAsync(url).Result;
            var body = response.Content.ReadAsStringAsync().Result;
            var history = JsonConvert.DeserializeObject<PostmarkMessageSearchResult>(body);

            return history.Messages;
        }

        public GetSuppressionsResponse GetSuppressions(string emailAddress)
        {
            var response = client.GetAsync($"/message-streams/outbound/suppressions/dump?EmailAddress={emailAddress}").Result;
            var body = response.Content.ReadAsStringAsync().Result;
            var obj = JsonConvert.DeserializeObject<GetSuppressionsResponse>(body);

            return obj;
        }

        public RemoveSuppressionResponse RemoveSuppression(string emailAddress)
        {
            var json = new { Suppressions = new[] { new { EmailAddress = emailAddress } } };
            var response = client.PostAsync("/message-streams/outbound/suppressions/delete", new StringContent(JsonConvert.SerializeObject(json), Encoding.UTF8, "application/json")).Result;
            var body = response.Content.ReadAsStringAsync().Result;
            var obj = JsonConvert.DeserializeObject<RemoveSuppressionResponse>(body);

            return obj;
        }
    }    

    public class EmailHistoryItemContent
    {
        public string HtmlBody { get; set; }
        public string TextBody { get; set; }

        public string GetContent()
        {
            return string.IsNullOrWhiteSpace(HtmlBody) ? TextBody : HtmlBody;
        }

        public string GetContentType()
        {
            return string.IsNullOrWhiteSpace(HtmlBody) ? "text/plaintext" : "text/html";
        }
    }
}