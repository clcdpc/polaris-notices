﻿using NLog;
using Notices.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Notices
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        private static Logger logger = LogManager.GetLogger("UnhandledExceptions");

        protected void Application_Error()
        {
            var ex = Server.GetLastError();
            logger.Fatal(ex.InnerException ?? ex);
            LogManager.Flush();

            if (Context == null) { return; }

            var auth = Context.Request.QueryString["auth"];
            if (auth == AppSettings.AuthString)
            {
                Response.Clear();

                if (ex.InnerException != null)
                {
                    Response.Write($"<h1>Inner Exception</h1><h2>{ex.InnerException.Message}</h2><pre>{ex}</pre><hr/>");
                }

                Response.Write($"<h2>{ex.Message}</h2><pre>{ex}</pre>");

                Server.ClearError();
            }
        }
    }
}
