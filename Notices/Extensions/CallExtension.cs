﻿using Notices.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using clc_twilio_csharp.Models;

namespace Notices.Extensions
{
	public static class CallExtension
	{
		public static bool HasRecording(this Call call) 
		{
			return call.StartTime > Dialout.RecordingCutOffDate;
		}
	}
}
