﻿using Notices.Core.Data;
using Notices.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Notices.Extensions
{
    public static class DialoutStringsExtension
    {
        public static MessagePart ToMessagePart(this Dialout_Strings dialoutString)
        {
            var val = dialoutString.Value;
            return val.StartsWith("http")
                   ? new MessagePart { PartType = PartType.File, PartText = val }
                   : new MessagePart { PartType = PartType.String, PartText = val.TrimEnd() + " " };
        }
    }
}