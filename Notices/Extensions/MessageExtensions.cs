﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using clc_twilio_csharp.Models;

namespace Notices.Extensions
{
    public static class MessageExtensions
    {
        public static bool IsError(this Message msg)
        {
            return msg.Status.Equals("undelivered", StringComparison.OrdinalIgnoreCase);
        }
    }
}