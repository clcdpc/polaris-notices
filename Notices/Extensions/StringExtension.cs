﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Notices.Extensions
{
    public static class StringExtension
    {
        public static string Pluralize(this string input, int count, string pluralAdditionalText = "s")
        {
            return count == 1 ? input : input + pluralAdditionalText;
            //return string.Format("{0}{1}", input, count == 1 ? "" : "s");
        }

        public static string DigitsOnly(this string input)
        {
            input = input ?? "";
            return new string(input.Where(char.IsDigit).ToArray());
        }
    }
}