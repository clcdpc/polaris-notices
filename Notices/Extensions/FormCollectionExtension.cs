﻿using Newtonsoft.Json;
using Notices.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Notices.Extensions
{
    public static class FormCollectionExtension
    {
        public static IEnumerable<MandrillMailEvent> GetEvents(this FormCollection fc)
        {
            string json = fc["mandrill_events"];
            return JsonConvert.DeserializeObject<IEnumerable<MandrillMailEvent>>(json);
        }
    }
}