﻿using Notices.Core;
using Notices.Core.Data;
using Notices.Models;
using Notices.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Notices.Extensions
{
    public static class OrganizationExtensions
    {
        public static MessagePart GetString(this PolarisNotification notice, MessageType type, bool tryParentOrg = true)
        {
            var msgString = "";

            int stringType = (int)type;

            var messageBranch = notice.PatronBranch;
            var messageLibrary = notice.PatronLibrary;

            var useReportingOrgMessageTypes = new[] { MessageType.Location, MessageType.HoldMessageFirst, MessageType.HoldMessageAdditional };

            if (useReportingOrgMessageTypes.Contains(type))
            {
                messageBranch = notice.ReportingBranchID;
                messageLibrary = notice.ReportingLibraryID;
            }

            if (type == MessageType.HoldDateFormats)
            {
                var branchOverrideDateFormat = DbSettings.GetValue<bool>(Mnemonics.OverrideHoldDateFormat, notice.ReportingBranchID, notice.PatronLanguage);
                if (branchOverrideDateFormat)
                {
                    messageBranch = notice.ReportingBranchID;
                    tryParentOrg = false;
                }
            }

            try { msgString = Dialout.Strings.SingleOrDefault(s => s.OrganizationID == messageBranch && s.StringTypeID == stringType)?.Value; }
            catch { }

            if (string.IsNullOrWhiteSpace(msgString))
            {
                if (tryParentOrg)
                {
                    try { msgString = Dialout.Strings.SingleOrDefault(s => s.OrganizationID == messageLibrary && s.StringTypeID == stringType)?.Value; }
                    catch { }
                }
                else
                {
                    if (type == MessageType.Location)
                    {
                        msgString = notice.ReportingBranchName;
                    }
                    else
                    {
                        throw new ArgumentException($"No entry exists for message type {type} for {messageBranch}");
                    }
                }
            }

            if (string.IsNullOrWhiteSpace(msgString))
                msgString = Dialout.Strings.Single(s => s.OrganizationID == 1 && s.StringTypeID == stringType).Value;

            foreach (Match match in Regex.Matches(msgString, "{([a-zA-Z0-9]{5,}?)}"))
            {
                try
                {
                    var ph = match.Value;
                    var propValue = notice.GetType().GetProperty(match.Value).GetValue(notice).ToString();

                    msgString = msgString.Replace(ph, propValue);
                }
                catch (Exception ex)
                {

                }
            }

            if (msgString.StartsWith("http"))
            {
                return new MessagePart { PartType = PartType.File, PartText = msgString };
            }

            return new MessagePart { PartType = PartType.String, PartText = (msgString ?? "").TrimEnd() + " " };// msgString.Reverse().First() == ' ' ? msgString : msgString + " " };
        }

        public static MessagePart GetString2(this PolarisNotification notice, MessageType type, bool tryParentOrg = true)
        {
            var msgString = "";

            int stringType = (int)type;

            var messageBranch = notice.PatronBranch;
            var messageLibrary = notice.PatronLibrary;

            if (type == MessageType.Location)
            {
                messageBranch = notice.ReportingBranchID;
                messageLibrary = notice.ReportingLibraryID;
            }

            if (type == MessageType.HoldDateFormats)
            {
                var branchOverrideDateFormat = DbSettings.OverrideHoldDateFormat(notice.ReportingBranchID, notice.PatronLanguage);
                if (branchOverrideDateFormat)
                {
                    messageBranch = notice.ReportingBranchID;
                    tryParentOrg = false;
                }
            }

            var orgs = new List<int> { messageBranch, 1 };
            if (tryParentOrg) { orgs.Add(messageLibrary); }

            var vals = Dialout.Strings.Where(s => orgs.Contains(s.OrganizationID));
            if (!vals.Any()) { throw new ArgumentException($"No entry exists for message type {type} for {messageBranch}"); }

            var val = vals.OrderByDescending(s => s.OrganizationID).First();
            return val.ToMessagePart();



            try { msgString = Dialout.Strings.SingleOrDefault(s => s.OrganizationID == messageBranch && s.StringTypeID == stringType).Value; }
            catch { }

            if (string.IsNullOrWhiteSpace(msgString))
            {
                if (tryParentOrg)
                {
                    try { msgString = Dialout.Strings.SingleOrDefault(s => s.OrganizationID == messageLibrary && s.StringTypeID == stringType).Value; }
                    catch { }
                }
                else
                {

                }
            }

            if (string.IsNullOrWhiteSpace(msgString))
                msgString = Dialout.Strings.Single(s => s.OrganizationID == 1 && s.StringTypeID == stringType).Value;

            if (msgString.StartsWith("http"))
            {
                return new MessagePart { PartType = PartType.File, PartText = msgString };
            }

            return new MessagePart { PartType = PartType.String, PartText = msgString.Reverse().First() == ' ' ? msgString : msgString + " " };
        }


    }
}
