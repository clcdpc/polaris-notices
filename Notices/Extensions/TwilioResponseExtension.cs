﻿using Clc.Polaris.Api;
using Clc.Polaris.Api.Models;
using Newtonsoft.Json;
using NLog;
using Notices.Core;
using Notices.Core.Data;
using Notices.Models;
using Notices.Services;
using Notices.Services.Postmark;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Hosting;
using Twilio.TwiML;

namespace Notices.Extensions
{
    public static class TwilioResponseExtension
    {
        static object voiceAttribute = new { voice = "alice" };
        static IPapiClient polaris = Methods.CreatePapiClient();
        static Logger logger = LogManager.GetCurrentClassLogger();

        public static TwilioResponse AddMessagePart(this TwilioResponse response, MessagePart part, string additionalText = "")
        {
            if (part.PartType == PartType.File)
            {
                response.Play(part.PartText);
                if (additionalText.Length > 0)
                {
                    response.Say(additionalText, voiceAttribute);
                }
            }
            else
            {
                if (additionalText.Length > 0)
                {
                    response.Say(string.Format("{0} {1}", part.PartText, additionalText), voiceAttribute);
                }
                else
                {
                    response.Say(part.PartText, voiceAttribute);
                }
            }

            return response;
        }

        public static TwilioResponse BuildMainMenu(this TwilioResponse twiml, string callsid)
        {
            var itemsOut = CallVars.ItemsOut[callsid].ToList();

            int almostOverdueCount = 0, overdueCount = 0;
            if (itemsOut.Count() > 0)
            {
                almostOverdueCount = itemsOut.Count(item => item.DueDate <= DateTime.Today.AddDays(7) && item.DueDate >= DateTime.Today);
                overdueCount = itemsOut.Count(i => i.DueDate <= DateTime.Now);
            }

            twiml.BeginGather(new { action = "/dialin/index?node=default", numDigits = "1" });

            CallVars.SkipIntro[callsid] = CallVars.SkipIntro.Keys.Contains(callsid) ? CallVars.SkipIntro[callsid] : false;

            // Only play the intro if the patron is initially calling in and has not heard it.
            if (!CallVars.SkipIntro[callsid])
            {
                CallVars.SkipIntro[callsid] = true;
                twiml.Say(string.Format("Hello {0}, thank you for calling the {1}.", CallVars.PatronData[callsid].NameFirst.ToLower(), CallVars.OtherPatronInfo[callsid].AssignedBranchName), voiceAttribute);
                twiml.Say("Press 0 at any time to be connected to your library or press 7 to be taken to this menu.", voiceAttribute);
                var itemsMsg = string.Format("You currently have {0} {1} checked out.", itemsOut.Count(), "item".Pluralize(itemsOut.Count()));
                if (overdueCount > 0) itemsMsg += string.Format(" {0} {1} currently overdue.", overdueCount, overdueCount == 1 ? "item is" : "items are");
                if (almostOverdueCount > 0) itemsMsg += string.Format(" {0} {1} due within the next 7 days.", almostOverdueCount, almostOverdueCount == 1 ? "item is" : "items are");

                twiml.Say(itemsMsg, voiceAttribute);
            }
            else
            {
                twiml.Say("Main menu.", voiceAttribute);
            }

            if (!CallVars.Holds.Keys.Contains(callsid))
            {
                Dialin.RefreshHeldItems(callsid);
            }

            var holdsCount = CallVars.Holds[callsid].Count;
            if (holdsCount > 0)
            {
                twiml.Say(string.Format("You have {0} {1} ready for pickup. To hear hold information, press 4.", holdsCount, "item".Pluralize(holdsCount)), voiceAttribute);
            }
            twiml.Say("To renew all items, press 1.", voiceAttribute);
            twiml.Say("To list the items you currently have checked out and renew individual items, press 2.", voiceAttribute);
            twiml.EndGather();
            twiml.Redirect("/dialin/index?node=default");

            return twiml;
        }

        public static TwilioResponse ListHolds(this TwilioResponse twiml, string callsid)
        {
            //var test = polaris.PatronHoldRequestsGet(CallVars.PatronBarcodes[callsid], CallVars.PatronPINs[callsid], HoldStatus.held);
            // If there is a list of holds for the current call then use that, otherwise use the Polaris API
            // to get the holds for current patron.
            if (!CallVars.Holds.Keys.Contains(callsid))
            {
                Dialin.RefreshHeldItems(callsid);
            }

            var holds = CallVars.Holds[callsid];
            CallVars.HoldIndex[callsid] = CallVars.HoldIndex.Keys.Contains(callsid) ? CallVars.HoldIndex[callsid] : 0;

            if (holds.Count == 0)
            {
                twiml.Say("You currently have no holds ready for pickup. Returning to main menu.", voiceAttribute);
                twiml.Redirect("/dialin/index?node=default");
                return twiml;
            }

            if (CallVars.HoldIndex[callsid] >= CallVars.Holds[callsid].Count)
            {
                CallVars.HoldIndex[callsid] = 0;
                twiml.Say("You have no more holds to list. Returning to main menu.", voiceAttribute);
                twiml.Redirect("/dialin/index?node=default");
                return twiml;
            }

            if (CallVars.HoldIndex[callsid] == 0)
            {
                twiml.Say(string.Format("You currently have {0} {1} ready for pickup.", holds.Count, "hold".Pluralize(holds.Count)), voiceAttribute);
            }

            var currentHold = holds[CallVars.HoldIndex[callsid]];

            twiml.BeginGather(new { action = "/dialin/index?node=holdOptions", numDigits = "1", timeout = "2" });
            twiml.Say(string.Format("{0}, {1}. available for pickup at {2} until {3}.",
                holds.Count == CallVars.HoldIndex[callsid] + 1
                    ? holds.Count == 1
                        ? "Your hold ready for pickup is"
                        : "Your final hold ready for pickup is"
                    : string.Format("Your {0} hold ready for pickup is", IntToString((CallVars.HoldIndex[callsid] + 1))),
                FormatTitle(currentHold.Title),
                currentHold.PickupBranchName,
                currentHold.PickupByDate.HasValue ? currentHold.PickupByDate.Value.ToString("dddd MMMM dd") : ""), voiceAttribute);
            twiml.Pause(1);

            twiml.Say("To hear your next hold, press 1. To return to the main menu, press 7.", voiceAttribute);
            twiml.EndGather();
            twiml.Redirect("/dialin/index?node=holdOptions");

            return twiml;
        }

        public static TwilioResponse NextHold(this TwilioResponse twiml, string callsid)
        {
            CallVars.HoldIndex[callsid]++;
            twiml.Redirect("/dialin/index?node=default&digits=4");
            return twiml;
        }

        public static TwilioResponse CancelHold(this TwilioResponse twiml, string callsid)
        {
            twiml.BeginGather(new { action = "/dialin/index?node=holdOptions", numDigits = 1, timeout = 7 });
            twiml.Say("Cancelling this hold will remove you from the queue and you will be unable to reclaim your spot in line. To cancel this hold press 5. To return to your list of holds press 1. To return to the main menu press 7.", voiceAttribute);
            twiml.EndGather();
            twiml.Redirect("/dialin/index?node=default&digits=4");
            return twiml;
        }

        public static TwilioResponse ConfirmCancel(this TwilioResponse twiml, string callsid)
        {
            var currentHold = CallVars.Holds[callsid][CallVars.HoldIndex[callsid]];
            var barcode = CallVars.PatronBarcodes[callsid];
            var pin = CallVars.PatronPINs[callsid];


            var result = polaris.HoldRequestCancel(barcode, currentHold.RequestId, pin).Data;

            if (string.IsNullOrEmpty(result.ErrorMessage))
            {
                Dialin.RefreshHeldItems(callsid);
                twiml.Say("Hold successfully cancelled, returning to hold list", voiceAttribute);
                twiml.Redirect("/dialin/index?node=default&digits=4");
                return twiml;
            }

            twiml.Say("Your hold could not be cancelled because: ", voiceAttribute);
            twiml.Say(result.ErrorMessage);
            twiml.Say("Please contact the library for more information. Returning to hold list.", voiceAttribute);
            twiml.Redirect("/dialin/index?node=default&digits=4");
            return twiml;
        }

        public static TwilioResponse BuildItemListMenu(this TwilioResponse twiml, string callsid)
        {
            CallVars.ItemType[callsid] = ItemType.All;
            List<GetCheckedOutItems_Result> overdueItems;
            var db = new DbHelper(AppSettings.DbServer);
            Dialin.RefreshItemsOut(callsid);
            overdueItems = db.GetCheckedOutItems(CallVars.PatronData[callsid].PatronID, 1).ToList();

            CallVars.ItemsOutIndex[callsid] = 0;

            // Tell the patron if they have no items out.
            if (CallVars.ItemsOut[callsid].Count() < 1)
            {
                twiml.Say("You currently have no items checked out. Returning to main menu.", voiceAttribute);
                twiml.Redirect("/dialin/index?node=default");
                return twiml;
            }

            twiml.BeginGather(new { action = "/dialin/index?node=listItems", numDigits = 1 });
            if (overdueItems.Count() > 0)
            {
                twiml.Say(string.Format("You currently have {0} {1} overdue. To list only {2} press 5.", overdueItems.Count(), "item".Pluralize(overdueItems.Count()), overdueItems.Count() == 1 ? "this item" : "these items"), voiceAttribute);
            }
            twiml.Say("To hear a list of all titles you currently have checked out press 1.", voiceAttribute);
            twiml.Say("To look up an item by barcode press 2.", voiceAttribute);
            if (AppSettings.DialinEnableEmailListOption && !string.IsNullOrWhiteSpace(CallVars.PatronData[callsid].EmailAddress) && Methods.GetEmailSender()?.GetType() != null)
            {
                twiml.Say("To have a list of your checked out titles sent to your email address, press 3", voiceAttribute);
            }
            twiml.Say("To return to the main menu press 7.", voiceAttribute);
            twiml.EndGather();
            twiml.Redirect("/dialin/index?node=listItems");
            return twiml;
        }

        public static TwilioResponse ListOverdueItems(this TwilioResponse twiml, string callsid)
        {
            if (CallVars.ItemType[callsid] != ItemType.Overdue)
            {
                CallVars.ItemType[callsid] = ItemType.Overdue;
                Dialin.RefreshItemsOut(callsid, true);
                CallVars.ItemsOutIndex[callsid] = 0;
            }

            return twiml.ListItems(callsid);
        }

        public static TwilioResponse EmailTitles(this TwilioResponse twiml, string callsid)
        {
            var patron = CallVars.PatronData[callsid];
            if (string.IsNullOrEmpty(patron.EmailAddress))
            {
                twiml.Say("You currently have no email address on file, returning to main menu", voiceAttribute);
                twiml.Redirect("/dialin/index?node=default");
                return twiml;
            }

            var items = new List<GetCheckedOutItems_Result>();
            var db = new DbHelper(AppSettings.DbServer);
            items = db.GetCheckedOutItems(CallVars.PatronData[callsid].PatronID).ToList();


            if (items.Count() < 1)
            {
                twiml.Say("You currently have no items checked out, returning to the main menu.", voiceAttribute);
                twiml.Redirect("/dialin/index?node=default");
                return twiml;
            }

            if (!Dialin.EmailTitleList(patron.EmailAddress, items))
            {
                twiml.Say("An error has occurred, please try again. If this error persists please contact your library for assistance.", voiceAttribute);
                return twiml;
            }
            twiml.Say("An email has been sent to the address on file and you should receive it in a few minutes. Returning to the main menu.", voiceAttribute);
            twiml.Redirect("/dialin/index?node=default");

            return twiml;
        }

        public static TwilioResponse ItemByBarcode(this TwilioResponse twiml, string callsid)
        {
            twiml.BeginGather(new { action = "/dialin/RenewItemByBarcode", timeout = 10 });
            twiml.Say("Enter the barcode of the item you wish to renew, followed by the pound key. Or press 7 to go back to the main menu.", voiceAttribute);
            twiml.EndGather();
            twiml.Redirect("/dialin/index?node=listItems&digits=2");

            return twiml;
        }

        public static TwilioResponse ListAllItems(this TwilioResponse twiml, string callsid)
        {
            if (CallVars.ItemType[callsid] != ItemType.All)
            {
                CallVars.ItemType[callsid] = ItemType.All;
                Dialin.RefreshItemsOut(callsid);

                CallVars.ItemsOutIndex[callsid] = 0;
            }
            // Get the position the patron is at in the list of items, if any.
            CallVars.ItemsOutIndex[callsid] = CallVars.ItemsOutIndex.Keys.Contains(callsid) ? CallVars.ItemsOutIndex[callsid] : 0;

            return twiml.ListItems(callsid);
        }

        public static TwilioResponse ListItems(this TwilioResponse twiml, string callsid)
        {
            var itemsOut = CallVars.ItemsOut[callsid];

            if (itemsOut == null || itemsOut.Count() == 0)
            {
                if (CallVars.ItemType[callsid] == ItemType.Overdue)
                {
                    twiml.Say("You currently have no overdue items", voiceAttribute);
                }
                else
                {
                    twiml.Say("You currently have no items checked out. Returning to main menu.", voiceAttribute);
                }

                twiml.Redirect("/dialin/index?node=default");
                return twiml;
            }

            // If somehow the patron gets past the number of items send them back to the menu.
            if (CallVars.ItemsOutIndex[callsid] >= itemsOut.Count())
            {
                twiml.Say("You have no more items to list, returning to the main menu.", voiceAttribute);
                twiml.Redirect("/dialin/index?node=default");
                CallVars.ItemsOutIndex[callsid] = 0;
                return twiml;
            }

            // Get the information for the current item
            var currentItem = itemsOut.ElementAt(CallVars.ItemsOutIndex[callsid]);
            int itemCount = itemsOut.Count();

            // Tell the patron about the current title.
            twiml.BeginGather(new { action = "/dialin/index?node=renewOptions", numDigits = "1", timeout = "2" });
            twiml.Say(string.Format("Your {0} {1} item is, {2}, due back on {3}",
                itemCount == 1 ? "only" : itemCount == CallVars.ItemsOutIndex[callsid] + 1 ? "final" : IntToString((CallVars.ItemsOutIndex[callsid] + 1)),
                CallVars.ItemType[callsid] == ItemType.Overdue ? "overdue" : "checked out",
                FormatTitle(currentItem.BrowseTitle),
                currentItem.DueDate.ToString("dddd MMMM dd")), voiceAttribute);
            twiml.Pause(1);
            twiml.Say("To renew this item, press 1. To hear the next title, press 2. To return to the main menu, press 7.", voiceAttribute);
            twiml.EndGather();
            twiml.Redirect("/dialin/index?node=listItems&digits=" + (CallVars.ItemType[callsid] == ItemType.All ? "1" : "2"));

            return twiml;
        }

        public static TwilioResponse RenewItem(this TwilioResponse twiml, string callsid)
        {
            var patronBarcode = CallVars.PatronBarcodes[callsid];
            var patronPIN = CallVars.PatronPINs[callsid];
            var currentItem = CallVars.ItemsOut[callsid].ElementAt(CallVars.ItemsOutIndex[callsid]);

            if (!CallVars.ItemRenewResults.Keys.Contains(callsid))
            {
                CallVars.ItemRenewResults[callsid] = polaris.ItemRenew(patronBarcode, currentItem.ItemRecordID, patronPIN).Data.ItemRenewResult;
            }

            var renewResult = CallVars.ItemRenewResults[callsid];

            if (renewResult.DueDateRows.Count > 0)
            {
                var db = new DbHelper(AppSettings.DbServer);
                CallVars.ItemsOut[callsid].Single(p => p.ItemRecordID == currentItem.ItemRecordID).DueDate = db.GetCheckedOutItems(CallVars.PatronData[callsid].PatronID).Single(p => p.ItemRecordID == currentItem.ItemRecordID).DueDate;


                twiml.Say(string.Format("Your item was successfully renewed. This item is due back on {0}.", renewResult.DueDateRows.First().DueDate.ToString("dddd MMMM dd")), voiceAttribute);
                twiml.Redirect("/dialin/index?node=renewOptions&digits=2");
                CallVars.ItemRenewResults.Remove(callsid);
                return twiml;
            }

            if (renewResult.BlockRows.Count > 0)
            {
                twiml.BeginGather(new { action = "/dialin/index?node=renewOptions", numDigits = 1 });
                twiml.Say(string.Format("Your item was unable to be renewed because, {0}", renewResult.BlockRows.First().ErrorDesc), voiceAttribute);
                twiml.Say("Press 2 to confirm and return to your list of items out.", voiceAttribute);
                twiml.EndGather();
                twiml.Redirect("/dialin/index?node=renewOptions&digits=1");
                CallVars.ItemRenewResults.Remove(callsid);
                return twiml;
            }

            twiml.BeginGather(new { action = "/dialin/index?node=renewOptions", numDigits = 1 });
            twiml.Say("Your item was unable to be renewed due to an unknown error. If this problem persists please contact your library for assistance.", voiceAttribute);
            twiml.Say("Press 2 to confirm and return to your list of items out.");
            twiml.EndGather();
            twiml.Redirect("/dialin/index?node=renewOptions&digits=1");

            return twiml;
        }

        public static TwilioResponse NextItem(this TwilioResponse twiml, string callsid)
        {
            CallVars.ItemsOutIndex[callsid]++;
            twiml.Redirect("/dialin/index?node=listItems&digits=" + (CallVars.ItemType[callsid] == ItemType.All ? "1" : "5"));
            return twiml;
        }

        public static TwilioResponse RenewAll(this TwilioResponse twiml, string callsid)
        {
            Dialin.RefreshItemsOut(callsid);
            var itemsOut = CallVars.ItemsOut[callsid];

            if (itemsOut.Count() == 0)
            {
                twiml.Say("You currently have no items to renew, returning to main menu.", voiceAttribute);
                twiml.Redirect("/dialin/index?node=default");
                return twiml;
            }

            var response = polaris.ItemRenewAllForPatron(CallVars.PatronBarcodes[callsid], CallVars.PatronPINs[callsid]);
            var result = response.Data.ItemRenewResult;

            var db = new DbHelper(AppSettings.DbServer);
            var eItems = db.GetCheckedOutItems(CallVars.PatronData[callsid].PatronID, 0).Where(i => i.ElectronicItem).Select(i => i.ItemRecordID).ToList() ?? new List<int>();


            var groupedBlocks = result.BlockRows.Where(r => !eItems.Contains(r.ItemRecordID)).GroupBy(r => r.ItemRecordID).Select(g => g.ToList()).ToList();

            twiml.BeginGather(new { action = "/dialin/index?node=menu", numDigits = "1", timeout = "1" });
            twiml.Say(string.Format("{0} {1} successfully renewed and {2} {3} unable to be renewed.",
                result.DueDateRows.Count,
                result.DueDateRows.Count == 1 ? "item was" : "items were",
                groupedBlocks.Count,
                groupedBlocks.Count == 1 ? "item was" : "items were"),
                voiceAttribute);

            // If there were items that were unable to be renewed. Tell the patron which items and why.
            if (result.BlockRows.Count > 0)
            {
                twiml.Say(string.Format("We were unable to renew the following {0}. ", "item".Pluralize(groupedBlocks.Count())), voiceAttribute);
                foreach (var row in groupedBlocks)
                {
                    var item = itemsOut.SingleOrDefault(t => t.ItemRecordID == row.First().ItemRecordID);
                    if (item == null)
                    {
                        logger.Error("ItemRenewAll item null {0}", row.First().ItemRecordID);
                        continue;
                    }

                    twiml.Say(string.Format("{0} because {1}.", FormatTitle(item.BrowseTitle), row.First().ErrorDesc), voiceAttribute);

                    if (row.First().ItemRecordID != groupedBlocks.Last().First().ItemRecordID)
                    {
                        twiml.Pause(1);
                    }
                }
            }
            twiml.EndGather();
            twiml.Redirect("/dialin/index?node=default");

            return twiml;

        }

        static string IntToString(int i)
        {
            string[] unitsAndTeens = { "First", "Second", "Third", "Fourth", "Fifth", "Sixth", "Seventh", "Eighth", "Ninth", "Tenth", "Eleventh", "Twelfth", "Thirteenth", "Fourteenth",
                "Fifteenth", "Sixteenth", "Seventeenth", "Eighteenth", "Nineteenth", "Twentieth"};

            if (i <= 20)
            {
                return unitsAndTeens[i - 1];
            }

            return i.ToString();
        }

        public static string FormatTitle(string title)
        {
            title = title.PadRight(100);
            return title.Substring(0, title.IndexOf(' ', 50)).Trim();
        }
    }
}