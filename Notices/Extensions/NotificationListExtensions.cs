﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Notices.Models;
using Twilio.TwiML;
using System.Configuration;
using Notices.Core.Data;
using Notices.Core;
using System.Web.Script.Serialization;
using Notices.Services;

namespace Notices.Extensions
{
    public static class NotificationListExtensions
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        public static string GetString(this List<PolarisNotification> notices, string mnemonic)
        {
            return DbSettings.GetValue<string>(mnemonic, notices.First().EmailTemplateBranchId, notices.First().PatronLanguage);
        }

        public static string BranchHoldMessage(this List<PolarisNotification> notices, bool isFirstBranch = false)
        {
            var formatString = notices.GetString(isFirstBranch ? MessageType.HoldMessageFirst : MessageType.HoldMessageAdditional).ToString();

            return string.Format(formatString,
                        notices.Count(),
                        notices.Count() == 1 ? "item" : "items",
                        notices.GetString(MessageType.Location, false),
                        notices.Min(t => t.HoldTillDate).Value.ToString(notices.GetString(MessageType.HoldDateFormats).ToString()));
        }

        public static string PhoneMessageTwiML(this List<PolarisNotification> notices, string answeredBy)
        {
            try
            {
                logger.Trace(new JavaScriptSerializer().Serialize(notices));
            }
            catch (Exception ex) { logger.Error(ex); }

            var response = new TwilioResponse();

            if (notices.Select(n => n.PatronID).Distinct().Count() > 1)
            {
                var ids = string.Join(", ", notices.Select(n => n.PatronID).Distinct().ToList());
                throw new Exception(string.Format("This method can only be used on a list of notices for a single patron. Currently the list contains patrons {0}", ids));
            }

            var messageBody = new StringBuilder();
            var firstNotice = notices.First();

            messageBody.Append(firstNotice.GetString(MessageType.Intro));

            if (notices.HasBills())
            {
                messageBody.Append(notices.GetString(MessageType.BillMessage));

                if (notices.HasHolds() || notices.HasOverdues())
                {
                    messageBody.Append(" and, ");
                }
            }

            int firstBranch = 0;
            int lastBranch = 0;
            if (notices.HasHolds())
            {
                firstBranch = notices.Where(n => n.IsAnyHold).Select(n => n.ReportingBranchID).Distinct().First();
                lastBranch = notices.Where(n => n.IsAnyHold).Select(n => n.ReportingBranchID).Distinct().Last();
            }

            // Group holds by pickup branch
            foreach (var branchHolds in notices.Where(n => n.IsAnyHold).GroupBy(t => t.ReportingBranchID))
            {
                var hold = branchHolds.First();
                var isOnlySecondHolds = branchHolds.All(h => h.IsSecondHold);
                var isFirstBranch = hold.ReportingBranchID == firstBranch;

                if (hold.ReportingBranchID == firstBranch)
                {
                    var formatString = hold.GetString(MessageType.HoldMessageFirst).ToString();

                    messageBody.AppendFormat(formatString,
                            branchHolds.Count(),
                            branchHolds.Count() == 1 ? "item" : "items",
                            hold.GetString(MessageType.Location, false),
                            branchHolds.Min(t => t.HoldTillDate).Value.ToString(hold.GetString(MessageType.HoldDateFormats).ToString()));

                    if (hold.ReportingBranchID == lastBranch)
                    {
                        messageBody.Append(notices.HasOverdues() ? " and, " : ". ");
                    }
                }
                else
                {
                    if (hold.ReportingBranchID == lastBranch)
                    {
                        messageBody.Append(notices.HasOverdues() ? ", " : " and, ");
                    }
                    else
                    {
                        messageBody.Append(", ");
                    }

                    messageBody.AppendFormat(hold.GetString(MessageType.HoldMessageAdditional).ToString(),
                            branchHolds.Count(),
                            branchHolds.Count() == 1 ? "item" : "items",
                            hold.GetString(MessageType.Location, false),
                            branchHolds.Min(t => t.HoldTillDate).Value.ToString(hold.GetString(MessageType.HoldDateFormats).ToString()));

                    if (hold.ReportingBranchID == lastBranch)
                    {
                        messageBody.Append(notices.HasOverdues() ? " and, " : ".");
                    }
                }
            }

            // Patron has overdues
            if (notices.HasOverdues())
            {   // Get the overdue message for the library				
                messageBody.Append(notices.GetString(MessageType.Overdue));
            }

            var voiceAttribute = new { voice = "alice" };

            string patronName = string.Format("{0} {1}.", firstNotice.NameFirst, firstNotice.NameLast).ToLower();

            var greeting = notices.GetString(MessageType.Greeting);
            var renewal = notices.GetString(MessageType.Renewal);
            var goodbye = notices.GetString(MessageType.Goodbye);
            var repeat = notices.GetString(MessageType.Repeat);

            response.AddMessagePart(greeting, patronName);
            response.Say(messageBody.ToString(), voiceAttribute);

            if (notices.HasOverdues())
            {
                var dialinUrl = string.Format("{0}?isTransfer=true&transferredBarcode={1}&transferredPatronId={2}", AppSettings.DialinURL, notices.First().PatronBarcode, notices.First().PatronID);
                response.BeginGather(new { numDigits = 1, action = dialinUrl, method = "GET", timeout = 1 });
                response.AddMessagePart(renewal);
                response.Pause(1);
            }

            response.AddMessagePart(repeat);
            response.AddMessagePart(greeting, patronName);
            response.Say(messageBody.ToString(), voiceAttribute);
            response.AddMessagePart(goodbye);

            if (notices.HasOverdues())
            {
                response.EndGather();
            }

            response.Hangup();

            return response.ToString();
        }

        public static string PhoneMessageTwiML2(this List<PolarisNotification> notices, string answeredBy)
        {
            try
            {
                logger.Trace(new JavaScriptSerializer().Serialize(notices));
            }
            catch (Exception ex) { logger.Error(ex); }

            var response = new TwilioResponse();

            if (notices.Select(n => n.PatronID).Distinct().Count() > 1)
            {
                var ids = string.Join(", ", notices.Select(n => n.PatronID).Distinct().ToList());
                throw new Exception(string.Format("This method can only be used on a list of notices for a single patron. Currently the list contains patrons {0}", ids));
            }

            var messageBody = new StringBuilder();

            messageBody.Append(notices.GetString(MessageType.Intro));

            var groupedHolds = notices.Where(n => n.IsAnyHold).GroupBy(t => t.ReportingBranchID).ToList();

            for (int i = 0; i < groupedHolds.Count; i++)
            {
                var branchHolds = groupedHolds[i].ToList();
                var isOnlySecondHolds = branchHolds.All(h => h.IsSecondHold);

                var isFirstBranch = i == 0;
                var isLastBranch = i == groupedHolds.Count - 1;
                var isSecondToLastBranch = i == groupedHolds.Count - 2;

                if (isOnlySecondHolds) { messageBody.Append(branchHolds.GetString(MessageType.SecondHoldOnlyPrepend)); }
                messageBody.Append(branchHolds.BranchHoldMessage(isFirstBranch));
                if (isOnlySecondHolds) { messageBody.Append(branchHolds.GetString(MessageType.SecondHoldOnlyAppend)); }

                if (isLastBranch)
                {
                    messageBody.Append(notices.HasOverdues() ? " and, " : ". ");
                }
                else
                {
                    messageBody.Append(isSecondToLastBranch && !notices.HasOverdues() ? "and, " : ", ");
                }
            }

            // Patron has overdues
            if (notices.HasOverdues())
            {   // Get the overdue message for the library				
                messageBody.Append(notices.GetString(MessageType.Overdue));
            }

            var voiceAttribute = new { voice = "alice" };

            string patronName = string.Format("{0} {1}.", notices.First().NameFirst, notices.First().NameLast).ToLower();

            var greeting = notices.GetString(MessageType.Greeting);
            var renewal = notices.GetString(MessageType.Renewal);
            var goodbye = notices.GetString(MessageType.Goodbye);
            var repeat = notices.GetString(MessageType.Repeat);

            response.AddMessagePart(greeting, patronName);
            response.Say(messageBody.ToString(), voiceAttribute);

            if (notices.HasOverdues())
            {
                var dialinUrl = string.Format("{0}?isTransfer=true&transferredBarcode={1}&transferredPatronId={2}", AppSettings.DialinURL, notices.First().PatronBarcode, notices.First().PatronID);
                response.BeginGather(new { numDigits = 1, action = dialinUrl, method = "GET" });
                response.AddMessagePart(renewal);
                response.EndGather();
            }

            response.AddMessagePart(repeat);
            response.AddMessagePart(greeting, patronName);
            response.Say(messageBody.ToString(), voiceAttribute);
            response.AddMessagePart(goodbye);
            response.Hangup();

            return response.ToString();
        }


        public static MessagePart GetString(this List<PolarisNotification> notices, MessageType type, bool tryParentOrg = true)
        {
            return notices.First().GetString(type, tryParentOrg);
            //var notice = notices.First();
            //var msgString = "";

            //int stringType = (int)type;

            //var messageBranch = notice.PatronBranch;
            //var messageLibrary = notice.PatronLibrary;

            //if (type == MessageType.Location)
            //{
            //    messageBranch = notice.ReportingBranchID;
            //    messageLibrary = notice.ReportingLibraryID;
            //}

            //if (type == MessageType.HoldMessageFirst || type == MessageType.HoldMessageAdditional)
            //{
            //    messageBranch = notice.ReportingBranchID;
            //    messageLibrary = notice.ReportingLibraryID;
            //}

            //if (type == MessageType.HoldDateFormats)
            //{
            //    var branchOverrideDateFormat = DbHelper.GetValue<bool>(Mnemonics.OverrideHoldDateFormat, notice.ReportingBranchID, notice.PatronLanguage);
            //    if (branchOverrideDateFormat)
            //    {
            //        messageBranch = notice.ReportingBranchID;
            //        tryParentOrg = false;
            //    }
            //}

            //try { msgString = Dialout.Strings.SingleOrDefault(s => s.OrganizationID == messageBranch && s.StringTypeID == stringType)?.Value; }
            //catch { }

            //if (string.IsNullOrWhiteSpace(msgString))
            //{
            //    if (tryParentOrg)
            //    {
            //        try { msgString = Dialout.Strings.SingleOrDefault(s => s.OrganizationID == messageLibrary && s.StringTypeID == stringType)?.Value; }
            //        catch { }
            //    }
            //    else
            //    {
            //        throw new ArgumentException($"No entry exists for message type {type} for {messageBranch}");
            //    }
            //}

            //if (string.IsNullOrWhiteSpace(msgString))
            //    msgString = Dialout.Strings.Single(s => s.OrganizationID == 1 && s.StringTypeID == stringType).Value;

            //if (msgString.StartsWith("http"))
            //{
            //    return new MessagePart { PartType = PartType.File, PartText = msgString };
            //}

            //return new MessagePart { PartType = PartType.String, PartText = string.IsNullOrWhiteSpace(msgString) ? "" : msgString.TrimEnd() + " " };
        }

        public static bool HasOverdues(this List<PolarisNotification> notices)
        {
            return notices.Any(n => n.IsOverdue);
        }

        public static bool HasHolds(this List<PolarisNotification> notices)
        {
            return notices.Any(n => n.IsAnyHold);
        }

        public static bool HasBills(this List<PolarisNotification> notices)
        {
            return notices.Any(n => n.IsBill);
        }

        public static bool HasHoldCancellations(this List<PolarisNotification> notices)
        {
            return notices.Any(n => n.IsHoldCancellation);
        }

        public static bool Has2ndHolds(this List<PolarisNotification> notices)
        {
            return notices.Any(n => n.IsSecondHold);
        }
    }
}
