﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Notices.Extensions
{
    public static class DateTimeExtension
    {
        public static bool IsWeekend(this DateTime dt)
        {
            return new[] { DayOfWeek.Saturday, DayOfWeek.Sunday }.Contains(dt.DayOfWeek);
        }
    }
}