﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Notices.Models;
using clc_twilio_csharp;
using Notices.Core.Data;
using System.Data.SqlClient;
using System.Data.Entity.Core.EntityClient;
using Notices.Services;
using Notices.Core;
using Clc.Polaris.Api;
using Clc.Polaris.Api.Models;
using clc_twilio_csharp.Models;
using System.Net.Http;
using Notices.Core.Models;

namespace Notices
{
    public static partial class Methods
    {
        private static Random random = new Random();

        public static string GetConnectionString(string server, string database, string metadata)
        {
            // Specify the provider name, server and database.
            string providerName = "System.Data.SqlClient";

            // Initialize the connection string builder for the
            // underlying provider.
            SqlConnectionStringBuilder sqlBuilder = new SqlConnectionStringBuilder();

            // Set the properties for the data source.
            sqlBuilder.DataSource = server;
            sqlBuilder.InitialCatalog = database;
            sqlBuilder.IntegratedSecurity = true;
            sqlBuilder.Encrypt = false;
            sqlBuilder.PersistSecurityInfo = true;
            sqlBuilder.MultipleActiveResultSets = true;

            // Build the SqlConnection connection string.
            string providerString = sqlBuilder.ToString();

            // Initialize the EntityConnectionStringBuilder.
            EntityConnectionStringBuilder entityBuilder =
                new EntityConnectionStringBuilder();

            //Set the provider name.
            entityBuilder.Provider = providerName;

            // Set the provider-specific connection string.
            entityBuilder.ProviderConnectionString = providerString;

            // Set the Metadata location.
            entityBuilder.Metadata = string.Format("res://*/{0}.csdl|res://*/{0}.ssdl|res://*/{0}.msl", metadata);

            var connString = entityBuilder.ToString();
            return connString;
        }

        public static void HandlePrtgSensor(string url)
        {
            if (!AppSettings.EnablePrtgSensors || AppSettings.Dev) return;

            using (var client = new HttpClient())
            {
                var response = client.GetAsync(url).Result;
            }
        }

        public static List<GetEmailReminderOtherItems_Result> CreateReminderOtherItemList()
        {
            var output = new List<GetEmailReminderOtherItems_Result>();

            for (var i = 0; i < Core.Methods.GenerateRandomInt(1, 10); i++)
            {
                output.Add(new GetEmailReminderOtherItems_Result
                {
                    AssignedBranch = $"{GenerateRandomString(GenerateRandomInt(7, 12))} {GenerateRandomString(GenerateRandomInt(7, 12))} Branch",
                    DueDate = GenerateRandomDate(),
                    MaterialType = GenerateRandomString(14),
                    Title = GenerateWords()
                });
            }

            return output;
        }

        public static TwilioClient CreateTwilioClient()
        {
            return new TwilioClient(AppSettings.TwilioAccountSid, AppSettings.TwilioAuthToken);
        }

        public static string GenerateRandomString(int length = 25)
        {
            const string chars = "abcdefghijklmnopqrstuvwxyz";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public static int GenerateRandomInt(int minimum = 0, int maximum = 1000)
        {
            return random.Next(minimum, maximum + 1);
        }

        public static string GenerateWords(int? numWords = null, int minWordLength = 4, int maxWordLength = 12)
        {
            numWords = numWords ?? random.Next(4, 7);
            var output = "";

            for (int i = 0; i < numWords; i++)
            {
                output += $"{GenerateRandomString(random.Next(minWordLength, maxWordLength))} ";
            }

            return output.Trim();
        }

        public static DateTime GenerateRandomDate()
        {
            return DateTime.Now.AddDays(GenerateRandomInt() * -1);
        }

        public static IEmailSender GetEmailSender()
        {
            var confValue = AppSettings.EmailSender.ToLower();

            switch (confValue)
            {
                case "null":
                    return new NullEmailSender();
                case "list":
                    return new AddToListEmailSender();
                case "postmark":
                    if (AppSettings.EmailUseStaging) { return new PostmarkStagingEmailSender(); }
                    else { return new PostmarkEmailSender(); }
                case "file":
                    return new OutputToFileEmailSender();
                case "mandrill":
                    return new MandrillEmailSender();
                default:
                    return null;
            }
        }

        public static void RunTests()
        {
            //var notices = PolarisNotification.GenerateTest(new[] { NotificationType.FirstOverdue, NotificationType.SecondOverdue, NotificationType.ThirdOverdue, NotificationType.ExpirationReminder, NotificationType.Cancel, NotificationType.Hold }, DeliveryOption.EmailAddress, patronBranch: 3, reportingBranchIds: new[] { 3, 7 });
            var notices = PolarisNotification.GenerateTest(new[] { NotificationType.Hold }, DeliveryOption.EmailAddress, patronBranch: 3, reportingBranchIds: new[] { 3, 7, 9 });
            notices.AddRange(PolarisNotification.GenerateTest(new[] { NotificationType.ExpirationReminder }, DeliveryOption.EmailAddress, patronBranch: 87, reportingBranchIds: new[] { 3, 7, 9 }));

            //notices.AddRange(PolarisNotification.GenerateTest(4, NotificationType.Cancel, DeliveryOption.EmailAddress, patronBranch: 3, reportingBranchId: 3));
            //notices.AddRange(PolarisNotification.GenerateTest(2, NotificationType.FirstOverdue, DeliveryOption.EmailAddress, 3));
            //notices.AddRange(PolarisNotification.GenerateTest(2, NotificationType.Hold, DeliveryOption.EmailAddress, 3));
            //notices.AddRange(PolarisNotification.GenerateTest(2, new[] { NotificationType.FirstOverdue, NotificationType.SecondOverdue, NotificationType.ThirdOverdue, NotificationType.Cancel }, DeliveryOption.EmailAddress, 3));

            //var notices = PolarisNotification.GenerateTest(count: 3, notificationTypeId: 3, deliveryOptionId: 2, reportingBranchId: 3);
            //notices.AddRange(PolarisNotification.GenerateTest(count: 3, notificationTypeId: 3, deliveryOptionId: 2, reportingBranchId: 3));

            //var test = Email.GenerateNoticeEmail(notices);
            var test2 = Services.Email.Email.SendNotices(notices, new AddToListEmailSender());
            var foo = AddToListEmailSender.Messages.First();
            var bar = foo;
        }

        

        public static PapiClient CreatePapiClient()
        {
            return new PapiClient
            {
                AccessID = AppSettings.PapiAccessId,
                AccessKey = AppSettings.PapiAccessKey,
                Hostname = AppSettings.PapiHostname,
                StaffOverrideAccount = new PolarisUser
                {
                    Domain = AppSettings.PapiStaffDomain,
                    Username = AppSettings.PapiStaffUsername,
                    Password = AppSettings.PapiStaffPassword
                }
            };
        }

        

        public static void CancelCallsInProcess()
        {
            Console.WriteLine("");
            Console.WriteLine("Gathering in process calls from Twilio.");
            var callsInProcess = new List<Call>();
            var twilio = CreateTwilioClient();

            callsInProcess.AddRange(twilio.ListCalls(status: "queued").Data.Calls);

            callsInProcess.AddRange(twilio.ListCalls(status: "ringing").Data.Calls);

            callsInProcess.AddRange(twilio.ListCalls(status: "in-process").Data.Calls);

            Console.WriteLine("Cancelling {0} calls.", callsInProcess.Count);

            callsInProcess.ForEach(call =>
            {
                Console.WriteLine("[ Sid:{0} ] - [ Previous Status:{1} ]", call.Sid, call.Status);
                var result = twilio.HangupCall(call.Sid, "canceled");
            });
            Console.WriteLine("Process completed");
        }
    }
}
