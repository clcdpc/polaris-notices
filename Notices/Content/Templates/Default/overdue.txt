{reporting_branch_name} 
{reporting_branch_street_one} 
{reporting_branch_city}, {reporting_branch_state} {reporting_branch_zip}

{patron_first_name} {patron_last_name} 
{patron_street_one} 
{patron_city}, {patron_state} {patron_zip}


{notice_header}

{overdue_items}

{notice_footer}

-------------------------

{unsubscribe_link}
