﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace Notices.Core
{
    public static class WindowsHelper
    {
        public static void RestartService(string servicename, int timeoutSeconds = 60)
        {
            if (!ServiceController.GetServices().Any(s => string.Equals(s.ServiceName, servicename, StringComparison.OrdinalIgnoreCase))) return;

            var service = new ServiceController(servicename);
            var timeout = TimeSpan.FromSeconds(timeoutSeconds);

            if (service.Status == ServiceControllerStatus.StartPending) return;

            if (service.Status != ServiceControllerStatus.Stopped)
            {
                // Stop Service
                service.Stop();
                service.WaitForStatus(ServiceControllerStatus.Stopped, timeout);
            }
            //Restart service
            service.Start();
            service.WaitForStatus(ServiceControllerStatus.Running, timeout);
        }
    }
}
