﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notices.Core
{
    public static class QueueHelper
    {
        public static int GetQueueMessageCount(string queueName)
        {
            try
            {
                var factory = new ConnectionFactory()
                {
                    HostName = AppSettings.RabbitMqHostname,
                    VirtualHost = AppSettings.RabbitMqVirtualHost,
                    UserName = AppSettings.RabbitMqUsername,
                    Password = AppSettings.RabbitMqPassword
                };


                using (var connection = factory.CreateConnection())
                using (var channel = connection.CreateModel())
                {
                    return (int)channel.QueueDeclarePassive(queueName).MessageCount;
                }
            }
            catch (Exception ex)
            {
                return -1;
            }
        }

        public static int GetQueueConsumerCount(string queueName)
        {
            try
            {
                var factory = new ConnectionFactory()
                {
                    HostName = AppSettings.RabbitMqHostname,
                    VirtualHost = AppSettings.RabbitMqVirtualHost,
                    UserName = AppSettings.RabbitMqUsername,
                    Password = AppSettings.RabbitMqPassword
                };


                using (var connection = factory.CreateConnection())
                using (var channel = connection.CreateModel())
                {
                    return (int)channel.QueueDeclarePassive(queueName).ConsumerCount;
                }
            }
            catch
            {
                return -1;
            }
        }
    }
}
