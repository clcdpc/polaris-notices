﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notices.Core
{
    public abstract class GlobalSettings
    {
        public virtual int Test => GetValue<int>("foo");

        public virtual string AuthString => GetValue<string>("auth_string");
        public virtual string BaseUrl => GetValue<string>("base_url");
        public virtual bool DevMode => GetValue<bool>("dev_mode");
        public virtual bool DialinEnable => GetValue<bool>("dialin_enable");
        public virtual bool DialinEnableItivaTransfer => GetValue<bool>("dialin_enable_itiva_transfer");
        public virtual int DialinPinLength => GetValue<int>("dialin_pin_length");
        public virtual string DialinTestBarcode => GetValue<string>("dialin_test_barcode");
        public virtual string DialinTestPin => GetValue<string>("dialin_test_pin");
        public virtual int DialinTimeoutLength => GetValue<int>("dialin_timeout_length");
        public virtual string DialinTitleEmailTemplateName => GetValue<string>("dialin_title_email_template_name");
        public virtual int DialoutCallRetentionDays => GetValue<int>("dialout_call_retention_days");
        public virtual bool DialoutEnable => GetValue<bool>("dialout_enable");
        public virtual string DialoutFromPhone => GetValue<string>("dialout_from_phone");
        public virtual bool DialoutRecordCalls => GetValue<bool>("dialout_record_calls");
        public virtual int DialoutRecordingRetentionDays => GetValue<int>("dialout_recording_retention_days");
        public virtual int DialoutTestPatronId => GetValue<int>("dialout_test_patron_id");
        public virtual int DialoutWeekdayStartHour => GetValue<int>("dialout_weekday_start_hour");
        public virtual int DialoutWeekdayStopHour => GetValue<int>("dialout_weekday_stop_hour");
        public virtual int DialoutWeekendStartHour => GetValue<int>("dialout_weekend_start_hour");
        public virtual int DialoutWeekendStopHour => GetValue<int>("dialout_weekend_stop_hour");
        public virtual bool EmailEnableSpamRejectionRemoval => GetValue<bool>("email_enable_spam_rejection_removal");
        public virtual bool EmailFailureProcessingEnable => GetValue<bool>("email_failure_processing_enable");
        public virtual bool EmailLibrarySpecificSpam => GetValue<bool>("email_library_specific_spam");
        public virtual bool EnableHangfireMsmq => GetValue<bool>("enable_hangfire_msmq");
        public virtual bool EnableNotifier => GetValue<bool>("enable_notifier");
        public virtual bool EnablePrtgSensors => GetValue<bool>("enable_prtg_sensors");
        public virtual string MandrillApiKey => GetValue<string>("mandrill_api_key");
        public virtual string MandrillUnsignedRejectionSensorUrl => GetValue<string>("mandrill_unsigned_rejections_sensor_url");
        public virtual string MessageServiceSid => GetValue<string>("message_service_sid");
        public virtual string PapiKey => GetValue<string>("papi_key");
        public virtual string PapiRequestHostname => GetValue<string>("papi_request_hostname");
        public virtual string PapiUser => GetValue<string>("papi_user");
        public virtual string RabbitMqHostname => GetValue<string>("rabbitmq_hostname");
        public virtual string RabbitMqPassword => GetValue<string>("rabbitmq_password");
        public virtual string RabbitMqUsername => GetValue<string>("rabbitmq_username");
        public virtual string RabbitMqVirtualhost => GetValue<string>("rabbitmq_virtualhost");
        public virtual string RebuildHangfireJobs => GetValue<string>("rebuild_hangfire_jobs");
        public virtual int SmsAfterHoursSendHour => GetValue<int>("sms_after_hours_send_hour");
        public virtual int SmsAfterHoursSendMinute => GetValue<int>("sms_after_hours_send_minute");
        public virtual bool SmsEnable => GetValue<bool>("sms_enable");
        public virtual int SmsMessageRetentionDays => GetValue<int>("sms_message_retention_days");
        public virtual string SmsReplyText => GetValue<string>("sms_reply_text");
        public virtual int SmsStartHour => GetValue<int>("sms_start_hour");
        public virtual string SmsStartReplyText => GetValue<string>("sms_start_reply_text");
        public virtual int SmsStopHour => GetValue<int>("sms_stop_hour");
        public virtual string SmsStopNoticeIdentifier => GetValue<string>("sms_stop_notice_identifier");
        public virtual string SmsStopReplyText => GetValue<string>("sms_stop_reply_text");
        public virtual string SmsStopZeroAccountsReplyText => GetValue<string>("sms_stop_zero_accounts_reply_text");
        public virtual string SmsTestNumber => GetValue<string>("sms_test_number");
        public virtual string StaffDomain => GetValue<string>("staff_domain");
        public virtual string StaffPassword => GetValue<string>("staff_password");
        public virtual string StaffUsername => GetValue<string>("staff_username");
        public virtual string TwilioAccountSid => GetValue<string>("twilio_account_sid");
        public virtual string TwilioAuthToken => GetValue<string>("twilio_auth_token");
        public virtual string TwilioCarrierViolationErrorSensorUrl => GetValue<string>("twilio_carrier_violation_errors_sensor_url");
        public virtual string TwilioErrorSensorUrl => GetValue<string>("twilio_errors_sensor_url");


        protected abstract T GetValue<T>(string key);
    }
}
