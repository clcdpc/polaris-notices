﻿using Clc.Polaris.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace Notices.Core.Models
{
    public class NoticeUpdate : QueueProcessInput
    {
        public int NotificationTypeId { get; set; }
        public int ReportingOrgId { get; set; }
        public int DeliveryOptionId { get; set; }
        public string DeliveryString { get; set; }
        public int PatronId { get; set; }
        public int ItemRecordId { get; set; }
        public NotificationStatus NotificationStatus { get; set; }
        public DateTime DeliveryDate { get; set; }
        public string Details { get; set; }
        public string Error { get; set; }

        public override string ToString()
        {
            return new JavaScriptSerializer().Serialize(this);
        }

        public string ToLogString()
        {
            return ToString();
        }

        public static NoticeUpdate FromJson(string json)
        {
            return new JavaScriptSerializer().Deserialize<NoticeUpdate>(json);
        }

        public static bool TryFromJson(string json, out NoticeUpdate update)
        {
            try
            {
                update = new JavaScriptSerializer().Deserialize<NoticeUpdate>(json);
                return true;
            }
            catch
            {
                update = null;
                return false;
            }
        }
    }
}