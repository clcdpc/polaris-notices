﻿using Clc.Postmark;
using clc_telnyx_csharp;
using clc_telnyx_csharp.Models;
using clc_twilio_csharp;
using Dapper;
using Newtonsoft.Json;
using NLog;
using Notices.Core.Data;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Notices.Core.Models
{
    public interface ISmsSender
    {
        bool UpdateNotices { get; }
        QueueProcessResult Send(SmsMessage input);
        bool CheckDuplicate(string to, string body, int orgId);
        SmsMessage ProcessInbound(HttpRequestBase request);
        Func<SmsMessage, string> ProcessReply { get; }
    }

    public class AddToFileSmsSender : ISmsSender
    {
        public Func<SmsMessage, string> ProcessReply => throw new NotImplementedException();

        public bool UpdateNotices => false;

        public bool CheckDuplicate(string to, string body, int orgId)
        {
            return false;
        }

        public SmsMessage ProcessInbound(HttpRequestBase request)
        {
            throw new NotImplementedException();
        }

        public QueueProcessResult Send(SmsMessage input)
        {
            LogManager.GetLogger($"sms-file-sender.{input.To}").Info($"{DateTime.Now} - {input.Body}");
            return new QueueProcessResult(true, "added to file");
        }
    }

    public class TelnyxSmsSender : ISmsSender
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public Func<SmsMessage, string> ProcessReply
        {
            get =>
                messageToSend =>
            {
                Send(messageToSend);
                return "";
            };
        }

        public bool UpdateNotices => true;

        public bool CheckDuplicate(string to, string body, int orgId)
        {
            using (var db = DbHelper.CreateConnection())
            {
                var sql = $@"
                            select count(*)
                            from CLC_Notices.dbo.TelnyxSmsMessages m
                            where m.SentFrom like @to
	                            and m.Body = @body
	                            and cast(m.Timestamp as date) = cast(getdate() as date)
                            ";

                var sentTodayCount = db.QuerySingle<int>(sql, new { to = $"%{to}", body });

                if (sentTodayCount > 0)
                {
                    return true;
                }
            }

            return false;
        }

        public SmsMessage ProcessInbound(HttpRequestBase request)
        {
            request.InputStream.Position = 0;
            var bodyText = new StreamReader(request.InputStream).ReadToEnd();

            var inboundMessage = JsonConvert.DeserializeObject<TelnyxInboundSmsMessage>(bodyText);
            var payload = inboundMessage.data.payload;

            return new SmsMessage { From = payload.from.phone_number, Body = payload.text, To = payload.to.FirstOrDefault()?.phone_number };
        }

        public QueueProcessResult Send(SmsMessage input)
        {
            var telnyx = new TelnyxClient(AppSettings.TelnyxV2ApiKey)
            {
                SmsWebhookUrl = $"{AppSettings.BaseURL}/Sms/TelnyxSmsWebhook?auth={AppSettings.AuthString}"
            };

            var errorMessage = "";
            var result = telnyx.SendSmsWithProfile(AppSettings.MessagingProfileId, $"+{input.To}", input.Body);

            if (result?.Data == null)
            {
                var errorMsg = $"Telnyx SendSmsWithProfile result null\r\n{result.Response.Content}";
                logger.Error(errorMsg);
                return new QueueProcessResult(false, errorMsg);
            }
            else
            {
                var data = result.Data.data;

                if (data.errors.Any()) { errorMessage += $"{data.id} - {data.to} - {string.Join(", ", data.errors)}"; }
                if (result.Exception != null) { errorMessage = $"\r\n{result.Exception.Message} \r\n {result.Exception.StackTrace}"; }

                var error = !string.IsNullOrWhiteSpace(errorMessage);

                using (var conn = new SqlConnection(DbHelper.ConnectionString))
                {
                    var sql = @"insert into TelnyxSmsMessages(Id,Timestamp,SentTo,SentFrom,Body,Parts,Status) values (@id, @timestamp, @phone_number, @from, @text, @parts, 'sent')";
                    conn.Execute(sql, new { data.id, timestamp = DateTime.Now, data.to.FirstOrDefault().phone_number, data.from, data.text, data.parts });
                }

                if (!error)
                {
                    logger.Debug("Sent: {0} - {1} - {2}", input.To, input.Body, result?.Data.data.id);
                    return new QueueProcessResult(true);
                }

                logger.Error($"Failed: {input.To} - {input.Body} | {errorMessage}");
                return new QueueProcessResult(false, errorMessage);
            }
        }
    }

    public class OplinSmsSender : ISmsSender
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public Func<SmsMessage, string> ProcessReply
        {
            get =>
                messageToSend =>
                {
                    Send(messageToSend);
                    return "";
                };
        }

        public bool UpdateNotices => true;

        public bool CheckDuplicate(string to, string body, int orgId)
        {
            using (var db = DbHelper.CreateConnection())
            {
                var sql = $@"
                            select count(*)
                            from CLC_Notices.dbo.TelnyxSmsMessages m
                            where m.SentFrom like @to
	                            and m.Body = @body
	                            and cast(m.Timestamp as date) = cast(getdate() as date)
                            ";

                var sentTodayCount = db.QuerySingle<int>(sql, new { to = $"%{to}", body });

                if (sentTodayCount > 0)
                {
                    return true;
                }
            }

            return false;
        }

        public SmsMessage ProcessInbound(HttpRequestBase request)
        {
            throw new NotImplementedException();
        }

        public QueueProcessResult Send(SmsMessage input)
        {
            var postmark = new PostmarkClient2(AppSettings.PostmarkOplinSmsKey);

            var errorMessage = "";
            input.Body = $"***{input.Body}***";
            var result = postmark.Send($"{input.To}@sms.oplin.org", AppSettings.PostmarkOplinSmsFromAddress, "", textBody: input.Body);

            if (result?.Data == null)
            {
                var errorMsg = $"Oplin send sms result null\r\n{result.Response.Content}";
                logger.Error(errorMsg);
                return new QueueProcessResult(false, errorMsg);
            }
            else
            {
                var data = result.Data;

                if (data.ErrorCode != 0) { errorMessage += $"{data.MessageID} - {data.To} - {data.ErrorCode}"; }
                if (result.Exception != null) { errorMessage = $"\r\n{result.Exception.Message} \r\n {result.Exception.StackTrace}"; }

                var error = !string.IsNullOrWhiteSpace(errorMessage);

                if (!error)
                {
                    logger.Debug("Sent: {0} - {1} - {2}", input.To, input.Body, result?.Data.MessageID);
                    return new QueueProcessResult(true);
                }

                logger.Error($"Failed: {input.To} - {input.Body} | {errorMessage}");
                return new QueueProcessResult(false, errorMessage);
            }
        }
    }

    public class TwilioSmsSender : ISmsSender
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public Func<SmsMessage, string> ProcessReply
        {
            get =>
                messageToSend =>
                {
                    Send(messageToSend);
                    return "";
                };
        }

        public bool UpdateNotices => true;

        public bool CheckDuplicate(string to, string body, int orgId)
        {
            return false;
        }

        public SmsMessage ProcessInbound(HttpRequestBase request)
        {
            request.InputStream.Position = 0;
            var bodyText = new StreamReader(request.InputStream).ReadToEnd();

            var inboundMessage = JsonConvert.DeserializeObject<SmsMessage>(bodyText);
            return inboundMessage;
        }

        public QueueProcessResult Send(SmsMessage input)
        {
            var twilio = new TwilioClient(AppSettings.TwilioAccountSid, AppSettings.TwilioAuthToken);
            var result = twilio.SendMessageWithService(input.To, input.Body, AppSettings.MessageServiceSid).Data;
            logger.Trace($"{input.To} - {input.Body} - {result.Status}");
            if (result?.ErrorCode != null)
            {
                logger.Trace($"{input.To} - {input.Body} - {result.Status} - {result.ErrorCode} - {result.ErrorMessage}");
                return new QueueProcessResult(false, $"{result.ErrorCode} - {result.ErrorMessage}");
            }

            return new QueueProcessResult(true);
        }
    }
}
