﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notices.Core.Models
{
    public enum DeliveryOption : int
    {
        MailingAddress = 1,
        EmailAddress,
        Phone1,
        Phone2,
        Phone3,
        FAX,
        EDI,
        TXTMessaging,
    }
}
