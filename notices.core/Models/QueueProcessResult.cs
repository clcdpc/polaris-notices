﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notices.Core.Models
{
    public class QueueProcessResult
    {
        public bool Success { get; set; }
        public string Message { get; set; } = string.Empty;

        public QueueProcessResult()
        {

        }

        public QueueProcessResult(bool success, string message = "")
        {
            Success = success;
            Message = message;
        }
    }
}
