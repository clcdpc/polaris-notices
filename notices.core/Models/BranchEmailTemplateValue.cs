﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Notices.Core.Models
{
    public class BranchEmailTemplateValue
    {
        public int OrganizationId { get; set; }
        public int NotificationTypeId { get; set; }
        public int LanguageId { get; set; }
        public string PlaintextTemplateUrl { get; set; }
        public string HtmlTempalateUrl { get; set; }
    }
}