﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notices.Core.Models
{
    public class SmsMessage : QueueProcessInput
    {        
        public string From { get; set; }
        public string To { get; set; }
        public string Body { get; set; }
        public int OrganizationId { get; set; }
        public string Error { get; set; } = null;
        public DateTime? QueuedAt { get; set; } = null;


        public SmsMessage()
        {

        }

        public SmsMessage(string to, string body, int orgId = 0)
        {
            To = to;
            Body = body;
            OrganizationId = orgId;
        }

        public static SmsMessage FromJson(string json)
        {
            return JsonConvert.DeserializeObject<SmsMessage>(json);
        }

        public static bool TryFromJson(string json, out SmsMessage message)
        {
            try
            {
                message = JsonConvert.DeserializeObject<SmsMessage>(json);
                return true;
            }
            catch
            {
                message = null;
                return false;
            }
        }

        public override string ToString()
        {
            return $"{To} - {Body}";
        }
    }
}
