﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notices.Core.Models
{
    public class PolarisSmsNotificationQueueEntry
    {
        public int PatronId { get; set; }
        public int NotificationTypeId { get; set; }
        public int ItemRecordId { get; set; }
        public string SmsDeliveryPhone { get; set; }
        public string BrowseTitle { get; set; }
        public int ReportingOrgId { get; set; }
        public string ReportingOrgName { get; set; }
        public DateTime? HoldTillDate { get; set; }
        public string MessageTemplate { get; set; }
        public string FormattedMessage { get; set; }

        public override string ToString() => $"{SmsDeliveryPhone} - {FormattedMessage} ||| ({FormattedMessage.Length})";
    }
}
