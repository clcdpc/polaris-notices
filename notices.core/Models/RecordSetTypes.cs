﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Notices.Core.Models
{
    public enum RecordSetTypes
    {
        Bounce = 1,
        Spam,
        SmsStop
    }
}