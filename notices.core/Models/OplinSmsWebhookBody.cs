﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notices.Core.Models
{
    public class OplinSmsWebhookBody
    {
        public string Type { get; set; }
        [JsonProperty("message_id")]
        public string MessageId { get; set; }
        [JsonProperty("patron_phone")]
        public string PatronPhone { get; set; }
        public string Owner { get; set; }
        public string Status { get; set; }

        public string LogLevel => IsSuccess ? "info" : "error";
        public bool IsSuccess => new[] { "attempted", "sent" }.Contains(Status, StringComparer.OrdinalIgnoreCase);

        public override string ToString()
        {
            return $"{MessageId} - {PatronPhone} - {Owner} - {Status}";
        }
    }
}