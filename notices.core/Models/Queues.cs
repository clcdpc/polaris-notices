﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notices.Core.Models
{
    public class Queues
    {
        public const string SMS_MESSAGES = "sms-messages";
        public const string SMS_NOTICE_UPDATES = "sms-notice-updates";
        public const string AFTER_HOURS_MESSAGES = "after-hours";
        public const string FAILED_MESSAGES = "failed-messages";
        public const string FAILED_SMS_UPDATES = "failed-sms-updates";
        public const string DUPLICATE_SMS_MESSAGES = "duplicate-sms-messages";

        public const string DIALOUT_NOTICE_UPDATES = "dialout-notice-updates";
        public const string FAILED_DIALOUT_UPDATES = "failed-dialout-updates";

        public const string EMAIL_NOTICE_UPDATES = "email-notice-updates";
        public const string FAILED_EMAIL_UPDATES = "failed-email-updates";
    }
}
