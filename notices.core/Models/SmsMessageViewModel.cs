﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Notices.Core.Models
{
    public class SmsMessageViewModel
    {
        public DateTime DateSent { get; set; }
        public string To { get; set; }
        public string Body { get; set; }
        public string Error { get; set; }

        public SmsMessageViewModel()
        {

        }

        public SmsMessageViewModel(clc_twilio_csharp.Models.Message message)
        {
            DateSent = message.DateSent ?? DateTime.MinValue;
            To = message.To;
            Body = message.Body;
            Error = $"{message.ErrorCode} - {message.ErrorMessage}";
        }
    }
}