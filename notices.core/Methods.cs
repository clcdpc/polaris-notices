﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using clc_twilio_csharp;
using Notices.Core.Data;
using System.Data.SqlClient;
using System.Data.Entity.Core.EntityClient;
using Notices.Core;
using Clc.Polaris.Api;
using Clc.Polaris.Api.Models;
using System.Net.Http;
using Notices.Core.Models;

namespace Notices.Core
{
    public static partial class Methods
    {
        private static Random random = new Random();

        public static string GetConnectionString(string server, string database, string metadata)
        {
            // Specify the provider name, server and database.
            string providerName = "System.Data.SqlClient";

            // Initialize the connection string builder for the
            // underlying provider.
            SqlConnectionStringBuilder sqlBuilder = new SqlConnectionStringBuilder();

            // Set the properties for the data source.
            sqlBuilder.DataSource = server;
            sqlBuilder.InitialCatalog = database;
            sqlBuilder.IntegratedSecurity = true;
            sqlBuilder.Encrypt = false;
            sqlBuilder.PersistSecurityInfo = true;
            sqlBuilder.MultipleActiveResultSets = true;

            // Build the SqlConnection connection string.
            string providerString = sqlBuilder.ToString();

            // Initialize the EntityConnectionStringBuilder.
            EntityConnectionStringBuilder entityBuilder =
                new EntityConnectionStringBuilder();

            //Set the provider name.
            entityBuilder.Provider = providerName;

            // Set the provider-specific connection string.
            entityBuilder.ProviderConnectionString = providerString;

            // Set the Metadata location.
            entityBuilder.Metadata = string.Format("res://*/{0}.csdl|res://*/{0}.ssdl|res://*/{0}.msl", metadata);

            var connString = entityBuilder.ToString();
            return connString;
        }

        public static PolarisOrganization GetOrg(int orgId)
        {
            return Variables.Organizations.SingleOrDefault(o => o.OrganizationID == orgId);
        }

        public static PolarisOrganization GetParentOrg(int orgId)
        {
            var parentId = orgId == 1 ? 1 : Variables.Organizations.SingleOrDefault(o => o.OrganizationID == orgId)?.ParentOrganizationID ?? 1;
            return Variables.Organizations.Single(o => o.OrganizationID == parentId);
        }

        public static PapiClient CreatePapiClient()
        {
            return new PapiClient
            {
                AccessID = AppSettings.PapiAccessId,
                AccessKey = AppSettings.PapiAccessKey,
                Hostname = AppSettings.PapiHostname,
                StaffOverrideAccount = new PolarisUser
                {
                    Domain = AppSettings.PapiStaffDomain,
                    Username = AppSettings.PapiStaffUsername,
                    Password = AppSettings.PapiStaffPassword
                }
            };
        }

        /*
        public static string GetSmsSender(int libraryId)
        {
            var type = Type.GetType(AppSettings.SmsSender);
            if (type == null) { throw new ArgumentException("Invalid Sms Sender"); }

            if (AppSettings.TelnyxLibraries.Contains(libraryId)) { return "telnyx"; }
            return AppSettings.SmsSender;
        }
        */

        public static ISmsSender GetSmsSender(int libraryId)
        {
            if (AppSettings.Debug) { return new AddToFileSmsSender(); }
            var smsSenderTypeName = DbSettings.SmsSender(libraryId);
            var type = Type.GetType(smsSenderTypeName);
            if (type == null) { throw new ArgumentException("Invalid Sms Sender"); }

            return (ISmsSender)Activator.CreateInstance(type);
        }

        public static string GenerateRandomString(int length = 25)
        {
            const string chars = "abcdefghijklmnopqrstuvwxyz";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public static int GenerateRandomInt(int minimum = 0, int maximum = 1000)
        {
            return random.Next(minimum, maximum + 1);
        }

        public static string GenerateWords(int? numWords = null, int minWordLength = 4, int maxWordLength = 12)
        {
            numWords = numWords ?? random.Next(4, 7);
            var output = "";

            for (int i = 0; i < numWords; i++)
            {
                output += $"{GenerateRandomString(random.Next(minWordLength, maxWordLength))} ";
            }

            return output.Trim();
        }

        public static DateTime GenerateRandomDate()
        {
            return DateTime.Now.AddDays(GenerateRandomInt() * -1);
        }
    }
}
