﻿using NLog;
using Notices.Core;
using Notices.Core.Models;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Client.MessagePatterns;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace Notices.Core
{
    public abstract class QueueListener<T> where T : QueueProcessInput
    {
        public string Queue { get; set; }
        public string FailedQueue { get; set; }
        public string InvalidQueue { get; set; }

        public bool ExitWhenEmpty { get; set; } = false;

        protected bool _listen = true;
        protected Logger logger;// = LogManager.GetCurrentClassLogger();
        protected ConnectionFactory factory;

        public QueueListener(string hostname, string virtualHost, string queue, string username, string password, string failedQueue, string invalidQueue, bool exitWhenEmpty = false, Logger _logger = null)
        {
            factory = new ConnectionFactory { HostName = hostname, VirtualHost = virtualHost, UserName = username, Password = password };
            Queue = queue;
            FailedQueue = failedQueue;
            InvalidQueue = invalidQueue;
            ExitWhenEmpty = exitWhenEmpty;
            logger = _logger ?? LogManager.GetCurrentClassLogger();
        }

        public void Start()
        {
            Task.Run(() => Listen());
        }

        protected void Listen()
        {
            try
            {
                Console.WindowWidth = 140;
            }
            catch { }

            logger.Info("Connecting to {0}/{1}...", factory.HostName, Queue);

            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: Queue,
                                     durable: true,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);

                var sub = new Subscription(channel, Queue, false);
                logger.Info("Connected to {0}/{1}.", factory.HostName, Queue);

                // local function to allow using return to break inner loop
                void work()
                {
                    while (_listen)
                    {
                        BasicDeliverEventArgs msg;
                        while (!sub.Next(1000, out msg))
                        {
                            if (!_listen || ExitWhenEmpty) return; // use return to break out of inner loop and local function
                        }

                        var body = Encoding.UTF8.GetString(msg.Body);
                        logger.Trace("Message Received: {0}", body);

                        try
                        {
                            var js = new JavaScriptSerializer();
                            var obj = js.Deserialize<T>(body);
                            var result = Process(obj);

                            if (!result.Success)
                            {
                                obj.ErrorMsg = result.Message;
                                MoveMessage(obj.ToJson(), FailedQueue);
                            }
                        }
                        catch (ArgumentException aex)
                        {
                            logger.Error(aex);
                            MoveMessage(body, InvalidQueue);
                        }
                        catch (Exception ex)
                        {
                            logger.Error(ex);
                            MoveMessage(body, FailedQueue);
                        }
                        finally
                        {
                            sub.Ack(msg);
                        }
                    }
                }

                // execute local function containing the loops
                work();

                logger.Info("Exiting...");
                sub.Close();
                Thread.Sleep(2000);
            }
        }

        public void Stop()
        {
            logger.Info("Stopping...");
            _listen = false;
        }

        public abstract QueueProcessResult Process(T input);

        protected void MoveMessage(string json, string queue)
        {
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(
                    queue: queue,
                    durable: true,
                    exclusive: false,
                    autoDelete: false,
                    arguments: null);

                var body = Encoding.UTF8.GetBytes(json);

                IBasicProperties props = channel.CreateBasicProperties();
                props.ContentType = "text/plain";
                props.DeliveryMode = 2;

                channel.BasicPublish("", queue, props, body);
            }
        }
    }
}
