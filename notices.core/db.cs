﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Linq.Expressions;
using System.Reflection;
using Notices.Core.Models;

namespace Notices.Core.Data
{
    public class DbHelper
    {
        public string Server { get; set; }

        static List<EmailTemplate> EmailTemplateCache { get; set; }

        public static string ConnectionString => $"Server={AppSettings.DbServer};Database=CLC_Notices;Trusted_Connection=True;Encrypt=False";

        public DbHelper() : this(AppSettings.DbServer)
        {

        }

        public DbHelper(string server)
        {
            Server = server;

            //if (EmailTemplateCache == null) EmailTemplateCache = EmailTemplates().ToList();
        }

        IEnumerable<T> Get<T>(string sql, Pred[] predicates = null)
        {
            var builder = new SqlBuilder();
            var template = builder.AddTemplate(sql);

            if (predicates != null)
            {
                foreach (var p in predicates)
                {
                    var par = new DynamicParameters();
                    par.Add(p.PropertyName, p.Value);
                    builder.Where($"{p.PropertyName} {p.Operator} @{p.PropertyName}", par);
                }
            }

            using (var conn = new SqlConnection($"Server={Server};Database=CLC_Notices;Trusted_Connection=True;"))
            {
                var results = conn.Query<T>(template.RawSql, template.Parameters, commandTimeout: 60);
                return results;
            }
        }

        public static SqlConnection CreateConnection()
        {
            return new SqlConnection(ConnectionString);
        }

        int Execute(string sql, object parameters)
        {
            using (var conn = new SqlConnection($"Server={Server};Database=CLC_Notices;Trusted_Connection=True;"))
            {
                var results = conn.Execute(sql, parameters);
                return results;
            }
        }

        public IEnumerable<TodaysHoldCall> TodaysHoldCalls()
        {
            var sql = "select * from CLC_Notices.dbo.TodaysHoldCalls";
            return Get<TodaysHoldCall>(sql, null);
        }

        public IEnumerable<Dialout_Strings> DialoutStrings()
        {
            var sql = "select * from CLC_Notices.dbo.Dialout_Strings";
            return Get<Dialout_Strings>(sql, null);
        }

        public int AddEmailStatus(bool status)
        {
            var sql = "insert into CLC_Notices.dbo.EmailStatus (StatusDate, Status) values (@statusDate, @status)";
            return Execute(sql, new { StatusDate = DateTime.Now.ToShortDateString(), status = status ? 1 : 0 });
        }

        public IEnumerable<Dialin_Strings> DialinStrings()
        {
            var sql = "select * from CLC_Notices.dbo.Dialin_Strings";
            return Get<Dialin_Strings>(sql, null);
        }

        IEnumerable<T> Query<T>(string sql, object parameters = null)
        {
            using (var conn = new SqlConnection($"Server={Server};Database=CLC_Notices;Trusted_Connection=True;"))
            {
                var results = conn.Query<T>(sql, parameters);
                return results;
            }
        }

        public IEnumerable<PolarisLanguageString> PolarisSmsNoticeLanguageStrings()
        {
            var sql = @"select * from PolarisSmsNoticeLanguageStrings";
            return Query<PolarisLanguageString>(sql, null);
        }

        public int UpdateTelnySmsHistory(Guid id, string status, int? errorCode = null, string errorMessage = null)
        {
            var sql = @"
            update TelnyxSmsHistory
            set Status = @status
	            ,ErrorCode = @errorCode
	            ,ErrorMessage = @errorMessage
            where Id = @id
            ";

            return Execute(sql, new { id, status, errorCode, errorMessage });
        }

        public string SA_GetMultiLingualStringValue(int orgId, int productId, string mnemonic, int languageId = 1033)
        {
            var sql = $"select Polaris.Polaris.SA_GetMultiLingualStringValue(@productId, @languageId, @orgId, @mnemonic)";
            return Query<string>(sql, new { productId, languageId, orgId, mnemonic }).FirstOrDefault();
        }

        public IEnumerable<GetPatronsForSmsNumber_Result> GetPatronsForSmsNumber(string pn)
        {
            var sql = "exec CLC_Notices.dbo.GetPatronsForSmsNumber @pn";
            var results = Query<GetPatronsForSmsNumber_Result>(sql, new { pn });
            return results;
        }
        public bool HoldNoticesHaveBeenPopuldated()
        {
            var sql = "exec CLC_Notices.dbo.CheckIfHoldNoticesHaveBeenPopulated";
            var results = Query<bool>(sql);
            return results.Single();
        }

        public int TwilioMailRollover(string libraryString)
        {
            var sql = "exec CLC_Notices.dbo.TwilioMailRollover @libraryString";
            var results = Query<int>(sql, new { libraryString }).SingleOrDefault();
            return results;
        }

        public IEnumerable<SmsMessageViewModel> GetTelnyxSmsMessages(string pn = "")
        {
            var sql = @"
                select top 50 
		             m.Timestamp [DateSent]
		            ,m.sentTo [To]
		            ,m.Body
                from CLC_Notices.dbo.TelnyxSmsMessages m
                /**where**/
                order by m.Timestamp desc
            ";

            var result = Get<SmsMessageViewModel>(sql, string.IsNullOrWhiteSpace(pn) ? null : new[] { new Pred { PropertyName = "SentTo", Operator = "like", Value = $"%{pn}" } });
            return result;
        }

        public IEnumerable<GetEmailReminderOtherItems_Result> GetEmailReminderOtherItems(int patronId)
        {
            var sql = "exec CLC_Notices.dbo.GetEmailReminderOtherItems @patronId";
            var results = Query<GetEmailReminderOtherItems_Result>(sql, new { patronId });
            return results;
        }

        public int RemovePatronSmsSettings(int patronId)
        {
            var sql = "exec CLC_Notices.dbo.RemovePatronSmsSettings @patronId";
            var results = Query<int>(sql, new { patronId }).SingleOrDefault();
            return results;
        }

        public EmailNoticeHeader GetEmailNoticeHeader(int[] orgIds, NotificationType noticeType, int languageId, EmailFormat format)
        {
            var sql = @"
            select top 1 *
            from CLC_Notices.dbo.EmailNoticeHeaders enh
            where enh.OrganizationId in @orgIds
	            and enh.NotificationTypeId = @noticeTypeId
	            and enh.LanguageId = @languageId
	            and enh.EmailFormatId = @formatId
            order by enh.OrganizationId desc";

            return Query<EmailNoticeHeader>(sql, new { orgIds, noticeTypeId = (int)noticeType, languageId, formatId = (int)format }).Single();
        }

        public EmailNoticeFooter GetEmailNoticeFooter(int[] orgIds, NotificationType noticeType, int languageId, EmailFormat format)
        {
            var sql = @"
            select top 1 *
            from CLC_Notices.dbo.EmailNoticeFooters enf
            where enf.OrganizationId in @orgIds
	            and enf.NotificationTypeId = @noticeTypeId
	            and enf.LanguageId = @languageId
	            and enf.EmailFormatId = @formatId
            order by enf.OrganizationId desc";

            return Query<EmailNoticeFooter>(sql, new { orgIds, noticeTypeId = (int)noticeType, languageId, formatId = (int)format }).Single();
        }

        public int AddEmailHistorySearchLog(string username, string email)
        {
            var sql = "insert into clc_notices.dbo.EmailHistorySearchLog (Date, Username, Query) values (getdate(), @username, @email)";
            return Execute(sql, new { username, email });
        }

        public IEnumerable<GetPatronsForEmail_Result> GetPatronsForEmail(string emailAddress)
        {
            var sql = "exec CLC_Notices.dbo.GetPatronsForEmail @emailAddress";
            var results = Query<GetPatronsForEmail_Result>(sql, new { emailAddress });
            return results;
        }

        public int AddPatronNote(int patronId, string note)
        {
            var sql = "exec CLC_Notices.dbo.AddPatronNote @patronId, @note";
            var results = Query<int>(sql, new { patronId, note }).SingleOrDefault();
            return results;
        }

        public int RemovePatronPhoneNumber(string to)
        {
            var sql = "exec CLC_Notices.dbo.RemovePatronPhoneNumber @to";
            var results = Query<int>(sql, new { to }).SingleOrDefault();
            return results;
        }

        public IEnumerable<GetCheckedOutItems_Result> GetCheckedOutItems(int patronId, int overdueOnly = 0)
        {
            var sql = "exec CLC_Notices.dbo.GetCheckedOutItems @patronId, @overdueOnly";
            var results = Query<GetCheckedOutItems_Result>(sql, new { patronId, overdueOnly });
            return results;
        }

        public IEnumerable<Dialin_Patrons> DialinPatrons(Pred[] predicates)
        {
            var sql = "select * from clc_notices.dbo.Dialin_Patrons /**where**/";
            return Get<Dialin_Patrons>(sql, predicates);
        }

        public Dialin_Patrons GetDialinPatron(string pn)
        {
            var sql = "select * from clc_notices.dbo.Dialin_Patrons where PhoneNumber = @pn";
            return Query<Dialin_Patrons>(sql, new { pn }).SingleOrDefault();
        }

        public IEnumerable<Dialin_Patrons> DialinPatrons(Pred predicate = null)
        {
            return DialinPatrons(new[] { predicate });
        }

        public int LogDialinUsage(int patronId, int branchId)
        {
            var sql = "insert into clc_notices.dbo.dialin_usage (patronid, branchid, timestamp) values (@patronId, @branchId, getdate())";
            return Query<int>(sql, new { patronId, branchId }).SingleOrDefault();
        }

        public int RemoveDialinPatron(string pn)
        {
            var sql = "delete from clc_notices.dbo.Dialin_Patrons where phonenumber = @pn";
            return Query<int>(sql, new { pn }).SingleOrDefault();
        }

        public int AddDialinPatron(string barcode, string pn)
        {
            var sql = "insert into clc_notices.dbo.dialin_patrons values(@barcode, @pn)";
            return Execute(sql, new { barcode, pn });
        }

        public IEnumerable<Dialin_Ignore_List> DialinIgnoreList(Pred[] predicates)
        {
            var sql = "select * from clc_notices.dbo.Dialin_Ignore_List /**where**/";
            return Get<Dialin_Ignore_List>(sql, predicates);
        }

        public IEnumerable<Dialin_Ignore_List> DialinIgnoreList(Pred predicate = null)
        {
            return DialinIgnoreList(new[] { predicate });
        }

        public RecordSet GetRecordSet(int orgId, RecordSetTypes rsType)
        {
            var sql = "select * from clc_notices.dbo.RecordSets where OrganizationID = @orgId and RecordSetTypeID = @rsType";
            return Query<RecordSet>(sql, new { orgId, rsType = (int)rsType }).SingleOrDefault();
        }

        public IEnumerable<RecordSet> RecordSets(Pred[] predicates)
        {
            var sql = "select * from clc_notices.dbo.RecordSets /**where**/";
            return Get<RecordSet>(sql, predicates);
        }

        public IEnumerable<SettingValue> SettingValues(Pred[] predicates = null)
        {
            var sql = "select * from clc_notices.dbo.SettingValues";
            return Get<SettingValue>(sql, predicates);
        }

        public IEnumerable<SettingType> SettingTypes(Pred[] predicates = null)
        {
            var sql = "select * from clc_notices.dbo.SettingTypes";
            return Get<SettingType>(sql, predicates);
        }

        IEnumerable<EmailTemplate> EmailTemplates(Pred[] predicates = null)
        {
            var sql = "select * from clc_notices.dbo.EmailTemplates";
            return Get<EmailTemplate>(sql, predicates);
        }

        public IEnumerable<PolarisNotification> PolarisNotifications(Pred[] predicates)
        {
            var sql = @"
            select *
            from CLC_Notices.dbo.PolarisNotifications
            /**where**/
            ";

            return Get<PolarisNotification>(sql, predicates);
        }


        public static BranchEmailTemplateValue GetEmailTemplate(int orgId, int notificationTypeId, int languageId = 1033)
        {
            var val = EmailTemplateCache
                .Where(t => new[] { 1, orgId, Methods.GetParentOrg(orgId).OrganizationID }.Contains(t.OrganizationId) && t.NotificationTypeId == notificationTypeId && t.LanguageId == languageId)
                .GroupBy(t => t.EmailFormatId)
                .Select(g => new { FormatId = g.Key, g.OrderByDescending(g2 => g2.OrganizationId).First().Url }).ToList();

            var foo = val;

            var output = new BranchEmailTemplateValue
            {
                OrganizationId = orgId,
                NotificationTypeId = notificationTypeId,
                LanguageId = languageId,
                PlaintextTemplateUrl = val.Single(v => v.FormatId == 1).Url,
                HtmlTempalateUrl = val.Single(v => v.FormatId == 2).Url
            };

            if (output.HtmlTempalateUrl.StartsWith("/")) { output.HtmlTempalateUrl = $"{AppSettings.BaseURL}{output.HtmlTempalateUrl}"; }
            if (output.PlaintextTemplateUrl.StartsWith("/")) { output.PlaintextTemplateUrl = $"{AppSettings.BaseURL}{output.PlaintextTemplateUrl}"; }

            return output;
        }

        public IEnumerable<PolarisNotification> GetEmailNotices(NotificationType[] noticeTypes)
        {
            var preds = new[]
            {
                Pred.Field<PolarisNotification>(pn => pn.DeliveryOptionID, "=", (int)DeliveryOption.EmailAddress),
                Pred.Field<PolarisNotification>(pn => pn.NotificationTypeID, "in", noticeTypes.Select(nt=>(int)nt))
            };

            return PolarisNotifications(preds);
        }

        public IEnumerable<PolarisNotification> GetSmsNotices()
        {
            var preds = new[]
            {
                Pred.Field<PolarisNotification>(pn => pn.DeliveryOptionID, "=", (int)DeliveryOption.TXTMessaging)
            };

            return PolarisNotifications(preds);
        }

        public IEnumerable<PolarisNotification> GetEmailNotices(NotificationType noticeType)
        {
            return GetEmailNotices(new[] { noticeType });
        }

        public IEnumerable<PolarisNotification> GetPhoneNotices()
        {
            var preds = new[]
            {
                Pred.Field<PolarisNotification>(pn => pn.DeliveryOptionID, "in", new[] { (int)DeliveryOption.Phone1, (int)DeliveryOption.Phone2, (int)DeliveryOption.Phone3 }),
                Pred.Field<PolarisNotification>(pn => pn.NotificationTypeID, "in", new[] { (int)NotificationType.FirstOverdue, (int)NotificationType.SecondOverdue, (int)NotificationType.ThirdOverdue, (int)NotificationType.Bill, (int)NotificationType.Hold, (int)NotificationType.SecondHold })
            };

            return PolarisNotifications(preds).Where(n => AppSettings.LibrariesToCall.Contains(n.PatronLibrary));
        }

        public IEnumerable<string> GetSmsNumbersWithOverdues()
        {
            var sql = @"
                select case when len(d.phone) = 10 then '1' end + d.phone [phone]
	                from (
	                select distinct Polaris.dbo.CLC_Custom_StripCharacters(case pr.TxtPhoneNumber when 1 then pr.PhoneVoice1 when 2 then pr.PhoneVoice2 when 3 then pr.PhoneVoice3 end) [phone]
	                from Polaris.polaris.ItemCheckouts ic
	                join Polaris.Polaris.PatronRegistration pr
		                on pr.PatronID = ic.PatronID
	                where ic.DueDate < getdate()
		                and isnull(pr.TxtPhoneNumber, 0) > 0
                ) d";
            return Get<string>(sql);
        }

        public IEnumerable<string> GetSmsNumbersWithHolds()
        {
            var sql = @"
                select case when len(d.phone) = 10 then '1' end + d.phone [phone]
	                from (
	                select distinct Polaris.dbo.CLC_Custom_StripCharacters(case pr.TxtPhoneNumber when 1 then pr.PhoneVoice1 when 2 then pr.PhoneVoice2 when 3 then pr.PhoneVoice3 end) [phone]
                    from polaris.polaris.SysHoldRequests shr
                    join Polaris.polaris.PatronRegistration pr
	                    on pr.PatronID = shr.PatronID
                    where shr.SysHoldStatusID = 6
		                and isnull(pr.TxtPhoneNumber, 0) > 0
                ) d";
            return Get<string>(sql);
        }

        public int? GetEmailEventTypeId(string eventType)
        {
            var sql = "select * from clc_notices.dbo.EmailEventTypeMap where EventType = @eventType";
            var result = Query<EmailEventTypeMap>(sql, new { eventType });
            return result.Any() ? new int?(result.Single().MapsToEmailEventId) : null;
        }

        public int GetLibraryFromEmailDomain(string domain)
        {
            var sql = "select OrganizationID from clc_notices.dbo.EmailDomains where domain = @domain";
            return Query<int>(sql, new { domain }).SingleOrDefault();
        }

        public IEnumerable<PolarisOrganization> PolarisOrganizations(Pred pred = null)
        {
            return PolarisOrganizations(pred == null ? null : new[] { pred });
        }

        public IEnumerable<PolarisOrganization> PolarisOrganizations(Pred[] preds)
        {
            var sql = "select * from clc_notices.dbo.PolarisOrganizations /**where**/";
            return Get<PolarisOrganization>(sql, preds);
        }

        //public static PolarisOrganization GetParentOrg(int orgId)
        //{
        //    var parentId = orgId == 1 ? 1 : OrganizationsCache.SingleOrDefault(o => o.OrganizationID == orgId)?.ParentOrganizationID ?? 1;
        //    return OrganizationsCache.Single(o => o.OrganizationID == parentId);
        //}


        //public static T GetValue<T>(string mnemonic, int organizationId = 1, int languageId = 1033)
        //{
        //    mnemonic = mnemonic.ToLower();

        //    var orgIds = new List<int>() { organizationId, 1 };

        //    var parent = GetParentOrg(organizationId);
        //    if (parent.OrganizationCodeID == 2) { orgIds.Add(parent.OrganizationID); }

        //    var val = SettingValueCache.Where(v => orgIds.Contains(v.OrganizationID) && v.Mnemonic == mnemonic && v.LanguageID == languageId)
        //                              .OrderByDescending(v => v.OrganizationID)
        //                              .FirstOrDefault()?
        //                              .Value;

        //    if (string.IsNullOrWhiteSpace(val))
        //    {
        //        var defaultValue = SettingTypeCache.SingleOrDefault(t => t.Mnemonic == mnemonic)?.DefaultValue;
        //        if (string.IsNullOrEmpty(defaultValue)) { throw new ArgumentException($"No default found for {mnemonic}"); }
        //        val = defaultValue;
        //    }

        //    return (T)Convert.ChangeType(val, typeof(T));
        //}


        //public static void Test()
        //{
        //    var foo = new[]
        //    {
        //        Pred.Field<PolarisOrganization>(o=>o.OrganizationID, "in", new[] {1,2,3,4 }),
        //        Pred.Field<PolarisOrganization>(o=>o.OrganizationCodeID, ">", 1)
        //    };

        //    var sql = @"
        //    select *
        //    from Polaris.Polaris.Organizations
        //    /**where**/
        //    ";

        //    var builder = new SqlBuilder();
        //    var template = builder.AddTemplate(sql);

        //    foreach (var p in foo)
        //    {
        //        var par = new DynamicParameters();
        //        par.Add(p.PropertyName, p.Value);
        //        builder.Where($"{p.PropertyName} {p.Operator} @{p.PropertyName}", par);

        //    }

        //    var zxcv = template.Parameters;
        //    var lkj = template.RawSql;

        //    using (var conn = new SqlConnection("Server=devdb;Database=polaris;Trusted_Connection=True;"))
        //    {
        //        var asdf = conn.Query(template.RawSql, template.Parameters);
        //        var qwer = foo;
        //    }


        //    var bar = foo;
        //}

        //public static string PolarisNotifications(int? patronId = null, List<NotificationType> noticeTypes = null, List<DeliveryOption> deliveryOptions = null)
        //{
        //    var sql = @"
        //    select *
        //    from CLC_Notices.dbo.PolarisNotifications pn
        //    /**where**/
        //    ";

        //    var builder = new SqlBuilder();
        //    var template = builder.AddTemplate(sql);

        //    if (patronId.HasValue)
        //    {
        //        builder.Where("pn.patronid = @patronid", new { patronId });
        //    }

        //    if (noticeTypes != null)
        //    {
        //        builder.Where("pn.NotificationTypeId in @notificationTypes", new { notificationTypes = noticeTypes.Select(nt => (int)nt) });
        //    }

        //    if (deliveryOptions != null)
        //    {
        //        builder.Where("pn.DeliveryOptionId in @deliveryOptions", new { deliveryOptions = deliveryOptions.Select(d => (int)d) });
        //    }

        //    using (var conn = new SqlConnection("Server=devdb;Database=CLC_Notices;Trusted_Connection=True;"))
        //    {
        //        var foo = conn.Query<PolarisNotification>(template.RawSql, template.Parameters);
        //        var bar = foo;
        //    }

        //    var asdf = template.Parameters;
        //    return template.RawSql;
        //}

    }

    public class Pred
    {
        public string PropertyName { get; set; }
        public string Operator { get; set; }
        public object Value { get; set; }

        public static Pred Field<T>(Expression<Func<T, object>> expression, string op, object value)
        {
            var foo = GetProperty(expression) as PropertyInfo;
            return new Pred { PropertyName = foo.Name, Operator = op, Value = value };
        }

        public static MemberInfo GetProperty(LambdaExpression lambda)
        {
            Expression expr = lambda;
            for (; ; )
            {
                switch (expr.NodeType)
                {
                    case ExpressionType.Lambda:
                        expr = ((LambdaExpression)expr).Body;
                        break;
                    case ExpressionType.Convert:
                        expr = ((UnaryExpression)expr).Operand;
                        break;
                    case ExpressionType.MemberAccess:
                        MemberExpression memberExpression = (MemberExpression)expr;
                        MemberInfo mi = memberExpression.Member;
                        return mi;
                    default:
                        return null;
                }
            }
        }
    }
}
