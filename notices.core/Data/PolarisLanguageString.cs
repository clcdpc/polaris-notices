﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notices.Core
{
    public class PolarisLanguageString
    {
        public int OrganizationId { get; set; }
        public int LanguageId { get; set; }
        public string Mnemonic { get; set; }
        public string Value { get; set; }
    }
}
