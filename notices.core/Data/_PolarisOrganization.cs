﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notices.Core.Data
{
    public partial class PolarisOrganization
    {
        public override string ToString()
        {
            return $"{OrganizationID} - {Name}";
        }
    }
}
