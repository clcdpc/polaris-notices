﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace Notices.Core
{
    public static class AppSettings
    {
        public const string NOTICE_UPDATE_SERVICE_NAME = "clc-notice-update-service";
        public const string SMS_SERVICE_NAME = "clc-sms-service";

        public static bool Debug => GetValue<bool>("debug");
        public static bool Dev => GetValue<bool>("dev");
        public static bool DialinEnableEmailListOption => GetValue<bool>("dialin_enable_title_email_option", true);
        public static bool EmailUseStaging => GetValue<bool>("email_use_staging", true);
        public static bool EnableDialin => GetValue<bool>("enable_dialin");
        public static bool EnableDialout => GetValue<bool>("enable_dialout");
        public static bool EnableEmailProcessing => GetValue<bool>("enable_email_processing");
        public static bool EnableFailedUpdateCheck => GetValue<bool>("enable_failed_update_check", false);
        public static bool EnableHangfireMsmq => GetValue<bool>("enable_hangfire_msmq");
        public static bool EnableItivaTransfer => GetValue<bool>("enable_itiva_transfer");
        public static bool EnableNotifier => GetValue<bool>("enable_notifier");
        public static bool EnablePrtgSensors => GetValue<bool>("enable_prtg_sensors");
        public static bool EnableSmsProcessing => GetValue<bool>("enable_sms_processing");
        public static bool EnableSpamRejectionRemoval => GetValue("enable_spam_rejection_removal", true);
        public static bool LibrarySpecificSpam => GetValue<bool>("library_specific_spam", true);
        public static bool PreventDuplicateSmsMessages => GetValue<bool>("sms_prevent_duplicates", true);
        public static bool RebuildHangfireJobs => GetValue<bool>("rebuild_hangfire_jobs");
        public static bool RecordCalls => GetValue("record_calls", true);
        public static bool VerifyAfterHoursHolds => GetValue("verify_after_hours_holds", false);
        public static bool VerifyAfterHoursOverdues => GetValue("verify_after_hours_overdues", false);

        public static int AfterHoursSmsSendHour => GetValue("after_hours_sms_send_hour", 8);
        public static int AfterHoursSmsSendMinute => GetValue("after_hours_sms_send_minute", 00);
        public static int CallRetentionDays => GetInt("call_retention_days");
        public static int DialoutEndTimeInprocessBuffer => GetValue("dialout_end_time_inprocess_buffer", 5);
        public static int DialoutRunInterval => GetValue("dialout_run_interval", 1);
        public static int PinDigits => GetInt("pin_digit_length");
        public static int RecordingRetentionDays => GetInt("recording_retention_days");
        public static int SMSStartHour => GetValue("sms_start_hour", 10);
        public static int SMSStopHour => GetValue("sms_stop_hour", 20);
        public static int SmsInvalidPhoneNumberRecordSet => GetValue<int>("sms_invalid_phone_number_record_set");
        public static int SmsRetentionDays => GetInt("sms_retention_days");
        public static int TestPatronId => GetValue("test_patron_id", 0);
        public static int TimeoutLimit => GetInt("timeout_limit");
        public static int WeekdayStartHour => GetInt("weekday_start_hour");
        public static int WeekdayStopHour => GetInt("weekday_stop_hour");
        public static int SaturdayStartHour => GetInt("saturday_start_hour");
        public static int SaturdayStopHour => GetInt("saturday_stop_hour");
        public static int SundayStartHour => GetInt("sunday_start_hour");
        public static int SundayStopHour => GetInt("sunday_stop_hour");

        public static string AdminPermissionGroup => GetString("admin_permission_group");
        public static string ClcTenant => GetString("clc_tenant");
        public static string AuthString => GetString("auth_string");
        public static string BaseURL => GetString("base_url").TrimEnd('/');
        public static string DbServer => GetString("db_server");
        public static string DebugPhoneNumber => GetString("debug_phone_number");
        public static string DialinURL => BaseURL + "/dialin/index";
        public static string DialoutFallbackUrl => BaseURL + "/dialout/FallbackResponse";
        public static string DialoutStatusUrl => BaseURL + "/dialout/TwilioStatus";
        public static string EmailProvider => GetString("email_provider").ToLower();
        public static string EmailSender => GetString("email_sender");
        public static string EmailTemplateName => GetString("email_template_name");
        public static string FromPhone => GetString("dialout_from_phone");
        public static string HipchatNotificationToken => GetString("hipchat_auth_token");
        public static string HipchatSmsNumber => GetString("hipchat_sms_number");
        public static string ItivaNumber => GetString("itiva_number");
        public static string MandrillApiKey => GetString("mandrill_api_key");
        public static string MandrillUnsignedRejectionsSensorUrl => GetString("mandrill_unsigned_rejections_sensor_url");
        public static string MessageServiceSid => GetString("message_service_sid");
        public static string MessagingProfileId => GetValue<string>("messaging_profile_id");
        public static string PapiAccessId => GetString("papi_user");
        public static string PapiAccessKey => GetString("papi_key");
        public static string PapiHostname => GetString("papi_request_hostname");
        public static string PapiStaffDomain => GetString("staff_domain");
        public static string PapiStaffPassword => GetString("staff_password");
        public static string PapiStaffUsername => GetString("staff_username");
        public static string PostmarkDbKey => GetString("postmark_db_key");
        public static string PostmarkOplinSmsFromAddress => GetString("postmark_oplin_sms_from");
        public static string PostmarkEmailKey => GetString("postmark_email_key");
        public static string PostmarkOplinSmsKey => GetString("postmark_oplin_sms_key");
        public static string PostmarkIspBlockSensorUrl => GetString("postmark_isp_block_sensor_url");
        public static string PostmarkTelephonyKey => GetString("postmark_telephony_key");
        public static string RabbitMqHostname => GetValue<string>("rabbitmq_hostname");
        public static string RabbitMqPassword => GetValue<string>("rabbitmq_password");
        public static string RabbitMqUsername => GetValue<string>("rabbitmq_username");
        public static string RabbitMqVirtualHost => GetValue<string>("rabbitmq_virtualhost");
        public static string RenewalNumber => GetString("renewal_number");
        public static string SmsReplyText => GetString("sms_reply_text");
        public static string SmsSender => GetValue<string>("sms_sender");
        public static string SmsStartReplyText => GetString("sms_start_reply_text");
        public static string SmsStatusCallback => BaseURL + "/sms/status";
        public static string SmsStopNoteIdentifier => GetString("sms_stop_note_identifier");
        public static string SmsStopReplyText => GetString("sms_stop_reply_text");
        public static string SmsStopZeroAccountsReplyText => GetString("sms_stop_zero_accounts_reply_text");
        public static string SmsTestNumber => GetString("sms_test_number");
        public static string TelnyxV1ApiKey => GetValue<string>("telnyx_v1_api_key");
        public static string TelnyxV2ApiKey => GetValue<string>("telnyx_v2_api_key");
        public static string TestBarcode => GetString("test_barcode");
        public static string TestPin => GetString("test_pin");
        public static string TwilioAccountSid => GetString("twilio_account_number");
        public static string TwilioAuthToken => GetString("twilio_auth_token");
        public static string TwilioCallURL => BaseURL + "/dialout/TwilioResponse";
        public static string TwilioCarrierViolationErrorSensorUrl => GetString("twilio_carrier_violation_errors_sensor_url");
        public static string TwilioErrorsSensorUrl => GetString("twilio_errors_sensor_url");
        public static string UnknownBounceSensorUrl => GetString("unknown_bounce_sensor_url");


        public static List<int> LibrariesToCall => ConfigurationManager.AppSettings["libraries_to_call"].Split(';').Select(t => Convert.ToInt32(t)).ToList();
        public static List<int> TelnyxLibraries => ConfigurationManager.AppSettings["telnyx_libraries"].Split(';').Select(s => int.Parse(s)).ToList();
        public static List<string> EmailProcessingExcludedReceivers => !string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["email_processing_excluded_receivers"]) ? ConfigurationManager.AppSettings["email_processing_excluded_receivers"].Split(';').Select(s => s.Trim()).Where(s => s.Length > 0).ToList() : new List<string>();
        public static List<string> EmailProcessingExcludedSenders => !string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["email_processing_excluded_senders"]) ? ConfigurationManager.AppSettings["email_processing_excluded_senders"].Split(';').Select(s => s.Trim()).Where(s => s.Length > 0).ToList() : new List<string>();
        public static List<string> EmailProcessingExcludedWords => !string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["email_processing_excluded_words"]) ? ConfigurationManager.AppSettings["email_processing_excluded_words"].Split(';').Select(s => s.Trim()).Where(s => !string.IsNullOrWhiteSpace(s)).ToList() : new List<string>();
        public static List<string> NotifyEmailAddresses => ConfigurationManager.AppSettings["notify_email_addresses"].Split(';').Select(s => s.Trim()).ToList();
        public static List<string> NotifyPhoneNumbers => ConfigurationManager.AppSettings["notify_phone_numbers"].Split(';').Select(s => s.Trim()).ToList();
        public static List<string> SmsStartTriggerWords => ConfigurationManager.AppSettings["sms_start_trigger_words"].Split(';').Select(s => s.Trim()).ToList();
        public static List<string> SmsStopTriggerWords => ConfigurationManager.AppSettings["sms_stop_trigger_words"].Split(';').Select(s => s.Trim()).ToList();

        static T GetValue<T>(string key, T defaultValue = default(T))
        {
            if (!ConfigurationManager.AppSettings.AllKeys.Contains(key)) return defaultValue;
            return (T)Convert.ChangeType(ConfigurationManager.AppSettings[key], typeof(T));
        }

        static string GetString(string key)
        {
            return ConfigurationManager.AppSettings[key];
        }

        //static bool GetBoolean(string key)
        //{
        //    return GetBoolean(key);
        //}

        static int GetInt(string key)
        {
            return int.Parse(ConfigurationManager.AppSettings[key]);
        }

        static double GetDouble(string key)
        {
            return double.Parse(ConfigurationManager.AppSettings[key]);
        }
    }
}
