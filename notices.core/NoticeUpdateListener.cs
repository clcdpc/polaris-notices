﻿using Clc.Polaris.Api;
using Clc.Polaris.Api.Models;
using Notices.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace Notices.Core
{
    public class NoticeUpdateListener : QueueListener<NoticeUpdate>
    {
        public NoticeUpdateListener(string hostname, string virtualHost, string queue, string username, string password, string failedQueue, string invalidQueue, bool exitWhenEmpty = false) : base(hostname, virtualHost, queue, username, password, failedQueue, invalidQueue, exitWhenEmpty)
        {
        }

        public override QueueProcessResult Process(NoticeUpdate input)
        {
            var papi = new PapiClient
            {
                AccessID = AppSettings.PapiAccessId,
                AccessKey = AppSettings.PapiAccessKey,
                Hostname = AppSettings.PapiHostname,
                StaffOverrideAccount = new PolarisUser
                {
                    Domain = AppSettings.PapiStaffDomain,
                    Username = AppSettings.PapiStaffUsername,
                    Password = AppSettings.PapiStaffPassword
                }
            };

            var updateParams = new NotificationUpdateParams()
            {
                NotificationTypeId = input.NotificationTypeId,
                ReportingOrgID = input.ReportingOrgId,
                DeliveryOptionId = input.DeliveryOptionId,
                DeliveryString = input.DeliveryString,
                NotificationDeliveryDate = input.DeliveryDate,
                PatronId = input.PatronId,
                ItemRecordId = input.ItemRecordId,
                NotificationStatusId = input.NotificationStatus,
                Details = input.Details
            };

            logger.Trace(new JavaScriptSerializer().Serialize(updateParams));

            var result = papi.NotificationUpdate(updateParams);
            var errorMsg = "";

            if (result.Response?.IsSuccessStatusCode != null && result.Response.IsSuccessStatusCode)
            {
                if (result.Data.PAPIErrorCode == 0)
                {
                    logger.Trace("{0}:OK:{1}", input.PatronId, result.Data);
                    return new QueueProcessResult(true);
                }

                try
                {
                    errorMsg = $"{result.Data.PAPIErrorCode} - {result.Data.ErrorMessage}.";
                }
                catch (Exception ex)
                {
                    errorMsg = $"Error building papi error message: {ex.ToString()}.";
                }
            }
            else
            {
                errorMsg += $" {result.Response.StatusCode} - {result.Response.ReasonPhrase}";
            }

            input.Error = errorMsg;
            logger.Error($"{input.PatronId} - {input.ItemRecordId} | {errorMsg}");
            if (result.Data.ErrorMessage.StartsWith("NotificationQueue entry does not exist for this delivery option", StringComparison.InvariantCultureIgnoreCase))
            {
                return new QueueProcessResult(true);
            }
            return new QueueProcessResult(false, errorMsg);
        }
    }
}
