﻿using Notices.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notices.Core
{
    public static class Constants
    {
        public static NotificationType[] OverdueNotificationTypes => new[] { NotificationType.FirstOverdue, NotificationType.SecondOverdue, NotificationType.ThirdOverdue };
        public static int[] OverdueNotificationTypeIds => OverdueNotificationTypes.Select(nt => (int)nt).ToArray();
    }
}
