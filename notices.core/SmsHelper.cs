﻿using NLog;
using Notices.Core.Data;
using Notices.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notices.Core
{
    public static class SmsHelper
    {
        static Logger logger = LogManager.GetCurrentClassLogger();

        public static int RemovePatronSmsDetails(string pn, string note, bool isSmsOptOut = false)
        {
            var papi = Methods.CreatePapiClient();
            note = $"{DateTime.Now.ToShortDateString()} - {note}";

            var db = new DbHelper(AppSettings.DbServer);

            var patrons = db.GetPatronsForSmsNumber(pn).ToList();

            if (patrons.Count == 0)
            {
                logger.Warn("No patrons found for phone number {0}", pn);
                return 0;
            }

            foreach (var patron in patrons)
            {
                logger.Trace("Processing patron {0} for phone number {1}", patron.patronid, pn);

                var currentPhoneCarrierId = 0;
                try
                {
                    currentPhoneCarrierId = db.RemovePatronSmsSettings(patron.patronid);
                    logger.Trace("Current phone carrier: {0}", currentPhoneCarrierId);
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                }


                var finalNote = note + $" CarrierID: {currentPhoneCarrierId}";

                if (isSmsOptOut)
                {
                    var predicates = new[]
                    {
                        Pred.Field<RecordSet>(r=>r.OrganizationID, "=", patron.PatronLibraryID),
                        Pred.Field<RecordSet>(r=>r.RecordSetTypeID, "=", (int)RecordSetTypes.SmsStop)
                    };

                    var rsid = db.RecordSets(predicates).Single().RecordSetID;
                    var rsAddResult = papi.RecordSetContentAdd(rsid, patron.patronid);
                    if (rsAddResult.Data?.PAPIErrorCode < 0)
                    {
                        logger.Error(rsAddResult?.Data?.PAPIErrorCode + " " + rsAddResult?.Data?.ErrorMessage);
                    }
                }
                //var updateResult = papi.PatronUpdateOverride(patron.Barcode, options);
                logger.Trace("Add note for {0}", patron.patronid);
                db.AddPatronNote(patron.patronid, finalNote);
                logger.Trace("Finished patron {0}", patron.patronid);
            }

            return patrons.Count;
        }
    }
}
