﻿using Notices.Core.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notices.Core
{
    public static class DbSettings
    {
        static List<SettingValue> SettingValueCache { get; set; }
        static List<SettingType> SettingTypeCache { get; set; }
        static List<PolarisOrganization> OrganizationsCache { get; set; }

        public static void Init()
        {
            var db = new DbHelper();

            if (SettingValueCache == null) SettingValueCache = db.SettingValues().ToList();
            if (SettingTypeCache == null) SettingTypeCache = db.SettingTypes().ToList();
            if (OrganizationsCache == null) OrganizationsCache = db.PolarisOrganizations().ToList();
        }

        public static string SmsHoldTillDateFormatString(int orgId = 1, int languageId = 1033) => GetValue<string>("sms_holdtilldate_format_string", orgId, languageId);
        public static string SmsSender(int orgId = 1, int languageId = 1033) => GetValue<string>("sms_sender", orgId, languageId);
        public static string DialoutFromPhone(int orgId = 1, int languageId = 1033) => GetValue<string>("dialout_from_phone", orgId, languageId);
        public static string PickupBranchUrl(int orgId = 1, int languageId = 1033) => GetValue<string>("pickup_branch_url", orgId, languageId);
        public static bool DialoutCallOnClosedDate(int orgId = 1, int languageId = 1033) => GetValue<bool>("dialout_call_for_holds_on_closed_date", orgId, languageId);
        public static bool DialoutCallOnClosedHoursOfOperation(int orgId = 1, int languageId = 1033) => GetValue<bool>("dialout_call_for_holds_on_closed_hours_of_operation", orgId, languageId);
        public static bool EmailCombineOverdueNotices(int orgId = 1, int languageId = 1033) => GetValue<bool>("email_combine_overdue_notices", orgId, languageId);
        public static bool EmailEnableExpirationNotices(int orgId = 1, int languageId = 1033) => GetValue<bool>("email_enable_expiration_reminder_notices", orgId, languageId);
        public static bool OverrideHoldDateFormat(int orgId = 1, int languageId = 1033) => GetValue<bool>("override_hold_date_format", orgId, languageId);
        public static bool PrebuildDialoutMessages(int orgId = 1, int languageId = 1033) => GetValue<bool>("prebuild_dialout_messages", orgId, languageId);
        public static bool EmailSeparateInnreachItems(int orgId = 1, int languageId = 1033) => GetValue<bool>("email_separate_innreach_items", orgId, languageId);

        public static T GetValue<T>(string mnemonic, int organizationId = 1, int languageId = 1033)
        {
            mnemonic = mnemonic.ToLower();

            var orgIds = new List<int>() { organizationId, 1 };

            var parent = GetParentOrg(organizationId);
            if (parent.OrganizationCodeID == 2) { orgIds.Add(parent.OrganizationID); }

            var val = SettingValueCache.Where(v => orgIds.Contains(v.OrganizationID) && v.Mnemonic == mnemonic && v.LanguageID == languageId)
                                      .OrderByDescending(v => v.OrganizationID)
                                      .FirstOrDefault()?
                                      .Value;

            if (string.IsNullOrWhiteSpace(val))
            {
                var defaultValue = SettingTypeCache.SingleOrDefault(t => t.Mnemonic == mnemonic)?.DefaultValue;
                if (string.IsNullOrEmpty(defaultValue)) { throw new ArgumentException($"No default found for {mnemonic}"); }
                val = defaultValue;
            }

            return (T)Convert.ChangeType(val, typeof(T));
        }

        public static PolarisOrganization GetParentOrg(int orgId)
        {
            var parentId = orgId == 1 ? 1 : OrganizationsCache.SingleOrDefault(o => o.OrganizationID == orgId)?.ParentOrganizationID ?? 1;
            return OrganizationsCache.Single(o => o.OrganizationID == parentId);
        }
    }
}
