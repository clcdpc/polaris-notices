﻿using Notices.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Notices;
using NLog;
using clc_twilio_csharp;
using Notices.Core.Data;
using Clc.Polaris.Api;
using clc_telnyx_csharp;
using System.Data.SqlClient;
using Dapper;
using Clc.Postmark;

namespace Notices.Core
{
    public class SmsListener : QueueListener<SmsMessage>
    {
        static List<string> PhoneNumbersWithOverdueItems { get; set; }
        static List<string> PhoneNumbersWithHeldItems { get; set; }

        bool IsAfterHoursQueue => string.Equals(Queue, Queues.AFTER_HOURS_MESSAGES, StringComparison.OrdinalIgnoreCase);

        DbHelper db;
        PapiClient papi;

        public SmsListener(string queue, bool exitWhenEmpty = false) : base(AppSettings.RabbitMqHostname, AppSettings.RabbitMqVirtualHost, queue, AppSettings.RabbitMqUsername, AppSettings.RabbitMqPassword, Queues.FAILED_MESSAGES, Queues.FAILED_MESSAGES, exitWhenEmpty, LogManager.GetCurrentClassLogger())
        {
            logger.Trace("Start Hour: {0} | Stop Hour: {1}", AppSettings.SMSStartHour, AppSettings.SMSStopHour);

            var db = new DbHelper(AppSettings.DbServer);

            if (IsAfterHoursQueue)
            {
                if (AppSettings.VerifyAfterHoursOverdues)
                {
                    PhoneNumbersWithOverdueItems = db.GetSmsNumbersWithOverdues().ToList();
                }

                if (AppSettings.VerifyAfterHoursHolds)
                {
                    PhoneNumbersWithHeldItems = db.GetSmsNumbersWithHolds().ToList();
                }
            }
        }

        public override QueueProcessResult Process(SmsMessage input)
        {
            if (DateTime.Now.Hour < AppSettings.SMSStartHour || DateTime.Now.Hour > AppSettings.SMSStopHour)
            {
                if (Queue != Queues.AFTER_HOURS_MESSAGES)
                {
                    MoveMessage(input.ToJson(), Queues.AFTER_HOURS_MESSAGES);
                    logger.Info("Moved to after hours queue: {0} - {1}", input.To, input.Body);
                    return new QueueProcessResult(true);
                }
            }

            try
            {
                var errorMessage = "";
                var invalidPhoneNumber = false;

                input.To = new string(input.To.Where(c => char.IsDigit(c)).ToArray());

                if (input.To.StartsWith("000"))
                {
                    invalidPhoneNumber = true;
                    errorMessage = "Invalid phone number";
                }

                if (input.To.Length < 10)
                {
                    invalidPhoneNumber = true;
                    errorMessage = "Invalid phone number, not enough digits";
                }

                if (input.To.Length > 10 && !(input.To.StartsWith("1") && input.To.Length == 11))
                {
                    invalidPhoneNumber = true;
                    errorMessage = "Invalid phone number, too many digits or wrong country code";
                }

                if (invalidPhoneNumber)
                {
                    var patrons = db.GetPatronsForSmsNumber(input.To).Select(p => p.patronid);
                    if (patrons.Any())
                    {
                        var addToRecordsetResult = papi.RecordSetContentAdd(AppSettings.SmsInvalidPhoneNumberRecordSet, patrons);
                    }
                    return new QueueProcessResult(false, errorMessage);
                }

                if (IsAfterHoursQueue)
                {
                    if (AppSettings.VerifyAfterHoursOverdues && !PhoneNumbersWithOverdueItems.Contains(input.To) && input.Body.ToLower().Contains("overdue"))
                    {
                        logger.Info("Overdue item doens't exist: {0} - {1}", input.To, input.Body);
                        return new QueueProcessResult(true);
                    }

                    if (AppSettings.VerifyAfterHoursHolds && !PhoneNumbersWithHeldItems.Contains(input.To) && (input.Body.ToLower().Contains("pickup") || input.Body.ToLower().Contains("pick up") || input.Body.ToLower().Contains("pick-up")))
                    {
                        logger.Info("Held item doens't exist: {0} - {1}", input.To, input.Body);
                        return new QueueProcessResult(true);
                    }
                }

                var sender = Methods.GetSmsSender(input.OrganizationId);
                return sender.Send(input);

                /*
                switch (Methods.GetSmsSender(input.OrganizationId).ToLower())
                {
                    case "telnyx":
                        return SendWithTelnyx(input);
                    case "oplin":
                        return SendWithOplin(input);
                    case "twilio":
                        return SendWithTwilio(input);
                    default:
                        throw new ArgumentException("invalid sms sender specified");
                }
                */

            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return new QueueProcessResult(false, ex.Message);
            }
        }

        //QueueProcessResult SendWithTwilio(SmsMessage input)
        //{
        //    var result = twilio.SendMessageWithService(input.To, input.Body, AppSettings.MessageServiceSid);

        //    if (result?.Data == null)
        //    {
        //        var errorMsg = "Twilio SendMessageWithService result null";
        //        errorMsg = errorMsg += "\r\n" + result.Response.Content;
        //        logger.Error(errorMsg);
        //        return new QueueProcessResult(false, errorMsg);
        //    }
        //    else
        //    {
        //        if (result.Data.ErrorCode == null && result.Exception == null)
        //        {
        //            logger.Info("Sent: {0} - {1} - {2}", input.To, input.Body, result?.Data.Sid);
        //            return new QueueProcessResult(true);
        //        }

        //        if (result.Data.ErrorCode.GetValueOrDefault() == 21211)
        //        {
        //            SmsHelper.RemovePatronSmsDetails(input.To, $"Phone number {input.To} is invalid");
        //            return new QueueProcessResult(true);
        //        }

        //        var errorMsg = "";
        //        if (result.Data.ErrorCode.HasValue) errorMsg += $"{result.Data.ErrorCode} - {result.Data.ErrorMessage}";
        //        if (result.Exception != null) { errorMsg += $" | {result.Exception}"; }

        //        logger.Error($"Failed: {input.To} - {input.Body} | {errorMsg}");
        //        return new QueueProcessResult(false, errorMsg);

        //    }
        //}

        //QueueProcessResult SendWithTelnyx(SmsMessage input)
        //{
        //    var errorMessage = "";
        //    var result = telnyx.SendSmsWithProfile(AppSettings.MessagingProfileId, $"+{input.To}", input.Body);

        //    if (result?.Data == null)
        //    {
        //        var errorMsg = $"Telnyx SendSmsWithProfile result null\r\n{result.Response.Content}";
        //        logger.Error(errorMsg);
        //        return new QueueProcessResult(false, errorMsg);
        //    }
        //    else
        //    {
        //        var data = result.Data.data;

        //        if (data.errors.Any()) { errorMessage += $"{data.id} - {data.to} - {string.Join(", ", data.errors)}"; }
        //        if (result.Exception != null) { errorMessage = $"\r\n{result.Exception.Message} \r\n {result.Exception.StackTrace}"; }

        //        var error = !string.IsNullOrWhiteSpace(errorMessage);

        //        using (var conn = new SqlConnection(DbHelper.ConnectionString))
        //        {
        //            var sql = @"insert into TelnyxSmsMessages(Id,Timestamp,SentTo,SentFrom,Body,Parts,Status) values (@id, @timestamp, @phone_number, @from, @text, @parts, 'sent')";
        //            conn.Execute(sql, new { data.id, timestamp = DateTime.Now, data.to.FirstOrDefault().phone_number, data.from, data.text, data.parts });
        //        }

        //        if (!error)
        //        {
        //            logger.Info("Sent: {0} - {1} - {2}", input.To, input.Body, result?.Data.data.id);
        //            return new QueueProcessResult(true);
        //        }

        //        logger.Error($"Failed: {input.To} - {input.Body} | {errorMessage}");
        //        return new QueueProcessResult(false, errorMessage);
        //    }
        //}

        QueueProcessResult SendWithOplin(SmsMessage input)
        {
            var postmark = new PostmarkClient2(AppSettings.PostmarkDbKey);

            var errorMessage = "";
            input.Body = $"***{input.Body}***";
            var result = postmark.Send($"{input.To}@sms.oplin.org", AppSettings.PostmarkOplinSmsFromAddress, "", textBody: input.Body);

            if (result?.Data == null)
            {
                var errorMsg = $"Telnyx SendSmsWithProfile result null\r\n{result.Response.Content}";
                logger.Error(errorMsg);
                return new QueueProcessResult(false, errorMsg);
            }
            else
            {
                var data = result.Data;

                if (data.ErrorCode != 0) { errorMessage += $"{data.MessageID} - {data.To} - {data.ErrorCode}"; }
                if (result.Exception != null) { errorMessage = $"\r\n{result.Exception.Message} \r\n {result.Exception.StackTrace}"; }

                var error = !string.IsNullOrWhiteSpace(errorMessage);

                using (var conn = new SqlConnection(DbHelper.ConnectionString))
                {
                    var sql = @"insert into TelnyxSmsMessages(Id,Timestamp,SentTo,SentFrom,Body,Parts,Status) values (@id, @timestamp, @phone_number, @from, @text, @parts, 'sent')";
                    conn.Execute(sql, new { id = data.MessageID, timestamp = DateTime.Now, phone_number = data.To.Split('@')[0], from = AppSettings.PostmarkOplinSmsFromAddress, text = data.Message, parts = 1 });
                }

                if (!error)
                {
                    logger.Info("Sent: {0} - {1} - {2}", input.To, input.Body, result?.Data.MessageID);
                    return new QueueProcessResult(true);
                }

                logger.Error($"Failed: {input.To} - {input.Body} | {errorMessage}");
                return new QueueProcessResult(false, errorMessage);
            }
        }
    }
}
