﻿using Notices.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Notices;
using NLog;
using clc_twilio_csharp;
using clc_telnyx_csharp;
using System.Data.SqlClient;
using Dapper;
using Notices.Core.Data;
using Clc.Polaris.Api;

namespace Notices.Core
{
    public class TelnyxSmsListener : QueueListener<SmsMessage>
    {
        public bool Testing = false;

        DbHelper db;
        PapiClient papi;

        public TelnyxSmsListener(string queue, bool exitWhenEmpty = false) : base(AppSettings.RabbitMqHostname, AppSettings.RabbitMqVirtualHost, queue, AppSettings.RabbitMqUsername, AppSettings.RabbitMqPassword, Queues.FAILED_MESSAGES, Queues.FAILED_MESSAGES, exitWhenEmpty, LogManager.GetCurrentClassLogger())
        {
            db = new DbHelper(AppSettings.DbServer);
            papi = Methods.CreatePapiClient();
            logger.Trace("Start Hour: {0} | Stop Hour: {1}", AppSettings.SMSStartHour, AppSettings.SMSStopHour);
        }

        public override QueueProcessResult Process(SmsMessage input)
        {
            TelnyxClient telnyx = new TelnyxClient(AppSettings.TelnyxV2ApiKey)
            {
                SmsWebhookUrl = $"{AppSettings.BaseURL}/Sms/TelnyxSmsWebhook?auth={AppSettings.AuthString}"
            };

            if (DateTime.Now.Hour < AppSettings.SMSStartHour || DateTime.Now.Hour > AppSettings.SMSStopHour)
            {
                if (Queue != Queues.AFTER_HOURS_MESSAGES)
                {
                    MoveMessage(input.ToJson(), Queues.AFTER_HOURS_MESSAGES);
                    logger.Info("Moved to after hours queue: {0} - {1}", input.To, input.Body);
                    return new QueueProcessResult(true);
                }
            }

            try
            {
                var errorMessage = "";
                var invalidPhoneNumber = false;

                input.To = new string(input.To.Where(c => char.IsDigit(c)).ToArray());

                if (input.To.StartsWith("000"))
                {
                    invalidPhoneNumber = true;
                    errorMessage = "Invalid phone number";
                }

                if (input.To.Length < 10)
                {
                    invalidPhoneNumber = true;
                    errorMessage = "Invalid phone number, not enough digits";
                }

                if (input.To.Length > 10 && !(input.To.StartsWith("1") && input.To.Length == 11))
                {
                    invalidPhoneNumber = true;
                    errorMessage = "Invalid phone number, too many digits or wrong country code";
                }

                if (invalidPhoneNumber)
                {
                    var patrons = db.GetPatronsForSmsNumber(input.To).Select(p=>p.patronid);
                    if (patrons.Any())
                    {
                        var addToRecordsetResult = papi.RecordSetContentAdd(AppSettings.SmsInvalidPhoneNumberRecordSet, patrons);
                    }
                    return new QueueProcessResult(false, errorMessage);
                }

                if (input.To.Length == 10)
                {
                    input.To = $"1{input.To}";
                }

                if (Testing) { return new QueueProcessResult(true, "testing"); }

                var result = telnyx.SendSmsWithProfile(AppSettings.MessagingProfileId, $"+{input.To}", input.Body);

                if (result?.Data == null)
                {
                    var errorMsg = $"Telnyx SendSmsWithProfile result null\r\n{result.Response.Content}";
                    logger.Error(errorMsg);
                    return new QueueProcessResult(false, errorMsg);
                }
                else
                {
                    var data = result.Data.data;

                    if (data.errors.Any()) { errorMessage += $"{data.id} - {data.to} - {string.Join(", ", data.errors)}"; }
                    if (result.Exception != null) { errorMessage = $"\r\n{result.Exception.Message} \r\n {result.Exception.StackTrace}"; }

                    var error = !string.IsNullOrWhiteSpace(errorMessage);

                    using (var conn = new SqlConnection(DbHelper.ConnectionString))
                    {
                        var status = error ? "error" : "sent";
                        var sql = @"insert into TelnyxSmsMessages(Id,Timestamp,SentTo,SentFrom,Body,Parts) values (@id, @timestamp, @phone_number, @from, @text, @parts)";
                        conn.Execute(sql, new { data.id, timestamp = DateTime.Now, data.to.FirstOrDefault().phone_number, data.from, data.text, data.parts });
                    }

                    if (!error)
                    {
                        logger.Info("Sent: {0} - {1} - {2}", input.To, input.Body, result?.Data.data.id);
                        return new QueueProcessResult(true);
                    }

                    logger.Error($"Failed: {input.To} - {input.Body} | {errorMessage}");
                    return new QueueProcessResult(false, errorMessage);

                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return new QueueProcessResult(false, ex.Message);
            }
        }
    }
}
