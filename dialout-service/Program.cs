﻿using Notices.Core;
using Notices.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Topshelf;

namespace notice_update_service
{
    class Program
    {
        static void Main(string[] args)
        {
            HostFactory.Run(x =>
            {
                x.Service<NoticeUpdateService>(s =>
                {
                    s.ConstructUsing(name => new NoticeUpdateService());
                    s.WhenStarted(svc => svc.Start());
                    s.WhenStopped(svc => svc.Stop());
                });
                x.RunAsLocalSystem();

                x.SetDescription("CLC notice update service");
                x.SetDisplayName("_CLC Notice Update Service");
                x.SetServiceName("clc-notice-update-service");
            });
        }

        public class NoticeUpdateService
        {
            NoticeUpdateListener phoneQueue;
            NoticeUpdateListener emailQueue;

            public void Start()
            {
                phoneQueue = new NoticeUpdateListener(hostname: AppSettings.RabbitMqHostname,
                                              virtualHost: AppSettings.RabbitMqVirtualHost,
                                              queue: Queues.DIALOUT_NOTICE_UPDATES,
                                              username: AppSettings.RabbitMqUsername,
                                              password: AppSettings.RabbitMqPassword,
                                              failedQueue: Queues.FAILED_DIALOUT_UPDATES,
                                              invalidQueue: Queues.FAILED_DIALOUT_UPDATES,
                                              exitWhenEmpty: false);

                emailQueue = new NoticeUpdateListener(hostname: AppSettings.RabbitMqHostname,
                                              virtualHost: AppSettings.RabbitMqVirtualHost,
                                              queue: Queues.EMAIL_NOTICE_UPDATES,
                                              username: AppSettings.RabbitMqUsername,
                                              password: AppSettings.RabbitMqPassword,
                                              failedQueue: Queues.FAILED_EMAIL_UPDATES,
                                              invalidQueue: Queues.FAILED_EMAIL_UPDATES,
                                              exitWhenEmpty: false);

                phoneQueue.Start();
                emailQueue.Start();
            }

            public void Stop()
            {
                phoneQueue.Stop();
                emailQueue.Stop();
            }
        }
    }
}
