﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using clc_twilio_csharp;
using Newtonsoft.Json;

namespace sms_notice_resender
{
    public partial class Form1 : Form
    {
        public List<SmsMessage> Messages = new List<SmsMessage>();
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var ofd = new OpenFileDialog() { Title = "Open SMS Service Log File" };
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                ProcessFile(ofd.FileName);
            }
        }

        private void ProcessFile(string filename)
        {
            var lines = File.ReadAllLines(filename);

            var filteredLines = lines.Where(l => l.Contains("Message Received:")).Select(l => l.Split(new string[] { "Message Received:" }, StringSplitOptions.RemoveEmptyEntries)[1].TrimEnd('|')).ToList();

            
            //var filteredLines = lines.Where(l => l.Contains("Moved to after hours queue:")).Select(l => l.Split(new string[] { "Moved to after hours queue:" }, StringSplitOptions.RemoveEmptyEntries)[1]).ToList();

            foreach (var line in filteredLines)
            {
                var parts = line.Split(new char[] { '-' }, 2).Select(p => p.Trim()).ToArray();
                //Messages.Add(new SmsMessage(parts[0], parts[1]));
                Messages.Add(JsonConvert.DeserializeObject<SmsMessage>(line));
            }

            listBox1.DataSource = Messages;
            label1.Text = $"Loaded {Messages.Count} messages";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var accountSid = ConfigurationManager.AppSettings["twilio_account_number"];
            var authToken = ConfigurationManager.AppSettings["twilio_auth_token"];
            var twilio = new TwilioClient(ConfigurationManager.AppSettings["twilio_account_number"], ConfigurationManager.AppSettings["twilio_auth_token"]);
            foreach (var message in Messages)
            {
                try
                {
                    var result = twilio.SendMessageWithService(message.To, message.Body, ConfigurationManager.AppSettings["message_service_sid"]);
                    var foo = result;
                }
                catch { }
            }

        }
    }

    public class SmsMessage
    {
        public string To { get; set; }
        public string Body { get; set; }

        public SmsMessage()
        {

        }

        public SmsMessage(string to, string body)
        {
            To = to;
            Body = body;
        }

        public override string ToString()
        {
            return $"{To} - {Body}";
        }
    }
}
