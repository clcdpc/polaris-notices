﻿using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace azure_functions
{
    public class Functions
    {
        private static Configuration config;

        public static async Task<HttpResponseMessage> TwilioErrorWebhook(HttpRequestMessage req, TraceWriter log)
        {
            int[] excludedErrorCodes = { 32016 };
            int[] warningsToInclude = { 14107 };

            try
            {
                var body = req.Content.ReadAsStringAsync().Result;

                dynamic json = JObject.Parse(body);

                var level = Convert.ToString(json.more_info.LogLevel).ToLower();
                int error = json.more_info.ErrorCode;

                if ((level == "error" && !excludedErrorCodes.Contains(error)) || warningsToInclude.Contains(error))
                {
                    if (config == null) LoadConfig();
                    var url = config.AppSettings.Settings["twilio_errors_sensor_url"].Value;
                    var result = new HttpClient().GetAsync(url).Result;
                }
            }
            catch (Exception ex)
            {
                var url = config.AppSettings.Settings["twilio_errors_sensor_url"].Value;
                var result = new HttpClient().GetAsync(url).Result;
            }

            return req.CreateResponse(HttpStatusCode.OK, "ok");
        }

        public static async Task<HttpResponseMessage> MandrillRejectionWebhook(HttpRequestMessage req, TraceWriter log)
        {
            var body = req.Content.ReadAsStringAsync().Result;
            if (string.IsNullOrWhiteSpace(body)) return req.CreateResponse(HttpStatusCode.OK, "ok");

            body = WebUtility.UrlDecode(body.Replace("mandrill_events=", ""));
            var json = JsonConvert.DeserializeObject<List<MandrillWebhookEvent>>(body);

            foreach (var _event in json)
            {
                if (string.Equals(_event.msg.reject?.reason, "unsigned", StringComparison.OrdinalIgnoreCase))
                {
                    if (config == null) LoadConfig();
                    var url = config.AppSettings.Settings["mandrill_unsigned_rejections_sensor_url"].Value;
                    var result = new HttpClient().GetAsync(url).Result;
                }
            }

            return req.CreateResponse(HttpStatusCode.OK, "ok");
        }

        private static void LoadConfig()
        {
            ExeConfigurationFileMap configMap = new ExeConfigurationFileMap();
            configMap.ExeConfigFilename = @"D:\home\site\wwwroot\web.config";
            config = ConfigurationManager.OpenMappedExeConfiguration(configMap, ConfigurationUserLevel.None);
        }
    }

    public class Reject
    {
        public string reason { get; set; }
        public object last_event_at { get; set; }
    }

    public class Msg
    {
        public int ts { get; set; }
        public string subject { get; set; }
        public string email { get; set; }
        public List<object> tags { get; set; }
        public List<object> opens { get; set; }
        public List<object> clicks { get; set; }
        public string state { get; set; }
        public List<object> smtp_events { get; set; }
        public object subaccount { get; set; }
        public List<object> resends { get; set; }
        public Reject reject { get; set; }
        public string _id { get; set; }
        public string sender { get; set; }
        public object template { get; set; }
    }

    public class MandrillWebhookEvent
    {
        [JsonProperty("event")]
        public string Event { get; set; }
        public string _id { get; set; }
        public Msg msg { get; set; }
        public int ts { get; set; }
    }
}