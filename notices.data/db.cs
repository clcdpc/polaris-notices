﻿using Dapper;
using Notices.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Linq.Expressions;
using System.Reflection;

namespace Notices.Data
{
    public class DbHelper
    {
        public string Server { get; set; }
        public string DbName { get; set; }

        static List<SettingValue> SettingValueCache { get; set; }
        static List<SettingType> SettingTypeCache { get; set; }
        static List<EmailTemplate> EmailTemplateCache { get; set; }
        static List<PolarisOrganization> OrganizationsCache { get; set; }


        public DbHelper(string server, string dbname)
        {
            Server = server;
            DbName = dbname;

            if (SettingValueCache == null) SettingValueCache = SettingValues().ToList();
            if (SettingTypeCache == null) SettingTypeCache = SettingTypes().ToList();
            if (EmailTemplateCache == null) EmailTemplateCache = EmailTemplates().ToList();
        }

        IEnumerable<T> Get<T>(string sql, Pred[] predicates = null)
        {
            var builder = new SqlBuilder();
            var template = builder.AddTemplate(sql);

            if (predicates != null)
            {
                foreach (var p in predicates)
                {
                    var par = new DynamicParameters();
                    par.Add(p.PropertyName, p.Value);
                    builder.Where($"{p.PropertyName} {p.Operator} @{p.PropertyName}", par);
                }
            }

            using (var conn = new SqlConnection($"Server={Server};Database={DbName};Trusted_Connection=True;"))
            {
                var results = conn.Query<T>(template.RawSql, template.Parameters);
                return results;
            }
        }

        public IEnumerable<TodaysHoldCall> TodaysHoldCalls()
        {
            var sql = "select * from CLC_Notices.dbo.TodaysHoldCalls";
            return Get<TodaysHoldCall>(sql, null);
        }

        public IEnumerable<Dialout_Strings> DialoutStrings()
        {
            var sql = "select * from CLC_Notices.dbo.Dialout_Strings";
            return Get<Dialout_Strings>(sql, null);
        }

        IEnumerable<SettingValue> SettingValues(Pred[] predicates = null)
        {
            var sql = "select * from clc_notices.dbo.SettingValues";
            return Get<SettingValue>(sql, predicates);
        }

        IEnumerable<SettingType> SettingTypes(Pred[] predicates = null)
        {
            var sql = "select * from clc_notices.dbo.SettingTypes";
            return Get<SettingType>(sql, predicates);
        }

        IEnumerable<EmailTemplate> EmailTemplates(Pred[] predicates = null)
        {
            var sql = "select * from clc_notices.dbo.EmailTemplates";
            return Get<EmailTemplate>(sql, predicates);
        }

        public IEnumerable<PolarisNotification> PolarisNotifications(Pred[] predicates)
        {
            var sql = @"
            select *
            from CLC_Notices.dbo.PolarisNotifications
            /**where**/
            ";

            return Get<PolarisNotification>(sql, predicates);
        }

        public IEnumerable<PolarisNotification> GetPhoneNotices()
        {
            var preds = new[]
            {
                Pred.Field<PolarisNotification>(pn => pn.DeliveryOptionID, "in", new[] { (int)DeliveryOption.Phone1, (int)DeliveryOption.Phone2, (int)DeliveryOption.Phone3 }),
                Pred.Field<PolarisNotification>(pn => pn.NotificationTypeID, "in", new[] { (int)NotificationType.FirstOverdue, (int)NotificationType.SecondOverdue, (int)NotificationType.ThirdOverdue, (int)NotificationType.Hold })
            };

            return PolarisNotifications(preds);
        }

        public static PolarisOrganization GetParentOrg(int orgId)
        {
            var parentId = orgId == 1 ? 1 : OrganizationsCache.SingleOrDefault(o => o.OrganizationID == orgId)?.ParentOrganizationID ?? 1;
            return OrganizationsCache.Single(o => o.OrganizationID == parentId);
        }


        public static T GetValue<T>(string mnemonic, int organizationId = 1, int languageId = 1033)
        {
            mnemonic = mnemonic.ToLower();

            var orgIds = new List<int>() { organizationId, 1 };

            var parent = GetParentOrg(organizationId);
            if (parent.OrganizationCodeID == 2) { orgIds.Add(parent.OrganizationID); }

            var val = SettingValueCache.Where(v => orgIds.Contains(v.OrganizationID) && v.Mnemonic == mnemonic && v.LanguageID == languageId)
                                      .OrderByDescending(v => v.OrganizationID)
                                      .FirstOrDefault()?
                                      .Value;

            if (string.IsNullOrWhiteSpace(val))
            {
                var defaultValue = SettingTypeCache.SingleOrDefault(t => t.Mnemonic == mnemonic)?.DefaultValue;
                if (string.IsNullOrEmpty(defaultValue)) { throw new ArgumentException($"No default found for {mnemonic}"); }
                val = defaultValue;
            }

            return (T)Convert.ChangeType(val, typeof(T));
        }


        //public static void Test()
        //{
        //    var foo = new[]
        //    {
        //        Pred.Field<PolarisOrganization>(o=>o.OrganizationID, "in", new[] {1,2,3,4 }),
        //        Pred.Field<PolarisOrganization>(o=>o.OrganizationCodeID, ">", 1)
        //    };

        //    var sql = @"
        //    select *
        //    from Polaris.Polaris.Organizations
        //    /**where**/
        //    ";

        //    var builder = new SqlBuilder();
        //    var template = builder.AddTemplate(sql);

        //    foreach (var p in foo)
        //    {
        //        var par = new DynamicParameters();
        //        par.Add(p.PropertyName, p.Value);
        //        builder.Where($"{p.PropertyName} {p.Operator} @{p.PropertyName}", par);

        //    }

        //    var zxcv = template.Parameters;
        //    var lkj = template.RawSql;

        //    using (var conn = new SqlConnection("Server=devdb;Database=polaris;Trusted_Connection=True;"))
        //    {
        //        var asdf = conn.Query(template.RawSql, template.Parameters);
        //        var qwer = foo;
        //    }


        //    var bar = foo;
        //}

        //public static string PolarisNotifications(int? patronId = null, List<NotificationType> noticeTypes = null, List<DeliveryOption> deliveryOptions = null)
        //{
        //    var sql = @"
        //    select *
        //    from CLC_Notices.dbo.PolarisNotifications pn
        //    /**where**/
        //    ";

        //    var builder = new SqlBuilder();
        //    var template = builder.AddTemplate(sql);

        //    if (patronId.HasValue)
        //    {
        //        builder.Where("pn.patronid = @patronid", new { patronId });
        //    }

        //    if (noticeTypes != null)
        //    {
        //        builder.Where("pn.NotificationTypeId in @notificationTypes", new { notificationTypes = noticeTypes.Select(nt => (int)nt) });
        //    }

        //    if (deliveryOptions != null)
        //    {
        //        builder.Where("pn.DeliveryOptionId in @deliveryOptions", new { deliveryOptions = deliveryOptions.Select(d => (int)d) });
        //    }

        //    using (var conn = new SqlConnection("Server=devdb;Database=CLC_Notices;Trusted_Connection=True;"))
        //    {
        //        var foo = conn.Query<PolarisNotification>(template.RawSql, template.Parameters);
        //        var bar = foo;
        //    }

        //    var asdf = template.Parameters;
        //    return template.RawSql;
        //}

    }

    public class Pred
    {
        public string PropertyName { get; set; }
        public string Operator { get; set; }
        public object Value { get; set; }

        public static Pred Field<T>(Expression<Func<T, object>> expression, string op, object value)
        {
            var foo = GetProperty(expression) as PropertyInfo;
            return new Pred { PropertyName = foo.Name, Operator = op, Value = value };
        }

        public static MemberInfo GetProperty(LambdaExpression lambda)
        {
            Expression expr = lambda;
            for (; ; )
            {
                switch (expr.NodeType)
                {
                    case ExpressionType.Lambda:
                        expr = ((LambdaExpression)expr).Body;
                        break;
                    case ExpressionType.Convert:
                        expr = ((UnaryExpression)expr).Operand;
                        break;
                    case ExpressionType.MemberAccess:
                        MemberExpression memberExpression = (MemberExpression)expr;
                        MemberInfo mi = memberExpression.Member;
                        return mi;
                    default:
                        return null;
                }
            }
        }
    }
}
