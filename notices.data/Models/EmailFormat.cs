﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Notices.Data.Models
{
    public enum EmailFormat
    {
        PlainText = 1,
        Html
    }
}