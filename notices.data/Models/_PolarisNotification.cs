﻿//using Clc.Polaris.Api.Models;
//using Notices.Data.Models;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;

//namespace Notices.Data
//{
//    public partial class PolarisNotification
//    {
//        public PolarisNotification()
//        {
//            CkoDate = null;
//            CkoLocation = null;
//            ItemBarcode = null;
//            ItemCallNumber = null;
//            ItemRecordID = 0;
//            Author = null;
//            DueDate = null;
//            HoldTillDate = null;
//            HoldCancellationReason = null;
//            CancelledHoldRequestDate = null;
//        }

//        public EmailFormat EmailFormat { get { return (EmailFormat)EmailFormatID; } }
//        public NotificationType NotificationType { get { return (NotificationType)NotificationTypeID; } }

//        public bool ShouldPlaceCall
//        {
//            get
//            {
//                var callOnClosedDate = NoticeEntities.GetValue<bool>(Mnemonics.DialoutCallOnClosedDate, ReportingBranchID, PatronLanguage);
//                var callonHoursOfOperationClosed = NoticeEntities.GetValue<bool>(Mnemonics.DialoutCallOnClosedHoursOfOperation, ReportingBranchID, PatronLanguage);

//                return (!HasClosedDate || callOnClosedDate) && (!HoursOfOperationClosed || callonHoursOfOperationClosed);               
//            }
//        }

//        public NoticeUpdate BuildUpdate(NotificationStatus notificationStatus, string details = "")
//        {
//            var update = new NoticeUpdate
//            {
//                NotificationTypeId = NotificationTypeID,
//                ReportingOrgId = ReportingBranchID,
//                DeliveryOptionId = DeliveryOptionID,
//                DeliveryDate = DateTime.Now,
//                PatronId = PatronID,
//                DeliveryString = DeliveryOptionID == 2 ? EmailAddress : DeliveryPhone,
//                ItemRecordId = ItemRecordID,
//                NotificationStatus = notificationStatus,
//                Details = NotificationType == NotificationType.Cancel ? ItemRecordID.ToString() : details
//            };

//            return update;
//        }

//        public override string ToString()
//        {
//            return $"{NotificationType} - {(DeliveryOption)DeliveryOptionID} | RB:{ReportingBranchID} - PID:{PatronID} IID:{ItemRecordID} PH:{DeliveryPhone} EM:{EmailAddress}";
//        }

//        public static PolarisNotification GenerateBogusNotice()
//        {
//            return new PolarisNotification
//            {
//                PatronID = Methods.GenerateRandomInt(),
//                NotificationTypeID = Methods.GenerateRandomInt(),
//                DeliveryOptionID = Methods.GenerateRandomInt(),
//                AltEmailAddress = Methods.GenerateRandomString(),
//                EmailAddress = Methods.GenerateRandomString(),
//                NameFirst = Methods.GenerateRandomString(),
//                NameLast = Methods.GenerateRandomString(),
//                PatronBranchAbbr = Methods.GenerateRandomString(),
//                PatronLibraryAbbr = Methods.GenerateRandomString(),
//                ReportingBranchAbbr = Methods.GenerateRandomString(),
//                DeliveryPhone = Methods.GenerateRandomString(),
//                DueDate = Methods.GenerateRandomDate(),
//                EmailFormatID = Methods.GenerateRandomInt(),
//                HoldTillDate = Methods.GenerateRandomDate(),
//                ReportingBranchID = Methods.GenerateRandomInt(),
//                ReportingLibraryID = Methods.GenerateRandomInt(),
//                hours = Methods.GenerateRandomString(),
//                PatronBarcode = Methods.GenerateRandomString(),
//                PatronBranch = Methods.GenerateRandomInt(),
//                PatronCity = Methods.GenerateRandomString(),
//                PatronLanguage = Methods.GenerateRandomInt(),
//                PatronLibrary = Methods.GenerateRandomInt(),
//                PatronState = Methods.GenerateRandomString(),
//                PatronStreetOne = Methods.GenerateRandomString(),
//                PatronStreetTwo = Methods.GenerateRandomString(),
//                PatronZip = Methods.GenerateRandomString(),
//                TxtPhoneNumber = Methods.GenerateRandomString(),
//                ReportingBranchCity = Methods.GenerateRandomString(),
//                ReportingBranchName = Methods.GenerateRandomString(),
//                ReportingBranchState = Methods.GenerateRandomString(),
//                ReportingBranchStreetOne = Methods.GenerateRandomString(),
//                ReportingBranchStreetTwo = Methods.GenerateRandomString(),
//                ReportingBranchZip = Methods.GenerateRandomString(),
//                ItemRecordID = Methods.GenerateRandomInt(),
//                Title = Methods.GenerateRandomString()
//            };
//        }

//        static PolarisNotification GenerateTest(int patronId, int patronBranch, int reportingBranch, int notificationTypeId, int deliveryOptionId)
//        {
//            return new PolarisNotification
//            {
//                PatronID = patronId,
//                NotificationTypeID = notificationTypeId,
//                DeliveryOptionID = Methods.GenerateRandomInt(),
//                AltEmailAddress = Methods.GenerateRandomString(),
//                EmailAddress = Methods.GenerateRandomString(),
//                NameFirst = Methods.GenerateRandomString(),
//                NameLast = Methods.GenerateRandomString(),
//                PatronBranchAbbr = Methods.GenerateRandomString(),
//                PatronLibraryAbbr = Methods.GenerateRandomString(),
//                ReportingBranchAbbr = Methods.GenerateRandomString(),
//                DeliveryPhone = Methods.GenerateRandomString(),
//                DueDate = Methods.GenerateRandomDate(),
//                EmailFormatID = Methods.GenerateRandomInt(),
//                HoldTillDate = Methods.GenerateRandomDate(),
//                ReportingBranchID = Methods.GenerateRandomInt(),
//                ReportingLibraryID = Methods.GenerateRandomInt(),
//                hours = Methods.GenerateRandomString(),
//                PatronBarcode = Methods.GenerateRandomString(),
//                PatronBranch = Methods.GenerateRandomInt(),
//                PatronCity = Methods.GenerateRandomString(),
//                PatronLanguage = Methods.GenerateRandomInt(),
//                PatronLibrary = Methods.GenerateRandomInt(),
//                PatronState = Methods.GenerateRandomString(),
//                PatronStreetOne = Methods.GenerateRandomString(),
//                PatronStreetTwo = Methods.GenerateRandomString(),
//                PatronZip = Methods.GenerateRandomString(),
//                TxtPhoneNumber = Methods.GenerateRandomString(),
//                ReportingBranchCity = Methods.GenerateRandomString(),
//                ReportingBranchName = Methods.GenerateRandomString(),
//                ReportingBranchState = Methods.GenerateRandomString(),
//                ReportingBranchStreetOne = Methods.GenerateRandomString(),
//                ReportingBranchStreetTwo = Methods.GenerateRandomString(),
//                ReportingBranchZip = Methods.GenerateRandomString(),
//                ItemRecordID = Methods.GenerateRandomInt(),
//                Title = Methods.GenerateRandomString(),
//                AutoRenewal = notificationTypeId == 7 ? Convert.ToBoolean(Methods.GenerateRandomInt(0, 1)) : false
//            };
//        }

//        public static List<PolarisNotification> GenerateTest(NotificationType notificationType, DeliveryOption deliveryOption, int patronBranch, int reportingBranchId)
//        {
//            return GenerateTest(new[] { notificationType }, deliveryOption, patronBranch, new[] { reportingBranchId });
//        }

//        public static List<PolarisNotification> GenerateTest(NotificationType[] notificationTypes, DeliveryOption deliveryOption, int patronBranch, int[] reportingBranchIds)
//        {
//            var notices = new List<PolarisNotification>();

//            var patronId = Methods.GenerateRandomInt(1000000, 9999999);
//            var emailAddress = $"{Methods.GenerateRandomString(15)}@{Methods.GenerateRandomString(10)}.{Methods.GenerateRandomString(3)}";
//            var nameFirst = Methods.GenerateRandomString(7);
//            var nameLast = Methods.GenerateRandomString(12);
//            var patronBranchAbbr = Methods.GenerateRandomString(5).ToUpper();
//            var patronLibraryAbbr = Methods.GenerateRandomString(5).ToUpper();
//            var deliveryPhone = $"{Methods.GenerateRandomInt(100, 999)}-{Methods.GenerateRandomInt(100, 999)}-{Methods.GenerateRandomInt(1000, 9999)}".ToString();
//            var patronBarcode = Methods.GenerateRandomInt(10000000, 99999999).ToString();
//            var patronCity = Methods.GenerateRandomString(15);
//            var patronLibrary = Methods.GetParentOrg(patronBranch).OrganizationID;
//            var patronState = Methods.GenerateRandomString(2).ToUpper();
//            var patronStreetOne = $"{Methods.GenerateRandomInt(100, 9999)} {Methods.GenerateRandomString(10)} St.";
//            var patronZip = Methods.GenerateRandomInt(10000, 99999).ToString();
//            var txtPhoneNumber = $"{Methods.GenerateRandomInt(100, 999)}-{Methods.GenerateRandomInt(100, 999)}-{Methods.GenerateRandomInt(1000, 9999)}".ToString();

//            foreach (var notificationType in notificationTypes)
//            {
//                foreach (var reportingBranchId in reportingBranchIds)
//                {
//                    var reportingLibraryId = Methods.GenerateRandomInt(1, 35);
//                    var reportingBranchAbbr = Methods.GenerateRandomString(5).ToUpper();
//                    var reportingBranchName = Methods.GenerateWords(null, 7, 12);
//                    var reportingBranchCity = Methods.GenerateRandomString(15);
//                    var reportingBranchState = Methods.GenerateRandomString(7);
//                    var reportingBranchStreetOne = $"{Methods.GenerateRandomInt(100, 9999)} {Methods.GenerateRandomString(10)} St.";
//                    var reportingBranchZip = Methods.GenerateRandomInt(10000, 99999).ToString();

//                    var count = Methods.GenerateRandomInt(5, 12);
//                    for (int i = 0; i < count; i++)
//                    {
//                        var notice = new PolarisNotification
//                        {
//                            PatronID = patronId,
//                            NotificationTypeID = (int)notificationType,
//                            DeliveryOptionID = (int)deliveryOption,
//                            EmailAddress = emailAddress,
//                            NameFirst = nameFirst,
//                            NameLast = nameLast,
//                            PatronBranchAbbr = patronBranchAbbr,
//                            PatronLibraryAbbr = patronLibraryAbbr,
//                            ReportingBranchAbbr = reportingBranchAbbr,
//                            DeliveryPhone = deliveryPhone,
//                            EmailFormatID = 1,
//                            ReportingBranchID = reportingBranchId,
//                            ReportingLibraryID = reportingLibraryId,
//                            PatronBarcode = patronBarcode,
//                            PatronBranch = patronBranch,
//                            PatronCity = patronCity,
//                            PatronLanguage = 1033,
//                            PatronLibrary = patronLibrary,
//                            PatronState = patronState,
//                            PatronStreetOne = patronStreetOne,
//                            PatronZip = patronZip,
//                            TxtPhoneNumber = txtPhoneNumber,
//                            ReportingBranchCity = reportingBranchCity,
//                            ReportingBranchName = reportingBranchName,
//                            ReportingBranchState = reportingBranchState,
//                            ReportingBranchStreetOne = reportingBranchStreetOne,
//                            ReportingBranchZip = reportingBranchZip,
//                            ItemAssignedBranch = Methods.GenerateWords(2) + " Branch",
//                            AutoRenewal = notificationType == NotificationType.AlmostOverdueReminder ? Methods.GenerateRandomInt(0, 1) > 0 : false
//                        };

//                        if (notificationType != NotificationType.ExpirationReminder)
//                        {
//                            notice.ItemRecordID = Methods.GenerateRandomInt();
//                            notice.MaterialType = Methods.GenerateRandomString(15);
//                            notice.ItemBarcode = Methods.GenerateRandomString();
//                            notice.Author = $"{Methods.GenerateRandomString(10)}, {Methods.GenerateRandomString(6)}";
//                            notice.Title = Methods.GenerateWords();
//                            notice.ItemCallNumber = Methods.GenerateRandomString(10);
//                        }

//                        if (notificationType == NotificationType.Hold || notificationType == NotificationType.SecondHold)
//                        {
//                            notice.HoldTillDate = Methods.GenerateRandomDate();
//                        }

//                        if (notificationType == NotificationType.Cancel)
//                        {
//                            notice.CancelledHoldRequestDate = Methods.GenerateRandomDate();
//                            notice.HoldCancellationReason = Methods.GenerateRandomString(50);
//                        }

//                        if (notificationType.ToString().ToLower().Contains("overdue"))
//                        {
//                            notice.CkoDate = Methods.GenerateRandomDate();
//                            notice.CkoLocation = Methods.GenerateRandomString(50);
//                            notice.DueDate = Methods.GenerateRandomDate();
//                        }

//                        notices.Add(notice);
//                    }
//                }
//            }

//            return notices;
//        }

//        public static PolarisNotification CreateTest(TestPatron patron, TestOrganization reportingOrg, TestItem item, int notificationTypeId)
//        {
//            return new PolarisNotification
//            {
//                PatronID = patron.PatronId,
//                PatronBarcode = patron.Barcode,
//                NameFirst = patron.NameFirst,
//                NameLast = patron.NameLast,
//                ItemRecordID = item.ItemRecordId,
//                Title = item.Title,
//                HoldTillDate = item.HoldTillDate,
//                DueDate = item.DueDate,
//                NotificationTypeID = notificationTypeId,
//                DeliveryOptionID = patron.DeliveryOptionId,
//                EmailAddress = patron.EmailAddress,
//                DeliveryPhone = patron.DeliveryPhone,
//                PatronStreetOne = patron.StreetOne,
//                PatronStreetTwo = patron.StreetTwo,
//                PatronBranch = patron.AssignedBranch.OrganizationId,
//                PatronBranchAbbr = patron.AssignedBranch.Abbreviation,
//                PatronCity = patron.City,
//                PatronLanguage = patron.LanguageId,
//                PatronLibrary = patron.AssignedBranch.ParentOrg.OrganizationId,
//                PatronLibraryAbbr = patron.AssignedBranch.ParentOrg.Abbreviation,
//                PatronState = patron.State,
//                PatronZip = patron.Zip,
//                ReportingBranchID = reportingOrg.OrganizationId,
//                ReportingBranchAbbr = reportingOrg.Abbreviation,
//                ReportingBranchCity = reportingOrg.City,
//                ReportingBranchName = reportingOrg.Name,
//                ReportingBranchState = reportingOrg.State,
//                ReportingBranchStreetOne = reportingOrg.StreetOne,
//                ReportingBranchStreetTwo = reportingOrg.StreetTwo,
//                ReportingBranchZip = reportingOrg.Zip,
//                ReportingLibraryID = reportingOrg.ParentOrg.OrganizationId
//            };
//        }

//        void GenerateValidTestNotice()
//        {

//        }

//        public int EmailTemplateBranchId => IsHold || IsHoldCancellation || IsClcCustomCovid19 ? PatronBranch : ReportingBranchID;
//        public int EmailTemplateLibraryId => IsHold || IsHoldCancellation || IsClcCustomCovid19 ? PatronLibrary : ReportingLibraryID;
//        public int EmailGroupBranchId => IsHold || IsClcCustomCovid19 ? PatronBranch : ReportingBranchID;

//        public NotificationType EmailGroupNotificationType
//        {
//            get
//            {
//                if (!IsOverdue) return (NotificationType)NotificationTypeID;
//                return NoticeEntities.GetValue<bool>(Mnemonics.EmailCombineOverdueNotices, ReportingBranchID, PatronLanguage) ? NotificationType.Combined : (NotificationType)NotificationTypeID;
//            }
//        }

//        public NotificationType EmailGroupNotificationType2 => IsOverdue && NoticeEntities.GetValue<bool>(Mnemonics.EmailCombineOverdueNotices, ReportingBranchID, PatronLanguage) ? NotificationType.Combined : (NotificationType)NotificationTypeID;
            


//        public bool IsOverdue => new int[] { 1, 12, 13 }.Contains(NotificationTypeID);
//        public bool IsHold => new int[] { 2, 18 }.Contains(NotificationTypeID);
//        public bool IsHoldCancellation => NotificationTypeID == 3;
//        public bool IsFirstHold => NotificationTypeID == 2;
//        public bool IsSecondHold => NotificationTypeID == 18;
//        public bool IsAlmostOverdueReminder => NotificationTypeID == 7;
//        public bool IsBill => NotificationTypeID == 11;
//        public bool IsClcCustomCovid19 => NotificationTypeID == 999;
//    }



//    public class TestPatron
//    {
//        public int PatronId { get; set; }
//        public string Barcode { get; set; }
//        public string NameFirst { get; set; }
//        public string NameLast { get; set; }
//        public int LanguageId { get; set; }
//        public int DeliveryOptionId { get; set; }
//        public string EmailAddress { get; set; }
//        public string DeliveryPhone { get; set; }
//        public string StreetOne { get; set; }
//        public string StreetTwo { get; set; }
//        public string City { get; set; }
//        public string State { get; set; }
//        public string Zip { get; set; }
//        public TestOrganization AssignedBranch { get; set; } = new TestOrganization();
//        public List<TestItem> OverdueItems { get; set; } = new List<TestItem>();
//        public List<TestItem> HeldItems { get; set; } = new List<TestItem>();
//        public List<TestItem> AutoRenewItems { get; set; } = new List<TestItem>();
//        public List<TestItem> AlmostOverdueItems { get; set; } = new List<TestItem>();
//    }

//    public class TestOrganization
//    {
//        static TestOrganization DefaultParent = new TestOrganization();

//        public int OrganizationId { get; set; }
//        public string Name { get; set; }
//        public string Abbreviation { get; set; }
//        public string StreetOne { get; set; }
//        public string StreetTwo { get; set; }
//        public string City { get; set; }
//        public string State { get; set; }
//        public string Zip { get; set; }

//        public TestOrganization ParentOrg { get; set; } = DefaultParent;
//    }

//    public class TestItem
//    {
//        public int ItemRecordId { get; set; }
//        public string Title { get; set; }
//        public DateTime? HoldTillDate { get; set; }
//        public DateTime? DueDate { get; set; }
//        public TestOrganization HeldBranch { get; set; }
//        public int AutoRenew { get; set; } = 0;
//    }
//}