//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Notices.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class EmailStatus
    {
        public int StatusID { get; set; }
        public System.DateTime StatusDate { get; set; }
        public int Status { get; set; }
    }
}
