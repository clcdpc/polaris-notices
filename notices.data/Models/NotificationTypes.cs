﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notices.Data.Models
{
    public enum NotificationType
    {
        Combined = 0,
        FirstOverdue,
        Hold,
        Cancel,
        Recall,
        All,
        Route,
        AlmostOverdueReminder,
        Fine,
        InactiveReminder,
        ExpirationReminder,
        Bill,
        SecondOverdue,
        ThirdOverdue,
        SerialClaim,
        PolarisFusionAccessRequestResponses,
        CourseReserves,
        BorrowByMailFailureNotice,
        SecondHold,
        MissingPart,
        ManualBill,
        ClcCustomCovid19 = 999
    }                                   
}
