﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Notices.Data.Models
{
    public enum RecordSetTypes
    {
        Bounce = 1,
        Spam,
        SmsStop
    }
}