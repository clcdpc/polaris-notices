//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Notices.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class ClcCustomNotificationQueue
    {
        public int Id { get; set; }
        public int PatronId { get; set; }
        public int NotifictationTypeId { get; set; }
        public Nullable<int> ItemRecordId { get; set; }
        public int DeliveryOptionId { get; set; }
        public int ReportingBranchId { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public bool AutoRenewal { get; set; }
        public Nullable<int> BibId { get; set; }
    }
}
