using Notices.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Notices.Data
{    
    public partial class PolarisNotification
    {
        public int NotificationTypeID { get; set; }
        public int PatronID { get; set; }
        public string PatronBarcode { get; set; }
        public string NameFirst { get; set; }
        public string NameLast { get; set; }
        public string PatronStreetOne { get; set; }
        public string PatronStreetTwo { get; set; }
        public string PatronCity { get; set; }
        public string PatronState { get; set; }
        public string PatronZip { get; set; }
        public int ItemRecordID { get; set; }
        public int PatronBranch { get; set; }
        public string PatronBranchAbbr { get; set; }
        public int PatronLibrary { get; set; }
        public string PatronLibraryAbbr { get; set; }
        public string PatronBranchPhone { get; set; }
        public int DeliveryOptionID { get; set; }
        public string EmailAddress { get; set; }
        public string AltEmailAddress { get; set; }
        public int EmailFormatID { get; set; }
        public string DeliveryPhone { get; set; }
        public string PatronPhone { get; set; }
        public int ReportingBranchID { get; set; }
        public string ReportingBranchAbbr { get; set; }
        public string ReportingBranchName { get; set; }
        public string ReportingBranchStreetOne { get; set; }
        public string ReportingBranchStreetTwo { get; set; }
        public string ReportingBranchCity { get; set; }
        public string ReportingBranchState { get; set; }
        public string ReportingBranchZip { get; set; }
        public string ReportingBranchPhone { get; set; }
        public int ReportingLibraryID { get; set; }
        public Nullable<System.DateTime> HoldTillDate { get; set; }
        public Nullable<System.DateTime> DueDate { get; set; }
        public string ItemBarcode { get; set; }
        public string Author { get; set; }
        public string Title { get; set; }
        public string MaterialType { get; set; }
        public Nullable<System.DateTime> CkoDate { get; set; }
        public string CkoLocation { get; set; }
        public string ItemCallNumber { get; set; }
        public string hours { get; set; }
        public int PatronLanguage { get; set; }
        public string TxtPhoneNumber { get; set; }
        public string HoldCancellationReason { get; set; }
        public Nullable<System.DateTime> CancelledHoldRequestDate { get; set; }
        public bool AutoRenewal { get; set; }
        public string ItemAssignedBranch { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public string FeeDescription { get; set; }
        public bool HasClosedDate { get; set; }
        public bool HoursOfOperationClosed { get; set; }
        public Nullable<bool> ILLFlag { get; set; }        

        public int EmailTemplateBranchId => IsHold || IsHoldCancellation || IsClcCustomCovid19 ? PatronBranch : ReportingBranchID;
        public int EmailTemplateLibraryId => IsHold || IsHoldCancellation || IsClcCustomCovid19 ? PatronLibrary : ReportingLibraryID;
        public int EmailGroupBranchId => IsHold || IsClcCustomCovid19 ? PatronBranch : ReportingBranchID;


        public bool IsOverdue => new int[] { 1, 12, 13 }.Contains(NotificationTypeID);
        public bool IsHold => new int[] { 2, 18 }.Contains(NotificationTypeID);
        public bool IsHoldCancellation => NotificationTypeID == 3;
        public bool IsFirstHold => NotificationTypeID == 2;
        public bool IsSecondHold => NotificationTypeID == 18;
        public bool IsAlmostOverdueReminder => NotificationTypeID == 7;
        public bool IsBill => NotificationTypeID == 11;
        public bool IsClcCustomCovid19 => NotificationTypeID == 999;
    }
}
