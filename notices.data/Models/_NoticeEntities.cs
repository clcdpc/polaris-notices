﻿//using Notices.Data;
//using Notices.Data.Models;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using Dapper;

//namespace Notices.Data
//{
//    public partial class NoticeEntities
//    {
//        public IEnumerable<PolarisNotification> GetPhoneNotices()
//        {
//            var deliveryOptions = new[]
//            {
//                (int)DeliveryOption.Phone1,
//                (int)DeliveryOption.Phone2,
//                (int)DeliveryOption.Phone3
//            };

//            var notificationTypes = new int[]
//            {
//                (int)NotificationType.FirstOverdue,
//                (int)NotificationType.SecondOverdue,
//                (int)NotificationType.ThirdOverdue,
//                (int)NotificationType.Hold
//            };

//            return GetNotifications(deliveryOptions, notificationTypes).Where(n => AppSettings.LibrariesToCall.Contains(n.PatronLibrary));
//        }

//        static List<SettingValue> SettingValueCache;
//        static List<SettingType> SettingTypeCache;
//        static List<EmailTemplate> EmailTemplateCache;

//        public List<PolarisNotification> GetEmailAutoRenewalItems(int patronId)
//        {
//            return PolarisNotifications.Where(n => n.PatronID == patronId && n.NotificationType == NotificationType.AlmostOverdueReminder && n.AutoRenewal == true).ToList();
//        }

//        public List<PolarisNotification> GetEmailAlmostOverdueItems(int patronId)
//        {
//            return PolarisNotifications.Where(n => n.PatronID == patronId && n.NotificationType == NotificationType.AlmostOverdueReminder && n.AutoRenewal == false).ToList();
//        }

//        public static T GetValue<T>(string mnemonic, int organizationId = 1, int languageId = 1033)
//        {
//            mnemonic = mnemonic.ToLower();

//            var orgIds = new List<int>() { organizationId, 1 };

//            var parent = Methods.GetParentOrg(organizationId);
//            if (parent.OrganizationCodeID == 2) { orgIds.Add(parent.OrganizationID); }

//            var val = SettingValueCache.Where(v => orgIds.Contains(v.OrganizationID) && v.Mnemonic == mnemonic && v.LanguageID == languageId)
//                                      .OrderByDescending(v => v.OrganizationID)
//                                      .FirstOrDefault()?
//                                      .Value;

//            if (string.IsNullOrWhiteSpace(val))
//            {
//                var defaultValue = SettingTypeCache.SingleOrDefault(t => t.Mnemonic == mnemonic)?.DefaultValue;
//                if (string.IsNullOrEmpty(defaultValue)) { throw new ArgumentException($"No default found for {mnemonic}"); }
//                val = defaultValue;
//            }

//            return (T)Convert.ChangeType(val, typeof(T));
//        }

//        public static BranchEmailTemplateValue GetEmailTemplate(int orgId, int notificationTypeId, int languageId = 1033)
//        {
//            var val = EmailTemplateCache
//                .Where(t => new[] { 1, orgId, Methods.GetParentOrg(orgId).OrganizationID }.Contains(t.OrganizationId) && t.NotificationTypeId == notificationTypeId && t.LanguageId == languageId)
//                .GroupBy(t => t.EmailFormatId)
//                .Select(g => new { FormatId = g.Key, g.OrderByDescending(g2 => g2.OrganizationId).First().Url }).ToList();

//            var foo = val;

//            var output = new BranchEmailTemplateValue
//            {
//                OrganizationId = orgId,
//                NotificationTypeId = notificationTypeId,
//                LanguageId = languageId,
//                PlaintextTemplateUrl = val.Single(v => v.FormatId == 1).Url,
//                HtmlTempalateUrl = val.Single(v => v.FormatId == 2).Url
//            };

//            if (output.HtmlTempalateUrl.StartsWith("/")) { output.HtmlTempalateUrl = $"{AppSettings.BaseURL}{output.HtmlTempalateUrl}"; }
//            if (output.PlaintextTemplateUrl.StartsWith("/")) { output.PlaintextTemplateUrl = $"{AppSettings.BaseURL}{output.PlaintextTemplateUrl}"; }

//            return output;
//        }

//        public IEnumerable<PolarisNotification> GetEmailNotices(NotificationType notificationType)
//        {
//            return GetNotifications((int)DeliveryOption.EmailAddress, new[] { (int)notificationType });
//        }

//        public IEnumerable<PolarisNotification> GetEmailNotices(IEnumerable<NotificationType> notificationTypes)
//        {
//            return GetNotifications((int)DeliveryOption.EmailAddress, notificationTypes.Select(n => (int)n).ToArray());
//        }

//        public IEnumerable<PolarisNotification> GetSmsNotices(NotificationType[] notificationTypes)
//        {
//            return GetNotifications((int)DeliveryOption.TXTMessaging, notificationTypes.Select(n => (int)n).ToArray());
//        }

//        public IEnumerable<PolarisNotification> GetNotifications(int deliveryOption, int notificationType)
//        {
//            return PolarisNotifications.Where(n => n.DeliveryOptionID == deliveryOption && n.NotificationTypeID == notificationType);
//        }

//        public IEnumerable<PolarisNotification> GetNotifications(int deliveryOption, int[] notificationTypes)
//        {
//            //using (var conn = new SqlConnection($"Server={AppSettings.DbServer};Database=CLC_Notices;Trusted_Connection=True;"))
//            using (var conn = new SqlConnection($"Server={AppSettings.DbServer};Database=CLC_Notices;Trusted_Connection=True;"))
//            {
//                var sql = @"
//                select *
//                from CLC_Notices.dbo.PolarisNotifications pn
//                where pn.DeliveryOptionID = @deliveryOption
//	                and pn.NotificationTypeID in ( @notificationTypes )
//                ";
//                return conn.Query<PolarisNotification>(sql, new { deliveryOption, notificationTypes });
//            }
//            //var notices = PolarisNotifications.Where(n => n.DeliveryOptionID == deliveryOption && notificationTypes.Contains(n.NotificationTypeID));
//            //return notices;
//        }

//        public IEnumerable<PolarisNotification> GetNotifications(int[] deliveryOptions, int[] notificationTypes)
//        {
//            return PolarisNotifications.Where(n => deliveryOptions.Contains(n.DeliveryOptionID) && notificationTypes.Contains(n.NotificationTypeID));
//        }
//    }

//    public partial class NotificationQueueSnapshot
//    {
//        public PolarisNotification ToPolarisNotification()
//        {
//            return new PolarisNotification
//            {
//                AltEmailAddress = AltEmailAddress,
//                Author = Author,
//                AutoRenewal = AutoRenewal,
//                EmailAddress = EmailAddress,
//                ItemAssignedBranch = ItemAssignedBranch,
//                PatronBranchAbbr = PatronBranchAbbr,
//                PatronLibraryAbbr = PatronLibraryAbbr,
//                ReportingBranchAbbr = ReportingBranchAbbr,
//                CancelledHoldRequestDate = CancelledHoldRequestDate,
//                CkoDate = CkoDate,
//                CkoLocation = CkoLocation,
//                DeliveryOptionID = DeliveryOptionID,
//                DeliveryPhone = DeliveryPhone,
//                DueDate = DueDate,
//                EmailFormatID = EmailFormatID,
//                HoldCancellationReason = HoldCancellationReason,
//                HoldTillDate = HoldTillDate,
//                hours = hours,
//                ItemBarcode = ItemBarcode,
//                ItemCallNumber = ItemCallNumber,
//                ItemRecordID = ItemRecordID,
//                MaterialType = MaterialType,
//                NameFirst = NameFirst,
//                NameLast = NameLast,
//                NotificationTypeID = NotificationTypeID,
//                PatronBarcode = PatronBarcode,
//                PatronBranch = PatronBranch,
//                PatronBranchPhone = PatronBranchPhone,
//                PatronCity = PatronCity,
//                PatronID = PatronID,
//                PatronLanguage = PatronLanguage,
//                PatronLibrary = PatronLibrary,
//                PatronPhone = PatronPhone,
//                PatronState = PatronState,
//                PatronStreetOne = PatronStreetOne,
//                PatronStreetTwo = PatronStreetTwo,
//                PatronZip = PatronZip,
//                ReportingBranchCity = ReportingBranchCity,
//                ReportingBranchID = ReportingBranchID,
//                ReportingBranchName = ReportingBranchName,
//                ReportingBranchPhone = ReportingBranchPhone,
//                ReportingBranchState = ReportingBranchState,
//                ReportingBranchStreetOne = ReportingBranchStreetOne,
//                ReportingBranchStreetTwo = ReportingBranchStreetTwo,
//                ReportingBranchZip = ReportingBranchZip,
//                ReportingLibraryID = ReportingLibraryID,
//                Title = Title,
//                TxtPhoneNumber = TxtPhoneNumber
//            };
//        }
//    }
//}