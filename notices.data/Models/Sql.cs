﻿using Dapper;
using Notices.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Notices.Data
{
    public class Sql
    {
        public static string SettingTypes => "select * from CLC_Notices.dbo.SettingTypes";
        public static string SettingValues => "select * from CLC_Notices.dbo.SettingValues";
        public static string EmailTemplates => "select * from CLC_Notices.dbo.EmailTemplates";
        //public static string NoticesConnectionString => $"Server={AppSettings.DbServer};Database=CLC_Notices;Trusted_Connection=True;";

        public static string GetPatronsForSmsnumber(string pn) => $"exec CLC_Notices.dbo.GetPatronsForSmsNumber '{pn}'";
        public static string AddPatronNote(int patronId, string note) => $"exec CLC_Notices.dbo.AddPatronNote {patronId}, '{note}'";
        //db.RecordSets.Single(r => r.OrganizationID == patron.PatronLibraryID && r.RecordSetTypeID == (int)RecordSetTypes.SmsStop).RecordSetID;
        public static string GetClcNoticesRecordSetForPatron(int patronLibraryId, RecordSetTypes rsType) => $@"
            select *
            from CLC_Notices.dbo.RecordSets rs
            where rs.OrganizationID = {patronLibraryId}
	            and rs.RecordSetTypeID = {(int)rsType}
        ";

        public static void PolarisNotifications(int? patronId = null)
        {
            var sql = @"
            select *
            CLC_Notices.dbo.PolarisNotifications pn
            /**where**/
            ";

            var builder = new SqlBuilder();
            builder.AddTemplate(sql);

            if (patronId.HasValue)
            {
                builder.Where("pn.patronid = @patronid", new { patronId });
            }
        }


    }
}
