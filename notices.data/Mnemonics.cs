﻿using Notices.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notices.Core
{
    public static class Mnemonics
    {
        public static string EmailEnableOverdueNotices => "email_enable_overdue_notices";
        public static string EmailEnableHoldNotices => "email_enable_hold_notices";
        public static string EmailEnableSecondHoldNotices => "email_enable_second_hold_notices";
        public static string EmailEnableCancelNotices => "email_enable_cancellation_notices";
        public static string EmailEnableExpirationNotices => "email_enable_expiration_reminder_notices";
        public static string EmailEnableAlmostOverdueNotices => "email_enable_almost_overdue_notices";
        public static string EmailEnableCovidNotice => "email_enable_covid_notice";


        public static string EnableCovidHoldDetails => "email_enable_covid_hold_details";



        public static string EmailSubjectFirstOverdue => "email_subject_first_overdue";
        public static string EmailSubjectSecondOverdue => "email_subject_second_overdue";
        public static string EmailSubjectThirdOverdue => "email_subject_third_overdue";
        public static string EmailSubjectCancel => "email_subject_cancel";
        public static string EmailSubjectExpiration => "email_subject_expiration";
        public static string EmailSubjectHold => "email_subject_hold";
        public static string EmailSubjectAlmostOverdue => "email_subject_almost_overdue";
        public static string EmailSubjectSecondHold => "email_subject_second_hold";
        public static string EmailSubjectCombined => "email_subject_combined";
        public static string EmailSubjectCovid19 => "email_subject_covid19";

        public static string DialoutFromPhone => "dialout_from_phone";
        public static string DialoutCallOnClosedDate => "dialout_call_for_holds_on_closed_date";
        public static string DialoutCallOnClosedHoursOfOperation = "dialout_call_for_holds_on_closed_hours_of_operation";

        public static string PickupBranchUrl => "pickup_branch_url";
        public static string PrebuildDialoutMessages => "prebuild_dialout_messages";
        public static string EmailNoticeFromAddress => "email_from_address";
        public static string OverrideHoldDateFormat => "override_hold_date_format";
        public static string EmailReminderAutorenewItemsTableHeaderRow => "email_reminder_autorenew_items_table_header_row";
        public static string EmailHeldItemsTableHeaderRow => "email_held_items_table_header_row";
        public static string EmailCovidHoldReminderTableHeaderRow => "email_covid_hold_reminder_table_header_row";

        public static string EmailCombineOverdueNotices => "email_combine_overdue_notices";
        public static string EmailSeparateInneachItems => "email_separate_innreach_items";

        public static string EmailHoldBranchGroupFormat(EmailFormat ef) => DoEmailFormat("email_hold_branch_group_format", ef);
        public static string EmailHeldItemListFormat(EmailFormat ef) => DoEmailFormat("email_held_item_list_format", ef);
        public static string EmailHoldBranchGroupSeparator(EmailFormat ef) => DoEmailFormat("email_hold_branch_group_separator", ef);
        public static string EmailExpirationBody(EmailFormat ef) => DoEmailFormat("email_expiration_reminder_body", ef);
        public static string EmailReminderAutorenewItemRowFormat(EmailFormat ef) => DoEmailFormat("email_reminder_autorenew_item_row_format", ef);
        public static string EmailReminderAutornewItemsHeader(EmailFormat ef) => DoEmailFormat("email_reminder_autorenew_items_header", ef);
        public static string EmailHeldItemGroupHeader(EmailFormat ef) => DoEmailFormat("email_held_item_group_header", ef);
        public static string EmailAlmostOverdueTableSeparator(EmailFormat ef) => DoEmailFormat("email_almost_overdue_table_separator", ef);


        static string DoEmailFormat(string mnemonic, EmailFormat format) => $"{mnemonic}_{format.ToString()}";
    }
}
