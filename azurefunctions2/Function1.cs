using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Configuration;
using Newtonsoft.Json.Linq;
using System.Linq;
using System.Net.Http;

namespace azurefunctions2
{
    public static class TwilioErrorWebhook
    {
        [FunctionName("TwilioErrorWebhook")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            var excludedErrorCodes = Environment.GetEnvironmentVariable("excluded_error_codes").Split(';');
            var warningsToInclude = Environment.GetEnvironmentVariable("included_warnings").Split(';');

            try
            {
                var body = await new StreamReader(req.Body).ReadToEndAsync();
                req.Body.Seek(0, SeekOrigin.Begin);
                log.LogInformation(body);

                var form = req.Form.ToDictionary(f => f.Key, f => f.Value);
                dynamic json = JObject.Parse(form["Payload"]);

                var level = Convert.ToString(json.more_info.LogLevel).ToLower();
                string error = json.more_info.ErrorCode;

                if ((level == "error" && !excludedErrorCodes.Contains(error)) || warningsToInclude.Contains(error))
                {
                    var url = Environment.GetEnvironmentVariable("twilio_errors_sensor_url");
                    var result = new HttpClient().GetAsync(url).Result;
                }
            }
            catch (Exception ex)
            {
                log.LogError(ex, ex.Message);
                var url = Environment.GetEnvironmentVariable("twilio_errors_sensor_url");
                var result = new HttpClient().GetAsync(url).Result;
            }

            return new OkObjectResult("ok");
        }
    }
}
