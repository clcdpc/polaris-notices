﻿using Notices.Core;
using Notices.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Topshelf;

namespace sms_service
{
    class Program
    {
        public static void Main()
        {
            ServicePointManager.SecurityProtocol |= SecurityProtocolType.Tls12;
            DbSettings.Init();
            HostFactory.Run(x =>
            {
                x.Service<Service>(s =>
                {
                    s.ConstructUsing(name => new Service());
                    s.WhenStarted(sms => sms.Start());
                    s.WhenStopped(sms => sms.Stop());
                });
                x.RunAsLocalSystem();

                x.SetDescription("CLC SMS notice processing service");
                x.SetDisplayName("_CLC SMS Service");
                x.SetServiceName("clc-sms-service");
            });
        }
    }

    public class Service
    {
        QueueListener<SmsMessage> smsQueue;
        
        public void Start()
        {
            smsQueue = new SmsListener(Queues.SMS_MESSAGES);
            smsQueue.Start();
        }

        public void Stop()
        {
            smsQueue.Stop();
        }
    }
}
