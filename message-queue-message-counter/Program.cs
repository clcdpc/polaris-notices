﻿using Notices.Core;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prtg_failed_sms_sensor
{
    class Program
    {
        static void Main(string[] args)
        {
            if (DateTime.Now.Minute <= 30)
            {
                Console.WriteLine("0:ok");
                return;
            }
            try
            {
                var factory = new ConnectionFactory()
                {
                    HostName = args[0],
                    VirtualHost = args[1],
                    UserName = args[3],
                    Password = args[4]
                };


                using (var connection = factory.CreateConnection())
                using (var channel = connection.CreateModel())
                {
                    Console.WriteLine($"{(int)channel.QueueDeclarePassive(args[2]).MessageCount}:ok");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("-1:error");
            }
        }
    }
}
